package mindinformatica.com.earon.utils;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.LinearLayout;

/**
 * Created by camillacecconi on 25/07/17.
 */

public class OneThirdLinearLayout extends LinearLayout {
        public OneThirdLinearLayout(Context context) { super(context);}
        public OneThirdLinearLayout(Context context, AttributeSet attrs) { super(context, attrs); }
        public OneThirdLinearLayout(Context context, AttributeSet attrs, int defStyle) { super(context, attrs, defStyle); }

        @Override
        protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
            super.onMeasure(widthMeasureSpec, heightMeasureSpec);
            View parentScrollView=((View)(getParent().getParent()));

            if (parentScrollView!=null) {
                // check the container of the container is an HorizontallScrollView
                if (parentScrollView instanceof HorizontalScrollView) {
                    // Yes it is, so change width to HSV's width
                    widthMeasureSpec=parentScrollView.getMeasuredWidth();
                }
            }
            setMeasuredDimension(widthMeasureSpec/3, heightMeasureSpec);
            int nChildren = getChildCount();
            for (int i = 0; i < nChildren; i++){
                getChildAt(i).getLayoutParams().width = widthMeasureSpec/3;
            }
        }
    }

