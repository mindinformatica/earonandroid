package mindinformatica.com.earon.jellyfish;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.os.AsyncTask;
import android.util.Log;
import android.util.LruCache;

import java.util.ArrayList;

import mindinformatica.com.earon.R;

/**
 * classe per disegnare lo sfondo del gioco jellyfish
 * Created by lorenzo on 17/07/18.
 */

public class Sfondo implements CharacterGame {

    public Bitmap image;
    private int i = 0;
    private Context context;
    private BitmapFactory.Options options;
    private ArrayList<BitmapWorkerTask> sfondoTask;

    private LruCache<String, Bitmap> mMemoryCache;//è sempre null, perchè non la usiamo

    public Sfondo(Context context, LruCache<String, Bitmap> mMemoryCache) {
        this.context = context;
        this.mMemoryCache = mMemoryCache;
        this.sfondoTask = new ArrayList<>();
        options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(context.getResources(), R.drawable.sfondo0, options);
        // Calculate inSampleSize
        options.inSampleSize = JellyConstants.calculateInSampleSize(options, JellyConstants.SCREEN_WIDTH,JellyConstants.SCREEN_HEIGHT);
        options.inJustDecodeBounds = false;

        Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.sfondo0, options);
        image = Bitmap.createScaledBitmap(bmp,JellyConstants.SCREEN_WIDTH,JellyConstants.SCREEN_HEIGHT,false);

        JellyConstants.setTopMargin(image);
    }

    @Override
    public void draw(Canvas canvas) {
        canvas.drawBitmap(image, 0, 0, null);
        //manageCurrentFrame();
        //canvas.drawBitmap(image,frameToDraw,whereToDraw,null);
    }

    @Override
    public void update(){
        i = i == 74 ? 0 : i + 1;
        //Log.d("SFONDO", "j: " + i);
        setImageFrames(i);
    }

    public void setImageFrames(int j){
        String nameSfondo = "sfondo" + j;
        int id = context.getResources().getIdentifier(nameSfondo, "drawable", context.getPackageName());
        loadBitmap(id, j);
    }

    private void addBitmapToMemoryCache(String key, Bitmap bitmap) {
        if (getBitmapFromMemCache(key) == null) {
            mMemoryCache.put(key, bitmap);
        }
    }

    private Bitmap getBitmapFromMemCache(String key) {
        return mMemoryCache.get(key);
    }

    private void loadBitmap(int resId, int j) {
        final String imageKey = String.valueOf(resId);

        /*final Bitmap bitmap = getBitmapFromMemCache(imageKey);
        if (bitmap != null) {
            image = bitmap;
        } else {*/
            try {
                if(image != null) {
                    image.recycle();
                    image = null;
                }
                System.gc();
                Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), resId, options);
                image = Bitmap.createScaledBitmap(bmp, JellyConstants.SCREEN_WIDTH, JellyConstants.SCREEN_HEIGHT, false);
                /*if ((mMemoryCache.size() + image.getByteCount() / 1024) < mMemoryCache.maxSize() && j % 7 == 1) {
                    BitmapWorkerTask task = new BitmapWorkerTask();
                    task.execute(resId);
                }*/
            }catch (OutOfMemoryError error){
                Log.e("OUT_OF_MEMORY", "Caricamento bitmap sfondo:" + error.getMessage());
                //mMemoryCache.evictAll();//svuoto le precedenti cache
            }
        //}
    }

    //al momento non viene usato, perchè le cache sono null. Per la dimensione delle immagini, risulta quasi inutile, perchè si riesce a tenere solo 2/3 immagini nelle cache
    class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap> {
        // Decode image in background.
        @Override
        protected Bitmap doInBackground(Integer... params) {
            if(!isCancelled()) {
                addBitmapToMemoryCache(String.valueOf(params[0]), image);
            }
            return image;
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            sfondoTask.add(this);
        }

        @Override
        protected void onPostExecute(Bitmap bitmap) {
            super.onPostExecute(bitmap);
            sfondoTask.remove(this);
        }
    }

    public Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = JellyConstants.calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public ArrayList<BitmapWorkerTask> getSfondoTasks(){
        return sfondoTask;
    }

}
