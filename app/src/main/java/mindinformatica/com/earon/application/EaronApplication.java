package mindinformatica.com.earon.application;

import android.app.Activity;
import android.app.Application;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothProfile;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import mindinformatica.com.earon.MainActivity;
import mindinformatica.com.earon.bluetooth.AbstractManagerBluetooth;
import mindinformatica.com.earon.bluetooth.BluetoothActivity;
import mindinformatica.com.earon.bluetooth.ManagerBluetooth;
import mindinformatica.com.earon.bluetooth.ManagerBluetoothNoLocalization;
import mindinformatica.com.earon.impostazioni.ListaDevicesActivity;

public class EaronApplication extends Application {

    final static public String TAG = "EARON_APPLICATION";
    public AbstractManagerBluetooth mb;
    private String savedDeviceAddress;
    private Activity mActivity;
    private BluetoothDevice mDevice;

    @Override
    public void onCreate() {
        super.onCreate();
        SharedPreferences prefs = this.getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        savedDeviceAddress = prefs.getString("device", null);
        //è qui che devo controllare se posso accedere al bluetooth
        if(Build.VERSION.SDK_INT < 23) {
            mb = new ManagerBluetooth(this);
        }else {
            mb = new ManagerBluetoothNoLocalization(this);
        }
        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle bundle) {
                if(activity instanceof BluetoothActivity){
                    mb.activity = ((BluetoothActivity) activity);
                    ((BluetoothActivity) activity).mb = mb;
                }
                if(activity instanceof MainActivity){
                    mb.startScanner(false);
                }
            }
            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {
                mActivity = activity;
                if(activity instanceof BluetoothActivity) {
                    BluetoothActivity ba = ((BluetoothActivity) activity);
                    mb.activity = ba;
                    ba.mb = mb;
                    if(activity instanceof ListaDevicesActivity){
                        mb.scanLeDevice(false, true);
                    }
                    if (mb.connectionState == BluetoothProfile.STATE_CONNECTED) {
                        ba.deviceAlreadyConnected(mDevice);
                    }else if(mb.connectionState == BluetoothProfile.STATE_DISCONNECTED){
                        ba.deviceNotAlreadyConnected();
                    }
                }
            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });
    }

    //This method is for use in emulated process environments.
    // It will never be called on a production Android device, where processes are removed by simply killing them;
    @Override
    public void onTerminate() {
        mb.destroy();
        super.onTerminate();
    }

    public void deviceFound(BluetoothDevice device) {
        //in questa activity se trovo un device, devo controllare se è quello che ho memorizzato, se lo è, mi connetto, altrimenti nulla
        if(mActivity != null && mActivity instanceof ListaDevicesActivity){
            ListaDevicesActivity a = ((ListaDevicesActivity)mActivity);
            a.deviceFound(device);
            /*if (!a.lista.contains(device) && device.getName() != null && device.getName().startsWith("UBA")){
                a.lista.add(device);
                if (a.listaAdapter != null)
                    a.listaAdapter.notifyDataSetChanged();
            }*/
        } else if (savedDeviceAddress != null && savedDeviceAddress.equals(device.getAddress()) && device.getName() != null && device.getName().startsWith("UBA")){
            mDevice = device;
            mb.connectToDevice(device);
            if(Build.VERSION.SDK_INT >= 23) {
                ((ManagerBluetoothNoLocalization) mb).unregisterAppReceiver();
            }
        }
    }

    public void connectToDevice(BluetoothDevice device) {
        mDevice = device;
        mb.connectToDevice(device);
    }

    public boolean isConnected(){
        return this.mDevice != null;
    }

}
