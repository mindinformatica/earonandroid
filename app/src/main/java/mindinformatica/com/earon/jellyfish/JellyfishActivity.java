package mindinformatica.com.earon.jellyfish;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Insets;
import android.graphics.Rect;
import android.graphics.drawable.AnimationDrawable;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import androidx.dynamicanimation.animation.DynamicAnimation;
import androidx.dynamicanimation.animation.FlingAnimation;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.LruCache;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowInsets;
import android.view.WindowManager;
import android.view.WindowMetrics;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Random;

import mindinformatica.com.earon.MainActivity;
import mindinformatica.com.earon.R;
import mindinformatica.com.earon.bluetooth.BluetoothActivity;


public class JellyfishActivity extends BluetoothActivity {

    public static boolean gameOver;
    private GameView gameView;
    private String savedDeviceAddress;
    private boolean deviceConnected = false;
    private double interval;

    private static LayoutInflater inflater;
    private static RelativeLayout secondLayout;
    private static int fishRatio; //indica il rapporto larghezza/altezza del pesce
    private int counter;
    private static int seconds;

    private int score;

    private TextView tempoJellyTv;
    private TextView pressioneJellyTv;
    private TextView punteggioJellyTv;
    private TextView maxPunteggioJellyTv;
    public static ImageView jellyDiedIv;

    private double minDelta;
    private static Handler UIHandler;
    private static Button fishNumberButton;

    private LruCache<String, Bitmap> mMemoryCache;//per ora non si usa: l'inizializzazione è commentata
    private static JellyfishActivity mainActivity;
    private BluetoothDevice device;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);*/

        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        JellyConstants.DEVICE_CONNECTED = false;
        gameOver = false;
        UIHandler = new Handler(Looper.getMainLooper());
        //setto il rapporto larghezza/altezza del pesce. servirà per creare ogni volta pesci in giusta scala
        BitmapDrawable bd = (BitmapDrawable) ContextCompat.getDrawable(this, R.drawable.pesciolinoscelto_1);
        int width = bd.getBitmap().getWidth();
        int height = bd.getBitmap().getHeight();
        fishRatio = width / height;

        //initializeCacheMemory();

        //prelevo la larghezza e altezza dello schermo
        int[] dimensions = getScreenDimensions();
        JellyConstants.SCREEN_HEIGHT = dimensions[1];
        JellyConstants.SCREEN_WIDTH = dimensions[0];

        //imposto GPIXEL, ovvero il valore di G correlato ai pixel verticali dello schermo
        JellyConstants.setgPixel(JellyConstants.SCREEN_HEIGHT);

        //ricavo l'intervallo di calcolo dei frame nel mainThread (servirà per settare il tempo per la legge oraria di Jelly
        interval = JellyConstants.getInterval();

        //Layout principale
        RelativeLayout mainLayout = new RelativeLayout(this);
        mainLayout.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        mainLayout.setClipChildren(false);
        mainLayout.setClipToPadding(false);
        mainLayout.setBackgroundColor(Color.TRANSPARENT);//TRANSPARENT o WHITE?

        gameView = new GameView(this, mMemoryCache);
        gameView.setId(R.id.gameId);
        //aggiungo la gameView al layout principale
        mainLayout.addView(gameView);
        setContentView(mainLayout);

        //attacco un secondo layout dove andranno i pesci
        inflater = getLayoutInflater();
        getWindow().addContentView(inflater.inflate(R.layout.relative_fish_layout, null), new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        secondLayout = findViewById(R.id.secondLayout);

        tempoJellyTv = secondLayout.findViewById(R.id.tempo_jelly);
        pressioneJellyTv = secondLayout.findViewById(R.id.pressione_jelly);
        punteggioJellyTv = secondLayout.findViewById(R.id.punteggio_jelly);
        maxPunteggioJellyTv = secondLayout.findViewById(R.id.max_punteggio_jelly);
        fishNumberButton = secondLayout.findViewById(R.id.fish_number_btn);

        jellyDiedIv = findViewById(R.id.jelly_died);

        mainActivity = this;

        //Thread per settare i frame di Jelly
        /*Thread thread = new Thread() {
            @Override
            public void run() {
                int i = 0;
                int j = 0;
                String nameJelly = "medusa";
                try {
                    while(!gameOver) {
                        sleep(25);
                        boolean isDying = gameView.getJellyfish().isDying;
                        if(isDying) {
                            if(i == 49 && nameJelly.endsWith("morta")){
                                gameOver = true;
                                continue;
                            }
                            if (i == 0) {
                                nameJelly = "medusa_morta";
                            }
                        }else{
                            nameJelly = "medusa";
                        }
                        String nameSfondo = "sfondo" + j;
                        //gameView.setSfondo(nameSfondo);
                        //gameView.setSfondo(j);
                        gameView.setImage(nameJelly + i);
                        i = i == 49 ? 0 : i + 1;
                        j = j == 74 ? 0 : j + 1;
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();*/
    }

    //ritorna larghezza e altezza dello schermo (da android 11 getMetrics è deprecato
    public int[] getScreenDimensions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            WindowMetrics windowMetrics = getWindowManager().getCurrentWindowMetrics();
            Insets insets = windowMetrics.getWindowInsets()
                    .getInsetsIgnoringVisibility(WindowInsets.Type.systemBars());
            return new int[] {windowMetrics.getBounds().width() - insets.left - insets.right, windowMetrics.getBounds().height()-insets.top-insets.bottom};
        } else {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            //noinspection deprecation
            getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            //ricavo prima lo spazio in altezza occupato dalla barra delle notifiche in alto
            int statusBarHeight = 0;
            int resourceId = getResources().getIdentifier("status_bar_height", "dimen", "android");
            if (resourceId > 0) {
                statusBarHeight = getResources().getDimensionPixelSize(resourceId);
            }
            return new int[] {displayMetrics.widthPixels, displayMetrics.heightPixels-statusBarHeight};
        }
    }

    private void initializeCacheMemory(){
        //gestione cache
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
        // Use 1/6th of the available memory for this memory cache.
        final int cacheSize = maxMemory / 8;
        mMemoryCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap bitmap) {
                // The cache size will be measured in kilobytes rather than
                // number of items.
                return bitmap.getByteCount() / 1024;
            }
        };
        mMemoryCache.evictAll();//svuoto le precedenti cache
    }

    public static void die(final GameView gameView) {
        //gameView.getMainThread().setRunning(false);
        Runnable r = new Runnable() {
            final TranslateAnimation trans1 = new TranslateAnimation(
                    Animation.RELATIVE_TO_PARENT, 0.1f,
                    Animation.RELATIVE_TO_PARENT, 0.3f,
                    Animation.RELATIVE_TO_PARENT, 0.0f,
                    Animation.RELATIVE_TO_PARENT, 1.0f);
            final JellyfishActivity jellyActivity = mainActivity;
            @Override
            public void run() {
                Jellyfish jelly = gameView.getJellyfish();
                jellyDiedIv.setX(jelly.getX());
                jellyDiedIv.setY(jelly.getY());
                ViewGroup.LayoutParams param = jellyDiedIv.getLayoutParams();
                param.width = JellyConstants.JELLY_WIDTH;
                param.height = JellyConstants.JELLY_HEIGHT;
                jellyDiedIv.setLayoutParams(param);
                jellyDiedIv.setVisibility(View.VISIBLE);
                AnimationDrawable backgroundAnim = (AnimationDrawable) jellyDiedIv.getBackground();
                backgroundAnim.start();

                AccelerateInterpolator accelerateInterpolator = new AccelerateInterpolator();
                trans1.setDuration(600);
                trans1.setInterpolator(accelerateInterpolator);
                trans1.setFillAfter(true);
                trans1.setFillEnabled(true);
                trans1.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        jellyDiedIv.setVisibility(View.INVISIBLE);
                        jellyActivity.finish();
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });

                FlingAnimation rot = new FlingAnimation(jellyDiedIv, DynamicAnimation.ROTATION);
                rot.setStartVelocity(2000f)
                        /*.setMinValue(0f) // minimum translationX property
                        .setMaxValue((float)JellyConstants.SCREEN_WIDTH)  // maximum translationX property*/
                        .setFriction(0.1f)
                        .start();

                FlingAnimation flingY = new FlingAnimation(jellyDiedIv, DynamicAnimation.TRANSLATION_Y);
                flingY.setStartVelocity(-1000f)
                        .setFriction(0.8f)
                        .addEndListener(new DynamicAnimation.OnAnimationEndListener() {
                            @Override
                            public void onAnimationEnd(DynamicAnimation animation, boolean canceled, float value, float velocity) {
                                //jellyDiedIv.setY(JellyConstants.SCREEN_HEIGHT);
                                //jellyDiedIv.setX(JellyConstants.SCREEN_WIDTH);
                                jellyDiedIv.startAnimation(trans1);
                            }
                        })
                        .start();

                FlingAnimation flingX0 = new FlingAnimation(jellyDiedIv, DynamicAnimation.TRANSLATION_X);
                flingX0.setStartVelocity(300f)
                        .setFriction(0.2f)
                        .start();
            }
        };
        UIHandler.post(r);
    }

    private static int getNumberPresentFish(){
        int num = 0;
        int n = secondLayout.getChildCount();
        for (int i = 0; i < n; i++) {
            View child = secondLayout.getChildAt(i);
            if (child instanceof ImageView && (child.getTag() == "fish_sx" || child.getTag() == "fish_dx")){
                num++;
            }
        }
        return num;
    }

    public static ArrayList<Rect> getRectFromView(){
        ArrayList<Rect> rects = new ArrayList<>();
        int n = secondLayout.getChildCount();
        float c1 = (float)1 / 13;
        float c2 = (float)1 / 12;
        for (int i = 0; i < n; i++) {
            View child = secondLayout.getChildAt(i);
            if (child instanceof ImageView && child.getId() != R.id.jelly_died){
                Rect r = new Rect();
                child.getGlobalVisibleRect(r);
                /*int diffY = r.bottom - r.top;
                int diffX = r.right - r.left;
                r.left = (int) (r.left + (diffX * c1));//si toglie un pò di padding a dx e sx (es: a r.left si aggiunge 1/13 della larghezza del rettangolo)
                r.right = (int) (r.right - (diffX * c1));
                r.top = (int) (r.top + (diffY * c2));//si toglie un pò di padding a sopra e sotto
                r.bottom = (int) (r.bottom - (diffY * c2));*/
                rects.add(r);
            }
        }
        return rects;
    }

    //fa partire l'animazione del pesce
    public static void animateFish(Context context, ImageView fish){
        try{
            if(gameOver){
               return;
            }
            Thread.sleep(getRandomLong(100,2000));
            //OvershootInterpolator a = new OvershootInterpolator();
            int widthPosition;
            if (fish.getTag() == "fish_sx"){
                widthPosition = JellyConstants.SCREEN_WIDTH + fish.getLayoutParams().width;
            }else{
                widthPosition = (-1) * (JellyConstants.SCREEN_WIDTH + fish.getLayoutParams().width);
            }

            long transitionTime = getRandomLong(6000, 15000);
            fish.animate().translationX(widthPosition).setDuration(transitionTime)/*.setInterpolator(a)*/.withEndAction(new Runnable() {
                ImageView fish;
                Context context;

                public Runnable init(Context context, ImageView fish){
                    this.fish = fish;
                    this.context = context;
                    return this;
                }

                @Override
                public void run() {
                    RelativeLayout parent = (RelativeLayout) fish.getParent();
                    int index = parent.indexOfChild(fish);
                    parent.removeView(fish);
                    fish = null;
                    System.gc();//suggerisco di chiamare il garbage collector. Non è detto venga chiamato
                    if(gameOver){
                        return;
                    }
                    //valuto il bottone relativo al numero di pesci da visualizzare
                    //se numero pesci è 1
                    if(fishNumberButton.getTag() == context.getResources().getString(R.string.oneFish)){
                        int n = getNumberPresentFish();
                        if(n == 0){//se non ci sono al momento altri pesci, ne creo uno nuovo, altrimenti non faccio nulla
                           createAndAnimateFish(index);
                        }
                    }else{
                        //se numero pesci è 2
                        int n = getNumberPresentFish();
                        if(n == 0){//se non ci sono al momento altri pesci, ne creo uno in più
                            try {//sospendo un attimo il thread per agevolare i calcoli negli altri thread (lo faccio solo qui dove ho da creare due pesci simultaneamente)
                                Thread.sleep(400);
                                createAndAnimateFish(index);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }
                        }
                        createAndAnimateFish(index + 1);//creo un pesce (due se non ce ne erano altri presenti)
                    }
                }

                private void createAndAnimateFish(int index){
                    try {//sospendo un attimo il thread per agevolare i calcoli negli altri thread
                        Thread.sleep(150);
                        boolean new_sx = Math.random() < 0.5;
                        ImageView fish = JellyfishActivity.createNewFish(context, new_sx, index);
                        String pesceTag = fish.getTag().toString().toUpperCase();
                        Log.i("PESCE","creato nuovo pesce: " + pesceTag);
                        animateFish(context, fish);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }.init(context,fish)).start();
        }catch (InterruptedException e){
            e.printStackTrace();
        }
    }

    /**
     *
     * @param context context di jellyfishActivity
     * @param sinistro booleano che stabilisce se va creato un pesce sinistro (se true) o destro (se false)
     * @param index (l'indice a cui va aggiunta la nuova ImageView nel relativeLayout
     * @return ImageView del pesce
     */
    public static ImageView createNewFish(Context context, boolean sinistro, int index){
        //creo una nuova imageView per il nuovo pesce
        ImageView fish = new ImageView(context);
        RelativeLayout.LayoutParams param = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        //prendo un intero random come nuova misura della larghezza
        int intValue = (int) getRandomLong(JellyConstants.SCREEN_WIDTH / 6, JellyConstants.SCREEN_WIDTH / 3);
        if(sinistro) {
            fish.setTag("fish_sx");
            fish.setBackgroundResource(R.drawable.pesce_riflesso_anim);
            fish.setScaleType(ImageView.ScaleType.CENTER_CROP);
            fish.setX(-1 * intValue);
        }else{
            fish.setTag("fish_dx");
            fish.setBackgroundResource(R.drawable.pesce_anim);
            fish.setX(JellyConstants.SCREEN_WIDTH);
        }
        fish.setScaleType(ImageView.ScaleType.CENTER_CROP);
        param.width = intValue;
        param.height = intValue / fishRatio;
        //imposto una posizione random lungo Y
        intValue = (int) getRandomLong(0, JellyConstants.SCREEN_HEIGHT - param.height);
        fish.setY(intValue);
        fish.setLayoutParams(param);
        AnimationDrawable reflexedAnim = (AnimationDrawable) fish.getBackground();
        reflexedAnim.start();
        secondLayout.addView(fish,secondLayout.getChildCount() - 1);
        return fish;
    }

    public void onChangeFishNumber(View view){
        if (view.getTag() == getString(R.string.oneFish)) {
            view.setBackground(ContextCompat.getDrawable(this, R.drawable.pesciolinocieco));
            view.setTag(getString(R.string.twoFish));
        }else{
            view.setBackground(ContextCompat.getDrawable(this,R.drawable.pesciolinocieco1));
            view.setTag(getString(R.string.oneFish));
        }
    }

    public void updateGameParameter(final float pressure, final double minDelta, final int count) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                seconds ++;
                tempoJellyTv.setText("" + seconds + " s");
                DecimalFormat df = new DecimalFormat("##.##");
                pressioneJellyTv.setText("" + df.format(pressure) + " hPa");
                if(minDelta <= 2){
                    pressioneJellyTv.setTextColor(getResources().getColor(R.color.greenbutton));
                }else{
                    pressioneJellyTv.setTextColor(getResources().getColor(R.color.colorAccent));
                }
                //aumento lo score
                score += updateScore(minDelta, count);
                punteggioJellyTv.setText("" + score);
            }
        });
    }

    private int updateScore(double minDelta, int counter){
        if (counter == 0) {
            return 0;
        } else if (minDelta <= 2) {
            return 50;
        } else if (minDelta <= 3) {
            return 30;
        } else if (minDelta <= 5) {
            return 18;
        } else if (minDelta <= 10) {
            return 13;
        } else if (minDelta <= 15) {
            return 5;
        } else {
            return 0;
        }
    }

    private void setFinalScore(){
        SharedPreferences prefs = getBaseContext().getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        int oldScore = prefs.getInt("score", 0);
        if(oldScore < score) {
            prefs.edit().putInt("score", score).commit();
        }
    }
    /**
     *
     * @return restituisce un intero compreso tra il valore minimo e il valore massimo
     */
    private static long getRandomLong(int min, int max){
        Random random = new Random();
        return random.nextInt(max + 1 - min) + min;
    }

    @Override
    protected void onResume() {
        super.onResume();
        //scan(false);

        gameOver = false;
        //impedisco allo schermo di attenuare la luminosità
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        SharedPreferences prefs = this.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
        savedDeviceAddress = prefs.getString("device", null);
        String soglia = prefs.getString("soglia", "");
        int maxScore = prefs.getInt("score", 0);
        maxPunteggioJellyTv.setText("" + maxScore);

        //gameView.massa = Double.parseDouble(soglia) / JellyConstants.G;
        if ("".equals(soglia) || "0".equals(soglia) ) soglia = "0.1";
        //dalla condizione di equilibrio forzaPressione - forzaPeso = 0 si ottiene la massa = sogliaCompensazione/g
        JellyConstants.SOGLIA_COMPENSAZIONE = Float.parseFloat(soglia);
        JellyConstants.MASSA =  Float.parseFloat(soglia) * JellyConstants.MULTIPLICATIVE_CONSTANT; // / JellyConstants.G;
        Log.i("MASSA_JELLYFISH", String.valueOf(JellyConstants.MASSA));

        //creo un pesce sinistro o destro
        //Si inizia il gioco con un solo pesce
        boolean new_sx = Math.random() < 0.5;
        createNewFish(this, new_sx, secondLayout.getChildCount() - 1);//TODO ricontrollare: index si potrebbe mettere direttamente nel metodo e non come parametro
        if(deviceConnected){
            connectedToDevice(device);//non ci servono info sul device connesso in quest activity
        }
        /*
        //se il tag è twoFish, si deve creare un secondo pesce
        String fishNum = prefs.getString("fishNumber", getString(R.string.twoFish));
        fishNumberButton.setTag(fishNum);
        if (fishNumberButton.getTag() == getString(R.string.twoFish)) {
            ImageView fish_dx = createNewFish(this, false, secondLayout.getChildCount() - 1);
            //animateFish(this, fish_dx);
        }else{//cambio il bottone del numero dei pesci e lo setto a 1 (di default è infatti settato a 2 pesci)
            onChangeFishNumber(fishNumberButton);
        }
        */
        counter = 0;
        seconds = 0;
        score = 0;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        SharedPreferences prefs = this.getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("viewpager_position", MainActivity.getDefaultTabIndex());
        //editor.putString("fishNumber", fishNumberButton.getTag().toString());
        editor.commit();
        setFinalScore();

        //cancello i runnable delle animazioni dei pesci
        int n = secondLayout.getChildCount();
        for (int i = 0; i < n; i++) {
            View child = secondLayout.getChildAt(i);
            if (child instanceof ImageView){
                child.animate().cancel();
            }
        }
        //fermo l'eventuale animazione di jelly morto
        jellyDiedIv.setAnimation(null);

        //fermo i task in background per il salvataggio dello sfondo nelle cache
        /*ArrayList<Sfondo.BitmapWorkerTask> sfondoTask = gameView.getSfondo().getSfondoTasks();
        for (int i = 0; i < sfondoTask.size(); i++){
            sfondoTask.get(i).cancel(true);
        }*/
    }

    @Override
    protected void onPause() {
        super.onPause();
        /*if (BluetoothActivity.class.isAssignableFrom((this.getClass()))){
            this.disconnect();
        }*/
        gameView.surfaceDestroyed(gameView.getHolder());
        gameOver = true;
        onBackPressed();
        //rimuovo l'impostazione che manteneva la luminosità dello schermo attiva
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setFinalScore();
        //mMemoryCache.evictAll();//svuoto le precedenti cache
    }

    @Override
    public void deviceFound(BluetoothDevice device) {
        //in questa activity se trovo un device, devo controllare se è quello che ho memorizzato, se lo è, mi connetto, altrimenti nulla
        if (savedDeviceAddress != null && savedDeviceAddress.equals(device.getAddress()) && device.getName() != null && device.getName().startsWith("UBA")){
            mb.connectToDevice(device);
        }
    }

    @Override
    public void deviceAlreadyConnected(BluetoothDevice mDevice) {
        deviceConnected = true;
        this.device = mDevice;
    }

    @Override
    public void deviceNotAlreadyConnected() {
        deviceConnected = false;
    }

    @Override
    public void connectedToDevice(BluetoothDevice device) {
        deviceConnected = true;
        JellyConstants.DEVICE_CONNECTED = true;
        final JellyfishActivity jellyActivity = mainActivity;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // devo nascondere la rotellina di attesa
                findViewById(R.id.progressBar).setVisibility(View.GONE);
                //faccio partire le animazioni dei pesci
                ImageView fish_sx = secondLayout.findViewWithTag("fish_sx");
                if(fish_sx != null){
                    animateFish(jellyActivity, fish_sx);
                }
                ImageView fish_dx = secondLayout.findViewWithTag("fish_dx");
                if(fish_dx != null) {
                    animateFish(jellyActivity, fish_dx);
                }
            }
        });
    }

    @Override
    public void noBluetooth() {

    }

    @Override
    public void pressioneChanged(float pressioneLorda, float temperatura, float pressioneNetta) {
        //l'accelerazione totale è data da (-forzaPressione + forzaPeso)/massa ==> -(pressione/massa) + g
        //double a = -pressioneNetta / JellyConstants.MASSA + JellyConstants.G;
        if (gameOver){
            return;
        }
        double a = JellyConstants.G_PIXEL * (1 - (pressioneNetta / JellyConstants.MASSA) );
        gameView.setJellyAcceleration(a);
        gameView.setJellyInterval(interval);

        //aggiorno i parametri di tempo, pressione nelle view del secondLayout ogni secondo
        if(counter % 8 == 1) {
            this.updateGameParameter(pressioneNetta, minDelta, counter);
            minDelta = 0;
        }
        minDelta = Math.max(minDelta, Math.abs(pressioneNetta - JellyConstants.SOGLIA_COMPENSAZIONE));
        counter ++;
    }

    @Override
    public void batteriaChanged(long batteria) {
        //TODO se livello batteria è al di sotto di una certa soglia, mandare avviso?
    }

    @Override
    public void disconnectedFromDevice() {
        deviceConnected = false;
    }

}
