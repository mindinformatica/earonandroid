package mindinformatica.com.earon.impostazioni;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;

import mindinformatica.com.earon.R;

/**
 * Created by camillacecconi on 17/07/17.
 */

public class SoglieActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_soglie);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.activity_soglie_title);
    }

    @Override
    protected void onPause() {
        super.onPause();
        EditText sogliaEt = findViewById(R.id.soglia_et);
        EditText limsupEt = findViewById(R.id.limsup_et);
        EditText liminfEt = findViewById(R.id.liminf_et);
        SharedPreferences prefs = this.getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("soglia", sogliaEt.getText().toString());
        editor.putString("limsup", limsupEt.getText().toString());
        editor.putString("liminf", liminfEt.getText().toString());
        editor.commit();

    }
    @Override
    protected void onResume() {
        super.onResume();
        EditText sogliaEt = findViewById(R.id.soglia_et);
        EditText limsupEt = findViewById(R.id.limsup_et);
        EditText liminfEt = findViewById(R.id.liminf_et);
        SharedPreferences prefs = this.getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);

        sogliaEt.setText(prefs.getString("soglia", ""));
        limsupEt.setText(prefs.getString("limsup", ""));
        liminfEt.setText(prefs.getString("liminf", ""));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            changeColorStatusBar(R.color.colorPrimary);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public void changeColorStatusBar(int colorId){
        Window window = this.getWindow();
        /*window.setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        );*/
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(this, colorId));
    }

}
