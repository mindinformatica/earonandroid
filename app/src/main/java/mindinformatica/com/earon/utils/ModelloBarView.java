package mindinformatica.com.earon.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.graphics.RectF;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;

import mindinformatica.com.earon.R;

/**
 * Created by camillacecconi on 18/07/17.
 */

public class ModelloBarView extends LinearLayout implements View.OnTouchListener {

    public abstract static class OnBarSegmentClickListener{
        public OnBarSegmentClickListener() {
        }

        public abstract void cliccato(Integer indice);
    }
    public OnBarSegmentClickListener listenerPezzetto;
    public ModelloBarView (Context context) {
        super(context);
        init(context, null, 0);
    }
    public ModelloBarView (Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs, 0);
    }
    public ModelloBarView (Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init(context, attrs, defStyle);
    }

    ArrayList<Integer> intervalli;
    boolean active = false;
    View toPassClick;
    //float percentage = 0;

    public ArrayList<Integer> getIntervalli(){
        return this.intervalli;
    }

    public int getDurata(){
        int toRet = 0;
        for (int intervallo : this.intervalli){
            toRet = toRet + intervallo;
        }
        return toRet;
    }
    public void setIntervalli(ArrayList<Integer> intervalli) {
        this.setBackgroundColor(getResources().getColor(R.color.redbutton));
        this.removeAllViews();
        for (int i = 0; i < intervalli.size(); i++){
            Integer interv = intervalli.get(i);
            Button b = new Button(getContext());
            b.setBackgroundColor((i%2==0)?getResources().getColor(R.color.soffiocolor):getResources().getColor(R.color.pausecolor));
            b.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, interv));
            b.setTag(i);
            b.setOnTouchListener(this);
            this.addView(b);
        }
        this.intervalli = intervalli;
        //this.invalidate();
    }
    public void addIntervallo(Integer intervallo) {
        this.intervalli.add(intervallo);
        int altezza = this.getHeight();
        Button b = new Button(getContext());
        b.setPressed(true);
        b.setBackground((this.intervalli.size()%2==1)?getResources().getDrawable(R.drawable.soffiodrawable):getResources().getDrawable(R.drawable.pausedrawable));
        b.setLayoutParams(new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.MATCH_PARENT, intervallo));
        b.setTag(this.intervalli.size()-1);
        b.setOnTouchListener(this);
        this.addView(b);
       // this.invalidate();
    }
    public void removeIntervallo() {
        //cancello l'ultimo intervallo
        intervalli.remove(intervalli.size()-1);
        this.removeViewAt(this.getChildCount()-1);

    }

    public void setActive(boolean active, View toPassClick){
        this.toPassClick = toPassClick;
        this.active = active;
        for (int i = 0; i < this.getChildCount(); i++){
            this.getChildAt(i).setClickable(active);
            this.getChildAt(i).setEnabled(active);
        }
    }
    public void setSegmentClickListener(OnBarSegmentClickListener list){
        this.listenerPezzetto = list;
    }
    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {
        evidenziaBackground(view);
        if (this.active){
            //quando clicco su un pezzetto, quello che dovrò fare è settare il number_picker al suo valore.
            int index = (int)view.getTag();
            if (index < intervalli.size()){
                Integer intervallo = this.intervalli.get(index);
                if (listenerPezzetto != null){

                    listenerPezzetto.cliccato(index);
                }
            }


        }else{
            if (toPassClick != null)
                toPassClick.performClick();
        }
        return true;
    }

    private void evidenziaBackground(View view) {
        int pezzi = this.getChildCount();
        for (int i = 0; i < pezzi; i++){
            View v = this.getChildAt(i);
            if (v.isPressed()){
                if (i%2==0){
                    v.setBackground(getResources().getDrawable(R.drawable.soffiodrawable));
                }else{
                    v.setBackground(getResources().getDrawable(R.drawable.pausedrawable));
                }
                v.setPressed(false);
            }
        }
        Log.i("Selezionato", Integer.toString((int)view.getTag()));
        view.setPressed(true);
        if (((int)view.getTag())%2 == 0){
            view.setBackground(getResources().getDrawable(R.drawable.soffiodrawable_selected));
        }else{
            view.setBackground(getResources().getDrawable(R.drawable.pausedrawable_selected));
        }

    }


    /**
     * Tutto il codice a seguire serve per stondare i bordi della barra
     */
    private final static float CORNER_RADIUS = 40.0f;
    private Bitmap maskBitmap;
    private Paint paint, maskPaint;
    private float cornerRadius;

    private void init(Context context, AttributeSet attrs, int defStyle) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        cornerRadius = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, CORNER_RADIUS, metrics);

        paint = new Paint(Paint.ANTI_ALIAS_FLAG);

        maskPaint = new Paint(Paint.ANTI_ALIAS_FLAG | Paint.FILTER_BITMAP_FLAG);
        maskPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));

        setWillNotDraw(false);
    }

    @Override
    public void draw(Canvas canvas) {
        if(canvas.getWidth() > 0 && canvas.getHeight() > 0) {
            Bitmap offscreenBitmap = Bitmap.createBitmap(canvas.getWidth(), canvas.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas offscreenCanvas = new Canvas(offscreenBitmap);

            super.draw(offscreenCanvas);

            if (maskBitmap == null) {
                maskBitmap = createMask(canvas.getWidth(), canvas.getHeight());
            }

            offscreenCanvas.drawBitmap(maskBitmap, 0f, 0f, maskPaint);
            canvas.drawBitmap(offscreenBitmap, 0f, 0f, paint);
        }
    }

    private Bitmap createMask(int width, int height) {
        Bitmap mask = Bitmap.createBitmap(width, height, Bitmap.Config.ALPHA_8);
        Canvas canvas = new Canvas(mask);

        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setColor(Color.WHITE);

        canvas.drawRect(0, 0, width, height, paint);

        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.CLEAR));
        canvas.drawRoundRect(new RectF(0, 0, width, height), cornerRadius, cornerRadius, paint);

        return mask;
    }
    /**
     * FINE codice per stondare la barra
     */
}
