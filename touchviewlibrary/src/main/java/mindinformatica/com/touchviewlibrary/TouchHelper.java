package mindinformatica.com.touchviewlibrary;
import android.view.MotionEvent;

public class TouchHelper {
	private final int mNumeroDita;
	private final Vettore2D mPunti[];
	private final Vettore2D mPuntiPrecedenti[];
	
	private long momentoClick;
	private Vettore2D posizioneClick;
	private static final int MAX_CLICK_DURATION = 1000;
	private float MAX_CLICK_DISTANCE;
	private boolean isClick = false;
	

	public TouchHelper(final int numeroDita, float density) {
		mNumeroDita = numeroDita;
		mPunti = new Vettore2D[numeroDita];
		mPuntiPrecedenti = new Vettore2D[numeroDita];
		MAX_CLICK_DISTANCE = 20*density;
	}
	
	public boolean isPremuto(final int i) {
		return mPunti[i]!=null;
	}
	
	/**
	 * Quante dita risultano premute?
	 * @return
	 */
	public int getDitaPremute() {
		int result = 0;
		
		for(int i=0; i<mPunti.length; i++) {
			if ( isPremuto(i) ) result += 1;
		}
		
		return result;
	}
	
	/**
	 * Calcola quanto si e' mosso il dito "i"
	 * @param i
	 * @return
	 */
	public Vettore2D deltaMovimento(int i) {
		if ( isPremuto(i) ) {
			
			Vettore2D precedente = mPuntiPrecedenti[i]!=null?mPuntiPrecedenti[i]:mPunti[i];
			Vettore2D risultato =  Vettore2D.sottrai(mPunti[i], precedente);
				return risultato;
			
		} else {
			return new Vettore2D(0, 0);
		}
	}
	
	/**
	 * La posizione del dito "i"
	 * @param i
	 * @return
	 */
	public Vettore2D getPosizione(int i) {
		Vettore2D risultato = mPunti[i];
		if ( risultato==null ) {
			risultato = new Vettore2D(0,0);
		}
		return risultato;
	}
	
	/**
	 * La posizione precedente del dito "i"
	 * @param i
	 * @return
	 */
	public Vettore2D getPosizionePrecedente(int i) {
		Vettore2D risultato = mPuntiPrecedenti[i];
		if ( risultato==null ) {
			risultato = new Vettore2D(0,0);
		}
		return risultato;
	}
	
	/**
	 * Gestione degli eventi
	 * @param event
	 */
	public void processaEvento(MotionEvent event) {
		int tipo = event.getAction() & MotionEvent.ACTION_MASK;
		
		if ( tipo== MotionEvent.ACTION_POINTER_UP || tipo== MotionEvent.ACTION_UP ) {
			long durata = System.currentTimeMillis() - momentoClick;
			Vettore2D posizioneFine = new Vettore2D(event.getX(0), event.getY(0));
			posizioneFine = Vettore2D.sottrai(posizioneFine, posizioneClick);
			if (durata < MAX_CLICK_DURATION  && posizioneFine.getLength() < MAX_CLICK_DISTANCE){
				setClick(true);
			}else{
				setClick(false);
			}
			gestisciDitoRilasciato(event);
		} else {
			if ( tipo== MotionEvent.ACTION_POINTER_DOWN || tipo== MotionEvent.ACTION_DOWN ) {
				momentoClick = System.currentTimeMillis();
				posizioneClick = new Vettore2D(event.getX(0), event.getY(0));
			}
			gestisciDitoPremuto(event);
		}
	}
	
	private void gestisciDitoRilasciato(MotionEvent event) {
		
		for ( int i=0; i<event.getPointerCount(); i++ ) {
			int idx = event.getPointerId(i);
			mPuntiPrecedenti[idx] = null;
			mPunti[idx] = null;
		}
	}
	
	private void gestisciDitoPremuto(MotionEvent event) {
		for( int i=0; i<mNumeroDita; i++ ) {
			// Ho meno dita premute
			if ( i>=event.getPointerCount() ) {
				mPunti[i] = null;
				mPuntiPrecedenti[i] = null;
			}
			
			// Questo e' un dito buono
			if ( i<event.getPointerCount() ) {
				int idPuntatore; 
				Vettore2D posizione;
				
				try {
					idPuntatore = event.getPointerId(i);
					posizione = new Vettore2D(event.getX(idPuntatore), event.getY(idPuntatore));
				} catch(IllegalArgumentException iae) {
					// le cose cambiano mentre le prendiamo!
					return;
				}
				
				if ( mPunti[idPuntatore]==null ) {
					// Prima non era premuto
					mPunti[idPuntatore]=posizione;
				} else {
					// Prima era premuto... mi aggiorno la storia
					if ( mPuntiPrecedenti[idPuntatore]!=null ) {
						mPuntiPrecedenti[idPuntatore].set(mPunti[idPuntatore]);
					} else {
						mPuntiPrecedenti[idPuntatore] = new Vettore2D(posizione);
					}
					
					// Mi proteggo contro gli spurii... magari scappa
					if ( Vettore2D.sottrai(mPunti[idPuntatore], posizione).getLength() < 64 ) {
						mPunti[idPuntatore].set(posizione);
					}
				}
			}
		}
	}

	public boolean wasClick() {
		boolean res = isClick;
		this.isClick = false;
		return res;
		
	}

	public void setClick(boolean isClick) {
		this.isClick = isClick;
	}
}























//package com.mindInformatica.apptc20.touchimage;
//
//import android.view.MotionEvent;
//
//public class TouchHelper {
//	private final int mNumeroDita;
//	private final Vettore2D mPunti[];
//	private final Vettore2D mPuntiPrecedenti[];
//	
//	public TouchHelper(final int numeroDita) {
//		mNumeroDita = numeroDita;
//		mPunti = new Vettore2D[numeroDita];
//		mPuntiPrecedenti = new Vettore2D[numeroDita];
//	}
//	
//	public boolean isPremuto(final int i) {
//		return mPunti[i]!=null;
//	}
//	
//	/**
//	 * Quante dita risultano premute?
//	 * @return
//	 */
//	public int getDitaPremute() {
//		int result = 0;
//		
//		for(int i=0; i<mPunti.length; i++) {
//			if ( isPremuto(i) ) result += 1;
//		}
//		
//		return result;
//	}
//	
//	/**
//	 * Calcola quanto si e' mosso il dito "i"
//	 * @param i
//	 * @return
//	 */
//	public Vettore2D deltaMovimento(int i) {
//		if ( isPremuto(i) ) {
//			Vettore2D precedente = mPuntiPrecedenti[i]!=null?mPuntiPrecedenti[i]:mPunti[i];
//			Vettore2D risultato =  Vettore2D.sottrai(mPunti[i], precedente);
//			return risultato;
//		} else {
//			return new Vettore2D(0, 0);
//		}
//	}
//	
//	/**
//	 * La posizione del dito "i"
//	 * @param i
//	 * @return
//	 */
//	public Vettore2D getPosizione(int i) {
//		Vettore2D risultato = mPunti[i];
//		if ( risultato==null ) {
//			risultato = new Vettore2D(0,0);
//		}
//		return risultato;
//	}
//	
//	/**
//	 * La posizione precedente del dito "i"
//	 * @param i
//	 * @return
//	 */
//	public Vettore2D getPosizionePrecedente(int i) {
//		Vettore2D risultato = mPuntiPrecedenti[i];
//		if ( risultato==null ) {
//			risultato = new Vettore2D(0,0);
//		}
//		return risultato;
//	}
//	
//	/**
//	 * Gestione degli eventi
//	 * @param event
//	 */
//	public void processaEvento(MotionEvent event) {
//		int tipo = event.getAction() & MotionEvent.ACTION_MASK;
//		
//		if ( tipo==MotionEvent.ACTION_POINTER_UP || tipo==MotionEvent.ACTION_UP ) {
//			gestisciDitoRilasciato(event);
//		} else {
//			gestisciDitoPremuto(event);
//		}
//	}
//	
//	private void gestisciDitoRilasciato(MotionEvent event) {
//		for ( int i=0; i<event.getPointerCount(); i++ ) {
//			int idx = event.getPointerId(i);
//			mPuntiPrecedenti[idx] = null;
//			mPunti[idx] = null;
//		}
//	}
//	
//	private void gestisciDitoPremuto(MotionEvent event) {
//		for( int i=0; i<mNumeroDita; i++ ) {
//			// Ho meno dita premute
//			if ( i>=event.getPointerCount() ) {
//				mPunti[i] = null;
//				mPuntiPrecedenti[i] = null;
//			}
//			
//			// Questo e' un dito buono
//			if ( i<event.getPointerCount() ) {
//				
//				int idPuntatore; 
//				Vettore2D posizione;
//				
//				try {
//					idPuntatore = event.getPointerId(i);
//					posizione = new Vettore2D(event.getX(idPuntatore), event.getY(idPuntatore));
//				} catch(IllegalArgumentException iae) {
//					// le cose cambiano mentre le prendiamo!
//					return;
//				}
//				
//				if ( mPunti[idPuntatore]==null ) {
//					// Prima non era premuto
//					mPunti[idPuntatore]=posizione;
//				} else {
//					// Prima era premuto... mi aggiorno la storia
//					if ( mPuntiPrecedenti[idPuntatore]!=null ) {
//						mPuntiPrecedenti[idPuntatore].set(mPunti[idPuntatore]);
//					} else {
//						mPuntiPrecedenti[idPuntatore] = new Vettore2D(posizione);
//					}
//					
//					// Mi proteggo contro gli spurii... magari scappa
//					if ( Vettore2D.sottrai(mPunti[idPuntatore], posizione).getLength() < 64 ) {
//						mPunti[idPuntatore].set(posizione);
//					}
//				}
//			}
//		}
//	}
//}
