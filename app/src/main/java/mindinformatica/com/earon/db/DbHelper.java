package mindinformatica.com.earon.db;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;

import mindinformatica.com.earon.rec.Sessione;

/**
 * Created by camillacecconi on 21/07/17.
 */

public class DbHelper extends SQLiteOpenHelper {
    /** Schema version. */
    public static final int DATABASE_VERSION = 6;
    /** Filename for SQLite file. */
    public static final String DATABASE_NAME = "earon.db";

    final TableInterface[] entryList;


    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        entryList = new TableInterface[]{new Sessione(context), new Modello()};
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        for (int i = 0; i < entryList.length; i++){
            sqLiteDatabase.execSQL(entryList[i].getCreateString());
        }

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        // NON POSSO ELIMINARE TUTTO, perché devo tenere i dati
        if (oldVersion <= 4 && newVersion >= 5){
            sqLiteDatabase.execSQL(entryList[1].getCreateString());
        }
        if (oldVersion <= 5 && newVersion >= 6){
            //aggiunta colonna is_imported
            try{
                Cursor cursor = sqLiteDatabase.rawQuery("SELECT " + Sessione.COLUMN_IS_IMPORTED + " from " + Sessione.TABLE_NAME + ";", null);
                cursor.close();
            }catch (SQLiteException e){
                sqLiteDatabase.execSQL("ALTER TABLE " + Sessione.TABLE_NAME + " ADD COLUMN  " + Sessione.COLUMN_IS_IMPORTED + " " + Sessione.TYPE_IS_IMPORTED + " DEFAULT 0;");
            }
            //aggiunta colonna data Fine Sessione
            try{
                Cursor cursor = sqLiteDatabase.rawQuery("SELECT " + Sessione.COLUMN_DATA_FINE + " from " + Sessione.TABLE_NAME + ";", null);
                cursor.close();
            }catch (SQLiteException e){
                sqLiteDatabase.execSQL("ALTER TABLE " + Sessione.TABLE_NAME + " ADD COLUMN  " + Sessione.COLUMN_DATA_FINE + " " + Sessione.TYPE_DATA_FINE);
            }
            try{
                Cursor cursor = sqLiteDatabase.rawQuery("SELECT " + Sessione.COLUMN_DESCRIZIONE_SESSIONE + " from " + Sessione.TABLE_NAME + ";", null);
                cursor.close();
            }catch (SQLiteException e){
                sqLiteDatabase.execSQL("ALTER TABLE " + Sessione.TABLE_NAME + " ADD COLUMN  " + Sessione.COLUMN_DESCRIZIONE_SESSIONE + " " + Sessione.TYPE_DESCRIZIONE_SESSIONE);
            }
            try{
                Cursor cursor = sqLiteDatabase.rawQuery("SELECT " + Sessione.COLUMN_DEVICE_NAME + " from " + Sessione.TABLE_NAME + ";", null);
                cursor.close();
            }catch (SQLiteException e){
                sqLiteDatabase.execSQL("ALTER TABLE " + Sessione.TABLE_NAME + " ADD COLUMN  " + Sessione.COLUMN_DEVICE_NAME + " " + Sessione.TYPE_DEVICE_NAME);
            }
        }
        /*int max = entryList.length;
        for (int j = 1; j <= max; j++){
            sqLiteDatabase.execSQL(entryList[max-j].getDropString());
        }
        for (int k = 0; k < entryList.length; k++){
            sqLiteDatabase.execSQL(entryList[k].getCreateString());
        }*/
    }
}



