package mindinformatica.com.earon.tutorial;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.FrameLayout;

import androidx.annotation.Nullable;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import mindinformatica.com.earon.R;
import mindinformatica.com.earon.bluetooth.BluetoothActivity;

/**
 * Created by camillacecconi on 11/07/17.
 */

public class FragmentTutorial extends Fragment {
    WebView webview;

    private AppBarLayout mAppBarView;
    private FrameLayout mTargetView;
    private FrameLayout mContentView;
    private WebChromeClient.CustomViewCallback mCustomViewCallback;
    private View mCustomView;
    private MyWebChromeClient mClient;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_tutorial, container, false);
        webview = rootView.findViewById(R.id.webview_tutorial);

        WebSettings webSettings = webview.getSettings();
        webSettings.setPluginState(WebSettings.PluginState.ON);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        webSettings.setSupportZoom(true);
        webview.setWebViewClient(new WebViewClient());
        mClient  = new MyWebChromeClient();
        webview.setWebChromeClient(mClient);

        if (savedInstanceState != null){
            //webview.restoreState(savedInstanceState);
            webview.onResume();
        }
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mContentView = view.findViewById(R.id.main_content_tutorial);
        mAppBarView = getActivity().findViewById(R.id.appbar);
        mTargetView = getActivity().findViewById(R.id.full_page_view);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null && BluetoothActivity.class.isAssignableFrom((getActivity().getClass()))&& getUserVisibleHint()){
            if (webview.getUrl() == null){
                String url = getActivity().getResources().getString(R.string.no_tutorial_url);
                Log.i("WEBVIEW", url);
                webview.loadUrl(url);
            }
            //((BluetoothActivity)getActivity()).scan(false); //inizio con uno scan, se non lo trovo, dopo un pochino mi fermo
            FloatingActionButton fab = (FloatingActionButton) getActivity().findViewById(R.id.fab);
            fab.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() != null && BluetoothActivity.class.isAssignableFrom((getActivity().getClass()))){
           // ((BluetoothActivity)getActivity()).disconnect();
        }
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getActivity() != null && BluetoothActivity.class.isAssignableFrom((getActivity().getClass()))){
            BluetoothActivity ac = ((BluetoothActivity)getActivity());
            if (isVisibleToUser){
                //ac.scan(false);//inizio con uno scan, se non lo trovo, dopo un pochino mi fermo
                /*String url = getActivity().getResources().getString(R.string.no_tutorial_url);
                Log.i("WEBVIEW", url);
                if (webview != null){
                    if (webview.getUrl() == null){
                        webview.loadUrl(url);
                    }else{
                        webview.onResume();
                    }
                }*/
                FloatingActionButton fab = getActivity().findViewById(R.id.fab);
                fab.setVisibility(View.INVISIBLE);
            }else{
                if (webview != null){
                    //webview.loadUrl(null);
                    webview.onPause();
                }
                //ac.disconnect();
                /*if (webview != null){
                    webview.loadUrl(null);
                    webview.onPause();
                }*/
            }
        }

    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (webview != null){
            webview.onPause();
            webview.saveState(outState);
        }

    }

    private class MyWebChromeClient extends WebChromeClient {
        FrameLayout.LayoutParams LayoutParameters = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT,
                FrameLayout.LayoutParams.MATCH_PARENT);

        @Override
        public void onShowCustomView(View view, CustomViewCallback callback) {
            WindowManager.LayoutParams attrs = getActivity().getWindow().getAttributes();
            attrs.flags |= WindowManager.LayoutParams.FLAG_FULLSCREEN;
            attrs.flags |= WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
            getActivity().getWindow().setAttributes(attrs);
            if (android.os.Build.VERSION.SDK_INT >= 14)
            {
                getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE);
            }
            mCustomViewCallback = callback;
            mTargetView.addView(view);
            mCustomView = view;
            mContentView.setVisibility(View.GONE);
            mAppBarView.setVisibility(View.GONE);
            mTargetView.setVisibility(View.VISIBLE);
            mTargetView.bringToFront();
        }

        @Override
        public void onHideCustomView() {
            if (mCustomView == null)
                return;

            WindowManager.LayoutParams attrs = getActivity().getWindow().getAttributes();
            attrs.flags &= ~WindowManager.LayoutParams.FLAG_FULLSCREEN;
            attrs.flags &= ~WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON;
            getActivity().getWindow().setAttributes(attrs);
            if (android.os.Build.VERSION.SDK_INT >= 14)
            {
                getActivity().getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_VISIBLE);
            }

            mCustomView.setVisibility(View.GONE);
            mTargetView.removeView(mCustomView);
            mCustomView = null;
            mTargetView.setVisibility(View.GONE);
            mCustomViewCallback.onCustomViewHidden();
            mContentView.setVisibility(View.VISIBLE);
            mAppBarView.setVisibility(View.VISIBLE);
        }

        @Override
        public void onCloseWindow(WebView window) {
            super.onCloseWindow(window);
        }

        public void onProgressChanged(WebView view, int progress) {
            /*Activity a = getActivity();
            if (a != null){
                a.setProgress(progress * 100);
                if(progress >= 85)
                    dialog.dismiss();
            }else{
                if (dialog != null && dialog.isShowing()){
                    dialog.dismiss();
                }
            }*/
        }

    }
}
