package mindinformatica.com.earon.jellyfish;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;

import mindinformatica.com.earon.R;

import static mindinformatica.com.earon.jellyfish.JellyfishActivity.gameOver;

/**
 * classe per disegnare la medusa
 * Created by lorenzo on 17/07/18.
 */

public class Jellyfish {
    private final double NORMALIZE_COSTANT;
    public Bitmap image; //immagine di jelly
    private final int screenHeight = JellyConstants.SCREEN_HEIGHT;
    private int x,y;//variabili per la posizione
    private int paddingLeft, paddingRigth, paddingTop, paddingBottom;//padding che servono per calcolare le collisioni senza contare i pixel vuoti del bordo immagine
    private double yVelocity = 0;
    private double acceleration = 0;
    private double interval; //intervallo della legge oraria del moto di jelly
    private final int bottomY;//posizione di jelly sul fondale
    boolean isDying;//variabile per stabilire se jelly sta morendo perchè passa troppo tempo sul fondale
    private BitmapFactory.Options options;

    private String nameJelly = "medusa";
    private int i = 0;
    private Context context;
    private long time;

    public Jellyfish(Context context) {
        this.context = context;
        options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(context.getResources(), R.drawable.medusa0, options);
        // Calculate inSampleSize
        double jellyRatio = (double)options.outWidth / options.outHeight;
        options.inSampleSize = JellyConstants.calculateInSampleSize(options, JellyConstants.SCREEN_WIDTH,JellyConstants.SCREEN_HEIGHT);
        options.inJustDecodeBounds = false;

        Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), R.drawable.medusa0, options);
        //ricavo il rapporto larghezza / altezza del drawable per mantenere la proporzione nell'immagine scalata
        //double jellyRatio = (double)bmp.getWidth() / bmp.getHeight();
        //setto le costanti delle dimensioni di jelly
        JellyConstants.JELLY_WIDTH = JellyConstants.SCREEN_WIDTH / 5;
        JellyConstants.JELLY_HEIGHT = (int) (JellyConstants.JELLY_WIDTH / jellyRatio);
        image = Bitmap.createScaledBitmap(bmp,JellyConstants.JELLY_WIDTH,JellyConstants.JELLY_HEIGHT,false);
        //setto la posizione iniziale di jelly
        x = JellyConstants.SCREEN_WIDTH / 2 - image.getWidth() / 2;
        y = screenHeight - image.getHeight();
        bottomY = y;

        paddingRigth = paddingLeft = paddingBottom = paddingTop = 0;
        setImagePadding(image);

        isDying = false;

        //NORMALIZE_COSTANT = ((double) JellyConstants.STANDARD_HEIGHT/JellyConstants.SCREEN_HEIGHT);
        NORMALIZE_COSTANT = ((double) JellyConstants.SCREEN_HEIGHT / JellyConstants.STANDARD_HEIGHT);//con tale costante velocizzo medusa per schermi che hanno SCREEN_HEIGHT maggiore di STANDARD_HEIGHT

        //Thread per stabilire se Jelly sta troppo sul fondale e muore (tempo massimo 5 secondi)
        Thread thread = new Thread() {
            @Override
            public void run() {
                long oldTime = System.currentTimeMillis();
                long currentTime;
                try {
                    while(!gameOver) {
                        sleep(20);
                        if(JellyConstants.DEVICE_CONNECTED) {
                            currentTime = System.currentTimeMillis();
                            if (bottomY - 10 <= y) {
                                if (currentTime - oldTime > 5000) {
                                    isDying = true;
                                }
                            } else {
                                isDying = false;
                                oldTime = System.currentTimeMillis();
                            }
                        }else{
                            oldTime = System.currentTimeMillis();
                        }
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    /**
     *
     * @return le option per ottimizzare il caricamento dell'immagine
     */
    public BitmapFactory.Options getOptions(){
        return options;
    }

    /**
     * Disegna la medusa
     * @param canvas la canvas su cui va disegnata la medusa
     */
    public void draw(Canvas canvas) {
        canvas.drawBitmap(image, x, y, null);
    }

    /**
     * Aggiorno la posizione e il frame della medusa:
     *   Uso la legge oraria:
     *      y = y0 + v0*t +1/2a*t^2
     *      v = v0 + a*t
     *   Se la nuova posizione va oltre la base dello schermo, setto la velocità a zero e la posizione al massimo possibile (ovvero medusa ferma alla base)
     *   Altrimenti, imposto la nuova velocità e nuova posizione
     *   Come intervallo di tempo si calcola la differenza tra il tempo dell'attuale esecuzione del metodo e di quello precedente (la prima volta si sfrutta il 1/targetFPS
     */
    public void update(){
        updateJellyDrawable();
        if (time != 0) {
            interval = (System.nanoTime() - time) / (1e6*1000);
        }
        int tempY = (int) (y + (yVelocity*interval + 0.5 * acceleration * interval*interval) * NORMALIZE_COSTANT);

        if(tempY >= screenHeight - image.getHeight()){
            y = screenHeight - image.getHeight();
            yVelocity = 0;
        }else{
            y = tempY;
            yVelocity = yVelocity + acceleration * interval;
        }
        time = System.nanoTime();
    }

    /**
     * Aggiorno il frame della medusa
     * Valuto se va considerata la medusa morta o meno
     */
    private void updateJellyDrawable(){
        i = i == 49 ? 0 : i + 1;
        if(this.isDying) {
            if(i == 49 && nameJelly.endsWith("morta")){
                gameOver = true;
            }
            if (i == 0) {
                nameJelly = "medusa_morta";
            }
        }else{
            nameJelly = "medusa";
        }

        //Log.d("JELLYFISH", "i: " + i);
        setJellyImage(nameJelly + i);
    }

    /**
     * Carica l'immagine della medusa usando le opzioni ottimizzate
     * @param stringName il nome della risorsa da prendere
     */
    public void setJellyImage(String stringName){
        if(image != null) {
            image.recycle();
            image = null;
        }
        System.gc();
        int id = context.getResources().getIdentifier(stringName, "drawable", context.getPackageName());
        Bitmap bmp = BitmapFactory.decodeResource(context.getResources(), id, getOptions());
        image = Bitmap.createScaledBitmap(bmp,JellyConstants.JELLY_WIDTH,JellyConstants.JELLY_HEIGHT,false);
    }

    public int getBottomY(){
        return bottomY;
    }

    public int getY(){
        return y;
    }

    public int getX(){
        return x;
    }

    public void setAcceleration(double a) {
        this.acceleration = a;
    }

    public Bitmap getJellyImage(){
        return image;
    }

    public void setInterval(double interval) {
        this.interval = interval;
    }

    /**
     *  vengono tolti dei padding per eliminare i bordi vuoti dell'immagine
     * @return il Rect in cui si trova Jelly
     */
    public Rect getJellyRect(){
        return new Rect(x + paddingLeft, y + paddingTop,x + image.getWidth() - paddingRigth,y + image.getHeight() - paddingBottom);
    }

    /**
     * calcola i padding per jelly (versione semplice: dall'alto per Y e in posizione centrale per X, si cerca il primo pixel nero e si rende tale valore)
     */
    private void setImagePadding(Bitmap bitmap){
        //imposto paddingTop
        int bitmapX = JellyConstants.JELLY_WIDTH / 2;
        for (int i = 0; i < JellyConstants.JELLY_HEIGHT; i++) {
            int pixel = bitmap.getPixel(bitmapX, i);
            if(pixel != 0 && Color.red(pixel) == 0 && Color.blue(pixel) == 0 && Color.blue(pixel) == 0) {
                paddingTop = i;
                break;
            }
        }
        /*for (int j = 0; j < bitmap.getWidth(); j++) {
            for (int k = 0; k < bitmap.getHeight(); k++) {
                int pixel = bitmap.getPixel(j, k);
                if(Color.red(pixel) == 0) {
                    paddingLeft = j;
                    break;
                }
            }
        }*/
        paddingBottom = paddingTop / 2;
        paddingRigth = paddingLeft = paddingTop;
        paddingTop = paddingTop * 2;
    }

}
