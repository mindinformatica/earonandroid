package mindinformatica.com.earon.impostazioni;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.appcompat.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

import mindinformatica.com.earon.application.EaronApplication;
import mindinformatica.com.earon.bluetooth.BluetoothActivity;
import mindinformatica.com.earon.R;

/**
 * Created by camillacecconi on 13/07/17.
 */

public class ListaDevicesActivity extends BluetoothActivity {

    public ArrayAdapter<BluetoothDevice> listaAdapter;
    public ArrayList<BluetoothDevice> lista = new ArrayList<>();
    private boolean scanning = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.activity_lista_title);
        ListView lv = (ListView) findViewById(R.id.lv_devices);
        listaAdapter = new DeviceAdapter(this, android.R.layout.simple_list_item_1, lista);

        listaAdapter.setNotifyOnChange(true);
        lv.setAdapter(listaAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                BluetoothDevice d = lista.get(i);
                ((EaronApplication)getApplication()).connectToDevice(d);
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_lista_device, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_scan:
                if (scanning){
                    disconnect();
                    scanning = false;
                    item.setTitle(R.string.startscan);
                }else{
                    scan(false);
                    scanning = true;
                    item.setTitle(R.string.stopscan);
                }
                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        super.onPause();
        //disconnect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        //appena creo l'activty, inizio a cercare i dispositivi.
        scan(false); //in questa activity, non voglio interrompere mai lo scanning, a meno che non chiudo la ac
    }

    @Override
    public void deviceFound(BluetoothDevice device) {
        if (!lista.contains(device) && device.getName() != null && device.getName().startsWith("UBA")){
            lista.add(device);
            if (listaAdapter != null)
                listaAdapter.notifyDataSetChanged();
       }
    }

    @Override
    public void noBluetooth() {
        Toast.makeText(this, R.string.bluetooth_error, Toast.LENGTH_LONG);
    }

    @Override
    public void pressioneChanged(float pressioneLorda, float temperatura, float pressioneNetta) {
        Log.i("Lettore", "LISTA ACTIVITY PRESSIONE CAMBIATA");
    }

    @Override
    public void batteriaChanged(long batteria) {

    }

    @Override
    public void disconnectedFromDevice() {

    }

    @Override
    public void deviceAlreadyConnected(BluetoothDevice mDevice) {

    }

    @Override
    public void deviceNotAlreadyConnected() {

    }

    @Override
    public void connectedToDevice(final BluetoothDevice device) {
        //devo aggiungere il device alle preferences, poi
        SharedPreferences.Editor editor = this.getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE).edit();
        editor.putString("device", device.getAddress());
        editor.commit();
        //posso chiudere questa activity
        finish();
        //in teoria quando torno sul fragment, si connette, perché ora ha il device memorizzato. c'è solo da controllare che ricarichi le prefs.

    }

    private class DeviceAdapter extends ArrayAdapter<BluetoothDevice>{

        public DeviceAdapter(Context context, int resource, ArrayList<BluetoothDevice> objects) {
            super(context, resource, objects);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = LayoutInflater.from(getContext()).inflate(R.layout.device_list_item, null);//set layout for displaying items
            BluetoothDevice d = getItem(position);
            TextView textView = v.findViewById(R.id.device_name_tv);
            if (textView != null) {
                textView.setText(d.getName());
            }
            return v;

        }
    }
}
