package mindinformatica.com.earon.rec;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

import mindinformatica.com.earon.R;
import mindinformatica.com.earon.bluetooth.BluetoothActivity;
import mindinformatica.com.earon.db.Modello;
import mindinformatica.com.earon.utils.ModelloBarView;

/**
 * Created by camillacecconi on 18/07/17.
 */

public class ModelliActivity extends AppCompatActivity {
    private final int REQUEST_DETAIL = 1;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modelli);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.activity_modelli_title);

    }

    @Override
    protected void onResume() {
        super.onResume();
        ListView lv = (ListView)findViewById(R.id.lv_modelli);
        ArrayList<Modello> lista = Modello.getModelli(this);
        Modello m = getModelloDefault(this);
        lista.add(0, m);
        ModelliAdapter ad = new ModelliAdapter(lista);
        lv.setAdapter(ad);
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                BluetoothActivity.changeColorStatusBar(this, R.color.colorPrimary);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @NonNull
    public static Modello getModelloDefault(AppCompatActivity ac) {
        ArrayList<Integer> intervallo = new ArrayList<>();
        intervallo.add(120);
        Modello m = new Modello(intervallo, ac.getResources().getString(R.string.default_template_title));
        m.setId(-1l);
        return m;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_modelli, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_add_modello:
                Intent i = new Intent(this, ModelloDetailActivity.class);
                ArrayList<Integer> intervalliNuovi = new ArrayList<>();
                intervalliNuovi.add(30);
                i.putExtra("modello", intervalliNuovi);
                i.putExtra("titoloModello", getResources().getString(R.string.nuovo_template));
                i.putExtra("idModello", -2l);
                startActivityForResult(i, REQUEST_DETAIL);
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private class ModelliAdapter extends BaseAdapter {

        ArrayList<Modello> oggetti;

        public ModelliAdapter(ArrayList<Modello> oggetti) {
            this.oggetti = oggetti;
        }

        @Override
        public int getCount() {
            return this.oggetti.size();
        }

        @Override
        public Object getItem(int i) {
            return this.oggetti.get(i);
        }

        @Override
        public long getItemId(int i) {
            return this.oggetti.get(i).getId();
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View v = LayoutInflater.from(ModelliActivity.this).inflate(R.layout.modello_list_item, null);
            String titolo = this.oggetti.get(i).getNomeTemplate();
            ArrayList<Integer> intervalli = oggetti.get(i).getIntervalli();
            LinearLayout ll = v.findViewById(R.id.modello_list_item_container);
            ll.setTag(i); //uso il titolo come
            ll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //questo in pratica è quello che succede quando clicco su un item qualunque.
                    //quello che devo fare (qui) è selezionare questo modello come "scelto" e poi tornare all'activity di registrazione
                    Intent i = ModelliActivity.this.getIntent();
                    getIntent().putExtra("modello", oggetti.get((int)view.getTag()).getIntervalli());
                    getIntent().putExtra("titoloModello", oggetti.get((int)view.getTag()).getNomeTemplate());
                    ModelliActivity.this.setResult(RESULT_OK, i);
                    //le preference servono per settare il titolo del modello nella mainActivity
                    SharedPreferences.Editor editor = getSharedPreferences(
                            "com.example.app", Context.MODE_PRIVATE).edit();
                    editor.putString("titoloModello", oggetti.get((int)view.getTag()).getNomeTemplate());
                    editor.putInt("modelloId", (int)view.getTag());
                    editor.commit();
                    finish();
                }
            });
            ImageButton btn = v.findViewById(R.id.button_modello_detail);
            btn.setTag(i);
            btn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Modello m = oggetti.get((int)view.getTag());
                    Intent intent = new Intent(ModelliActivity.this, ModelloDetailActivity.class);
                    intent.putExtra("modello", oggetti.get((int)view.getTag()).getIntervalli());
                    intent.putExtra("titoloModello", oggetti.get((int)view.getTag()).getNomeTemplate());
                    intent.putExtra("idModello", oggetti.get((int)view.getTag()).getId());
                    startActivity(intent);

                }
            });
            TextView titoloTv = v.findViewById(R.id.modello_list_item_title);
            titoloTv.setText(titolo);
            TextView durata = v.findViewById(R.id.modello_list_item_durata);
            durata.setText(getTotalTime(intervalli));
            ModelloBarView barra = v.findViewById(R.id.modello_list_item_barra);
            barra.setIntervalli(intervalli);
            barra.setActive(false, ll);
            v.invalidate();
            return v;
        }


    }
    public static Integer getTotalTimeSec(ArrayList<Integer> lista) {
        Integer totale = 0;
        for (Integer valore : lista) {
            totale = totale + valore;
        }
        return totale;
    }
    public static String getTotalTime(ArrayList<Integer> lista){
        String tempo = DateUtils.formatElapsedTime(getTotalTimeSec(lista));
        return tempo;
    }
}
