package mindinformatica.com.earon.impostazioni;

import android.os.Build;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import mindinformatica.com.earon.R;
import mindinformatica.com.earon.bluetooth.BluetoothActivity;

/**
 * Created by camillacecconi on 17/07/17.
 */

public class InfoActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_info);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.activity_info_title);
       WebView mWebView = (WebView) findViewById(R.id.webview);

        // Abilita l'esecuzione del Javascript
        // anche se rappresenta un problema di sicurezza
        // (il cliente vuole cosi')
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setPluginState(WebSettings.PluginState.ON);
        webSettings.setJavaScriptEnabled(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        webSettings.setSupportZoom(true);
        mWebView.setWebViewClient(new WebViewClient());
        mWebView.loadUrl(getResources().getString(R.string.url_informazioni));
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                BluetoothActivity.changeColorStatusBar(this, R.color.colorPrimary);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }
}
