package mindinformatica.com.earon.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.RequiresApi;
import androidx.core.content.ContextCompat;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import mindinformatica.com.earon.R;
import mindinformatica.com.earon.jellyfish.JellyfishActivity;

/**
 * Created by camillacecconi on 11/07/17.
 */

public abstract class BluetoothActivity extends AppCompatActivity {
    public AbstractManagerBluetooth mb;
    private boolean mAutoStop = true;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*if(Build.VERSION.SDK_INT < 23) {
            mb = new ManagerBluetooth(this);
        }else {
            mb = new ManagerBluetoothNoLocalization(this);
        }*/
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (!(this instanceof JellyfishActivity)) {
                    changeColorStatusBar(this, R.color.colorPrimary);
                } else {
                    changeColorStatusBar(this, R.color.whitetext);
                }
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    public void scan(boolean autoStop){
        mb.startScanner(autoStop);
        mAutoStop = autoStop;
    }
    public void disconnect(){
        Log.i("DISCONNECT1", "going to disconnect");
        mb.disconnect();
    }
    //TODO metodo stop scan

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == ManagerBluetooth.REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_CANCELED) {
                //Bluetooth not enabled.
                finish();
                return;
            }else{
                mb.startScanner(mAutoStop);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case ManagerBluetooth.REQUEST_ACCESS_COARSE_LOCATION:
            case ManagerBluetooth.REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mb.startScanner(mAutoStop);

                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }
    @Override
    protected void onPause() {
        //mb.pause();
        super.onPause();

    }

    @Override
    protected void onDestroy() {
        //mb.destroy();
        super.onDestroy();
    }

    public abstract void deviceFound(BluetoothDevice device);
    public abstract void connectedToDevice(final BluetoothDevice device);
    public abstract void noBluetooth();
    public abstract void pressioneChanged(float pressioneLorda, float temperatura, float pressioneNetta);
    public abstract void batteriaChanged(long batteria);
    public abstract void disconnectedFromDevice();
    public abstract void deviceAlreadyConnected(BluetoothDevice mDevice);
    public abstract void deviceNotAlreadyConnected();

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public static void changeColorStatusBar(Activity ac, int colorId){
        Window window = ac.getWindow();
        /*window.setFlags(
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS,
                WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS
        );*/
// clear FLAG_TRANSLUCENT_STATUS flag:
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
// add FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS flag to the window
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
// finally change the color
        window.setStatusBarColor(ContextCompat.getColor(ac, colorId));
    }

    public String readTextFromUri(Uri uri) throws IOException {
        StringBuilder stringBuilder = new StringBuilder();
        InputStream inputStream = getContentResolver().openInputStream(uri);
        if(inputStream != null) {
            BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
            String line;
            while ((line = reader.readLine()) != null) {
                stringBuilder.append(line);
            }
        }
        return stringBuilder.toString();
    }

}

