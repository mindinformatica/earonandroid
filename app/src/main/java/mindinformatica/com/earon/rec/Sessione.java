package mindinformatica.com.earon.rec;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

import androidx.appcompat.app.AppCompatActivity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import mindinformatica.com.earon.R;
import mindinformatica.com.earon.db.DbHelper;
import mindinformatica.com.earon.db.TableInterface;

/**
 * Created by camillacecconi on 20/07/17.
 */

public class Sessione implements TableInterface{
    private static final String DB_DATE_FORMAT = "yyyyMMddHHmmss";
    transient private long id;
    ArrayList<Misura> data = new ArrayList<>();
    String templateName;    //modello
    String mode;    //profilo
    transient Date dInizioSessione;
    transient Date dFineSessione;
    double startDate;
    double stopDate;
    String peripheralName;
    String descr;
    ArrayList<Integer> steps = new ArrayList<>(); //intervalli
    transient Float maxPressure;
    transient Float minPressure;
    transient Float compPressure;
    transient Context context;
    transient Boolean isImported;

    public Sessione(Context c){
        this.context = c;
    }

    @Override
    public String toString() {
        return dateToString(dInizioSessione);
    }

    public double getStartDate() {
        return startDate;
    }

    public void setStartDate(double startDate) {
        this.dInizioSessione = new Date((long)startDate*1000);
        this.startDate = startDate;
    }

    public void setStartDate(Date date) {
        this.startDate = ((double)date.getTime()) / 1000;
    }

    public double getStopDate() {
        return stopDate;
    }

    public void setStopDate(double stopDate) {
        this.dInizioSessione = new Date((long)stopDate*1000);
        this.stopDate = stopDate;
    }

    public void setStopDate(Date date) {
        this.stopDate = ((double)date.getTime()) / 1000;
    }

    public Date getDInizioSessione() {
        return dInizioSessione;
    }

    public void setDInizioSessione(Date dSessione) {
        this.dInizioSessione = dSessione;
    }

    public Date getDFineSessione() {
        return dFineSessione;
    }

    public void setDFineSessione(Date dSessioneFine) {
        this.dFineSessione = dSessioneFine;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getPeripheralName() {
        return peripheralName;
    }

    public void setPeripheralName(String deviceName) {
        this.peripheralName = deviceName;
    }

    public String getTemplateName() {
        return templateName;
    }

    public void setTemplateName(String modello) {
        this.templateName = modello;
    }

    public Boolean getImported() {
        if(isImported == null){
            return false;
        }
        return isImported;
    }

    public void setImported(Boolean imported) {
        isImported = imported;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String profilo) {
        this.mode = profilo;
    }

    public ArrayList<Integer> getSteps() {
        return steps;
    }

    public void setSteps(ArrayList<Integer> intervalli) {
        this.steps = intervalli;
    }

    public Float getCompPressure() {
        return compPressure;
    }

    public void setCompPressure(Float compPressure) {
        this.compPressure = compPressure;
    }

    public Float getMaxPressure() {
        return maxPressure;
    }

    public void setMaxPressure(Float maxPressure) {
        this.maxPressure = maxPressure;
    }

    public Float getMinPressure() {
        return minPressure;
    }

    public void setMinPressure(Float minPressure) {
        this.minPressure = minPressure;
    }

    public synchronized ArrayList<Misura> getData() {
        return data;
    }

    public synchronized void addMisura(Misura misura){
        this.data.add(misura);
    }

    public int getLineColor(long timestamp){
        //mi arriva il timestamp in millisecondi. Devo capire in quale intervallo sta.
        int intervalloInCuiSono = getIntervalloCorrente(timestamp);
        //se quando esco i è pari, vuol dire che sono in un intervallo in cui sto soffiando, altrimenti in una pausa (perché l'ho aumentato di 1)
        int color = (intervalloInCuiSono%2==0)?context.getResources().getColor(R.color.soffiocolor):context.getResources().getColor(R.color.pausecolor);
        return color;
    }
    public int getIntervalloCorrente(long timestamp){
        long tsSecondi = timestamp/1000;
        int tempo = 0;
        int intervalloInCuiSono = 0;
        for (int i = 0; i< steps.size(); i++){
            tempo = tempo + steps.get(i);
            if (tsSecondi < tempo){
                intervalloInCuiSono = i;
                break;
            }
        }
        return intervalloInCuiSono;
}

    public long getTempoTotale(){
        long t = 0;
        for (int intervallo : steps){
            t = t + intervallo*1000;
        }
        return t;
    }
    public int getNGiriTrascorsi(long timestamp){
        //un giro è una combinazione soffio, pausa, quindi il numero di giri trascorsi e ntrascorsi/2
        return (getIntervalloCorrente(timestamp)/2) +1;
    }
    public int getNGiriTotali(){
        int toRet;
        if (steps.size() % 2 == 0){
            toRet = steps.size()/2;
        }else{
            toRet = (steps.size()/2) +1;
        }
        return toRet; //
    }


    /**METODI CHE SERVONO PER IL DB*/

    /* una sessione è composta da:
    * id sessione
    * data e ora sessione
    * nome del modello della sessione
    * steps di quella sessione
    * profilo di quella sessione
    * liminf
    * soglia compensazione
    * limsup
    * misurazioni
    * data fine sessione
    * descrizione sessione
    * device name
    * */

    public static final String TABLE_NAME = "Sessioni";
    public static final String COLUMN_ID_SESSIONE = "_id";
    public static final String TYPE_ID_SESSIONE = TYPE_INTEGER;
    public static final String COLUMN_DATA = "D_sessione";
    public static final String TYPE_DATA = TYPE_TEXT;
    public static final String COLUMN_MODELLO = "Modello";
    public static final String TYPE_MODELLO = TYPE_TEXT;
    public static final String COLUMN_INTERVALLI = "Intervalli";
    public static final String TYPE_INTERVALLI = TYPE_TEXT;
    public static final String COLUMN_PROFILO = "Profilo";
    public static final String TYPE_PROFILO = TYPE_TEXT;
    public static final String COLUMN_LIMINF = "Liminf";
    public static final String TYPE_LIMINF = TYPE_REAL;
    public static final String COLUMN_LIMSUP = "Limsup";
    public static final String TYPE_LIMSUP = TYPE_REAL;
    public static final String COLUMN_SOGLIA = "Soglia";
    public static final String TYPE_SOGLIA = TYPE_REAL;
    public static final String COLUMN_MISURAZIONI = "Misurazioni";
    public static final String TYPE_MISURAZIONI = TYPE_TEXT;
    public static final String COLUMN_DATA_FINE = "D_sessione_fine";
    public static final String TYPE_DATA_FINE = TYPE_TEXT;
    public static final String COLUMN_DEVICE_NAME = "Device_name";
    public static final String TYPE_DEVICE_NAME = TYPE_TEXT;
    public static final String COLUMN_DESCRIZIONE_SESSIONE = "Descrizione";
    public static final String TYPE_DESCRIZIONE_SESSIONE = TYPE_TEXT;
    public static final String COLUMN_IS_IMPORTED = "Importato";
    public static final String TYPE_IS_IMPORTED = TYPE_INTEGER;


    @Override
    public String getCreateString() {
        return "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_ID_SESSIONE + " " + TYPE_ID_SESSIONE + " PRIMARY KEY, " +
                COLUMN_DATA + " " + TYPE_DATA + "," +
                COLUMN_MODELLO + " " + TYPE_MODELLO + "," +
                COLUMN_INTERVALLI + " " + TYPE_INTERVALLI + "," +
                COLUMN_PROFILO + " " + TYPE_PROFILO + "," +
                COLUMN_LIMINF + " " + TYPE_LIMINF+ "," +
                COLUMN_LIMSUP + " " + TYPE_LIMSUP+ ","+
                COLUMN_SOGLIA + " " + TYPE_SOGLIA + "," +
                COLUMN_MISURAZIONI + " " + TYPE_MISURAZIONI + "," +
                COLUMN_DATA_FINE + " " + TYPE_DATA_FINE + "," +
                COLUMN_DEVICE_NAME + " " + TYPE_DEVICE_NAME + "," +
                COLUMN_DESCRIZIONE_SESSIONE + " " + TYPE_DESCRIZIONE_SESSIONE + "," +
                COLUMN_IS_IMPORTED + " " + TYPE_IS_IMPORTED + " DEFAULT 0)";
    }

    @Override
    public String getDropString() {
        return "DROP TABLE IF EXISTS " + TABLE_NAME;
    }

    public ContentValues createContentValuesDescrizione(){
        ContentValues values = new ContentValues();
        values.put(COLUMN_DESCRIZIONE_SESSIONE, descr);
        return values;
    }

    public ContentValues createContentValues(){
        ContentValues values = new ContentValues();
        //non metto l'id, si riempie da solo con uno non ancora preso. Potrebbe riutilizzare l'id di righe cancellate.
        values.put(COLUMN_DATA, dateToString(dInizioSessione));
        values.put(COLUMN_MODELLO, templateName);
        values.put(COLUMN_INTERVALLI, intervalliToString());
        values.put(COLUMN_PROFILO, mode);
        values.put(COLUMN_LIMINF, minPressure);
        values.put(COLUMN_LIMSUP, maxPressure);
        values.put(COLUMN_SOGLIA, compPressure);
        values.put(COLUMN_MISURAZIONI, misurazioniToString());
        values.put(COLUMN_DATA_FINE, dateToString(dFineSessione));
        values.put(COLUMN_DEVICE_NAME, peripheralName);
        values.put(COLUMN_DESCRIZIONE_SESSIONE, descr);
        values.put(COLUMN_IS_IMPORTED, getImported() ? 1 : 0);
        return values;
    }

    public void fillFromCursor(Cursor crs){
        //quando arrivo qui, il cursore deve già indicarmi l'elemento che mi interessa
        id = crs.getInt(crs.getColumnIndexOrThrow(COLUMN_ID_SESSIONE));
        dInizioSessione = stringToDate(crs.getString(crs.getColumnIndexOrThrow(COLUMN_DATA)));
        templateName = crs.getString(crs.getColumnIndexOrThrow(COLUMN_MODELLO));
        stringToIntervalli(crs.getString(crs.getColumnIndexOrThrow(COLUMN_INTERVALLI)));
        mode = crs.getString(crs.getColumnIndexOrThrow(COLUMN_PROFILO));
        minPressure = (!crs.isNull(crs.getColumnIndexOrThrow(COLUMN_LIMINF)))?crs.getFloat(crs.getColumnIndexOrThrow(COLUMN_LIMINF)):null;
        maxPressure = (!crs.isNull(crs.getColumnIndexOrThrow(COLUMN_LIMSUP)))?crs.getFloat(crs.getColumnIndexOrThrow(COLUMN_LIMSUP)):null;
        compPressure = (!crs.isNull(crs.getColumnIndexOrThrow(COLUMN_SOGLIA)))?crs.getFloat(crs.getColumnIndexOrThrow(COLUMN_SOGLIA)):null;
        stringToMisurazioni(crs.getString(crs.getColumnIndexOrThrow(COLUMN_MISURAZIONI)));
        dFineSessione = stringToDate(crs.getString(crs.getColumnIndexOrThrow(COLUMN_DATA_FINE)));
        peripheralName = crs.getString(crs.getColumnIndexOrThrow(COLUMN_DEVICE_NAME));
        descr = crs.getString(crs.getColumnIndexOrThrow(COLUMN_DESCRIZIONE_SESSIONE));
        isImported = crs.getInt(crs.getColumnIndexOrThrow(COLUMN_IS_IMPORTED)) == 1;
    }
    public void salvaSessione(){
        DbHelper db = new DbHelper(context);
        try{
            db.getWritableDatabase().insertOrThrow(TABLE_NAME, null, createContentValues());
        }finally {
            db.close();
        }
    }

    public void salvaDescrizioneSessione(){
        DbHelper db = new DbHelper(context);
        try{
            db.getWritableDatabase().update(TABLE_NAME, createContentValuesDescrizione(), COLUMN_ID_SESSIONE + "= ? ", new String[] {String.valueOf(id)} );
        }finally {
            db.close();
        }
    }

    public static void deleteSessione(Context c, Long id){
        DbHelper db = new DbHelper(c);
        try{
           String whereString = COLUMN_ID_SESSIONE + " = " +Long.toString(id);
            db.getWritableDatabase().delete(TABLE_NAME, whereString, null);
        }finally{
            db.close();
        }

    }
    public static ArrayList<Sessione> getSessioni(Context c){
        DbHelper db = new DbHelper(c);
        try {
            Cursor crs = db.getReadableDatabase().query(TABLE_NAME, null, null, null, null, null, COLUMN_IS_IMPORTED + ", " + COLUMN_DATA + " DESC"); //colonne: tutte, colonne where: nessuno, valore where: nessuno
            ArrayList<Sessione> sessioni = null;
            if (crs != null && crs.getCount() > 0) {
                sessioni = new ArrayList<>();
                while (crs.moveToNext()) {
                    Sessione s = new Sessione(c);
                    try {
                        s.fillFromCursor(crs);
                        sessioni.add(s);
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }

                }
            }
            return sessioni;
        }finally{
            db.close();
        }
    }
    private String intervalliToString(){
        return intervalliToString(steps);
    }
    public static String intervalliToString(ArrayList<Integer> intervalliPassati){
        String val = "";
        for (Integer inter : intervalliPassati){
            val = val + ((val.length() != 0)?"_":"") + Integer.toString(inter);
        }
        return val;
    }
    private void stringToIntervalli(String s){
        steps = staticStringToIntervalli(s);
    }
    public static ArrayList<Integer> staticStringToIntervalli(String s){
        ArrayList<Integer> intervalli = new ArrayList<>();
        String[] lista = s.split("_");
        for (String valore : lista){
            intervalli.add(Integer.parseInt(valore));
        }
        return intervalli;
    }
    private synchronized String misurazioniToString(){
        String ret = "";

        for (Misura m : data){
            ret = ret + ((ret.length()!=0)?"_":"") + m.creaStringa();
        }
        return ret;
    }
    private synchronized void stringToMisurazioni(String s){
        String[] lista = s.split("_");
        for (String misuraTot : lista){
            Misura m = new Misura();
            m.parsaStringa(misuraTot);
            data.add(m);
        }
    }

    private String dateToString(Date d){
        if(d == null){
            return "";
        }
        DateFormat df = new SimpleDateFormat(DB_DATE_FORMAT);
        return df.format(d);
    }
    private Date stringToDate(String s){
        if(s == null){
            return null;
        }
        DateFormat df = new SimpleDateFormat(DB_DATE_FORMAT);
        try {
            return df.parse(s);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static Sessione getSessione(AppCompatActivity c, Long idSessione) {
        DbHelper db = new DbHelper(c);
        try {
            String whereString = COLUMN_ID_SESSIONE + " = " + Long.toString(idSessione);
            // String[] whereArgs = {idSessione};
            Cursor crs = db.getReadableDatabase().query(TABLE_NAME, null, whereString, null, null, null, COLUMN_DATA); //colonne: tutte, colonne where: nessuno, valore where: nessuno

            if (crs != null && crs.getCount() > 0) {
                crs.moveToFirst();
                Sessione s = new Sessione(c);
                s.fillFromCursor(crs);

                return s;
            } else {
                return null;
            }
        }finally{
            db.close();
        }
    }

    public long getId() {
        return id;
    }

    public void setContext(Context context) {
        this.context = context;
    }
}
