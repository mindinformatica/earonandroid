package mindinformatica.com.earon.jellyfish;

import android.app.Fragment;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import androidx.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import mindinformatica.com.earon.R;
import mindinformatica.com.earon.bluetooth.BluetoothActivity;

/**
 * Fragment della mainActivity
 * Created by lorenzo on 16/07/18.
 */

public class FragmentJellyfish extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_jellyfish, container, false);
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null && BluetoothActivity.class.isAssignableFrom((getActivity().getClass()))&& getUserVisibleHint()){
            //((BluetoothActivity)getActivity()).scan(false); //inizio con uno scan, se non lo trovo, dopo un pochino mi fermo
            View fab = getActivity().findViewById(R.id.fab);
            fab.setVisibility(View.INVISIBLE);
            updateScoreTextView(getView());
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        /*if (getActivity() != null && BluetoothActivity.class.isAssignableFrom((getActivity().getClass()))){
           ((BluetoothActivity)getActivity()).disconnect();
        }*/
    }

    /*@Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getActivity() != null && BluetoothActivity.class.isAssignableFrom((getActivity().getClass()))){
            BluetoothActivity ac = ((BluetoothActivity)getActivity());
            if (isVisibleToUser){
                ac.scan(false);//inizio con uno scan, se non lo trovo, dopo un pochino mi fermo
            }
        }
    }*/

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        //recupero il punteggio dalle preferenze
    }

    private void updateScoreTextView(View view){
        SharedPreferences prefs = getActivity().getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        int score = prefs.getInt("score", 0);
        TextView tvScore = view.findViewById(R.id.score);
        tvScore.setText("" + score);
    }

}
