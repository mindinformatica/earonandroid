package mindinformatica.com.earon.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattDescriptor;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothProfile;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanSettings;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import mindinformatica.com.earon.application.EaronApplication;


/**
 * Manager Bluetooth valido sia per api superiori alla 23 che inferiori.
 * Vanno implementati i metodi astratti:
 *  startScanner
 *  pause
 *  scanLeDevice
 *
 * Created by lorenzo on 03/08/18.
 */



public abstract class AbstractManagerBluetooth {
    static final int REQUEST_ACCESS_COARSE_LOCATION = 1;
    static final int REQUEST_ACCESS_FINE_LOCATION = 2;
    BluetoothAdapter mBluetoothAdapter;
    static int REQUEST_ENABLE_BT = 1;
    Handler mHandler;
    static final long SCAN_PERIOD = 10000;
    BluetoothLeScanner mLEScanner;
    ScanSettings settings;
    List<ScanFilter> filters;
    private BluetoothGatt mGatt;
    public BluetoothActivity activity;
    private boolean waitWhileWritingDescriptor = false;
    private ArrayList<BluetoothGattCharacteristic> charQueue = new ArrayList<>();

    private float pressioneTara = 0;

    public abstract void startScanner(boolean autoStop);
    public abstract void pause();
    public abstract void scanLeDevice(final boolean enable, final boolean autoStop);

    public EaronApplication app;
    public AbstractManagerBluetooth(EaronApplication c){
        app = c;
        mHandler = new Handler(Looper.getMainLooper());
        if (!c.getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(c, "BLE Not Supported", Toast.LENGTH_SHORT).show();
        }
    }

    public void destroy(){
        if (mGatt == null) {
            return;
        }
        mGatt.disconnect();
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, int rssi,
                                     byte[] scanRecord) {
                    Log.i("onLeScan", device.toString());
                    app.deviceFound(device);
                }

            };

    public void connectToDevice(BluetoothDevice device) {
        if (mGatt == null) {
            scanLeDevice(false, true);// will stop after first device detection
            Log.e("CONNETTO GATT", "device " + device.getName() + " " + app.TAG +" HO CHIUSO SCAN");
            mGatt = device.connectGatt(app, false, gattCallback);

        }
    }
    public void disconnect(){ //TODO è uguale a destroy, ma in realtà devo farlo quando cambio fragment, non quando esco dall'activity
        scanLeDevice(false, true);
        if (mGatt == null){
            Log.i("MGATT", "null");
            return;
        }else{
            Log.i("MGATT", "going to disconnect");
            mGatt.disconnect();
            // mGatt.close();
            // mGatt = null;
        }

    }

    public int connectionState;

    private final BluetoothGattCallback gattCallback = new BluetoothGattCallback() {

        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            Log.i("onConnectionStateChange", "Status: " + status);
            connectionState = newState;
            switch (newState) {
                case BluetoothProfile.STATE_CONNECTED:
                    Log.i("gattCallback", "STATE_CONNECTED " +app.TAG + " " + gatt.getDevice());
                    gatt.discoverServices();
                    //TODO dovrò chiamare il metodo "connectedToDevice" solo dopo che avrò letto (e memorizzato da qualche parte) la pressione
                    activity.connectedToDevice(gatt.getDevice());
                    break;
                case BluetoothProfile.STATE_DISCONNECTED:
                    Log.e("gattCallback", "STATE_DISCONNECTED");

                    if (mGatt != null){
                        Log.i("MGATT", "going to close");
                        mGatt.close();
                        Log.i("MGATT", "lista = " +mGatt.getDevice());
                    }
                    Log.i("MGATT", "closed");
                    mGatt = null;
                    activity.disconnectedFromDevice();
                    break;
                default:
                    Log.e("gattCallback", "STATE_OTHER");
            }

        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            List<BluetoothGattService> services = gatt.getServices();
            Log.i("onServicesDiscovered", services.toString());

            for (BluetoothGattService s : services){
                if (s.getUuid().equals(UUID.fromString("AFF70000-A3EF-4753-957C-9750A82F3AB4"))){ //servizio per la pressione
                    for(BluetoothGattCharacteristic c : s.getCharacteristics()){
                        if (c.getUuid().equals(UUID.fromString("AFF70001-A3EF-4753-957C-9750A82F3AB4"))){
                            //leggo il valore di pressione iniziale, per tarare il dispositivo
                            // gatt.readCharacteristic(c);
                            tryRead(gatt, c);
                        }
                    }
                }else if (s.getUuid().equals(UUID.fromString("0000180f-0000-1000-8000-00805f9b34fb"))){ //servizio per la batteria
                    for(BluetoothGattCharacteristic c : s.getCharacteristics()){
                        if (c.getUuid().equals(UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb"))){
                            //leggo il valore di batteria, per poi metterlo nella textview
                            //gatt.readCharacteristic(c);
                            tryRead(gatt, c);
                        }
                    }
                }

            }
        }

        @Override
        // Characteristic notification
        public void onCharacteristicChanged(BluetoothGatt gatt,
                                            BluetoothGattCharacteristic characteristic) {
            leggiCaratteristiche(characteristic, false);
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt,
                                         BluetoothGattCharacteristic
                                                 characteristic, int status) {
            leggiCaratteristiche(characteristic, true);
            //le caratteristiche che ho letto, voglio anche ricevere le notifiche
            registerToNotification(gatt, characteristic);
            Log.i("onCharacteristicRead", characteristic.toString());
        }

        @Override
        public void onDescriptorWrite(BluetoothGatt gatt, BluetoothGattDescriptor descriptor, int status) {
            super.onDescriptorWrite(gatt, descriptor, status);
            if (charQueue.size() > 0){
                BluetoothGattCharacteristic cNew = charQueue.remove(0);
                gatt.readCharacteristic(cNew);
            }else{
                waitWhileWritingDescriptor = false;
            }
        }

    };
    private float readParam(String stringa){
        int l = stringa.length();
        int i = 0;
        String result = "";
        while (i+2 <= l){
            result = stringa.substring(i, i+2) + result;
            i = i+2;
        }
        return (Integer.parseInt(result, 16))/100f;
    }

    private void leggiCaratteristiche(BluetoothGattCharacteristic characteristic, boolean firstTime) {
        if (characteristic.getUuid().equals(UUID.fromString("AFF70001-A3EF-4753-957C-9750A82F3AB4"))){
            byte[] array = characteristic.getValue();
            String valore = bytesToHex(characteristic.getValue());
            // Log.i("STRINGA", valore);
            /*String[] pressioneList = valore.substring(0,7).split("(?<=\\G...)");
            String pressioneString = "";
            for (int i = pressioneList.length-1; i >= 0; i--){
                pressioneString = pressioneString + pressioneList[i];
            }*/
            float pressioneLorda = readParam(valore.substring(0,7));
            /*String[] temperaturaList = valore.substring(8,16).split("(?<=\\G...)");
            String temperaturaString = "";
            for (int i = temperaturaList.length-1; i >= 0; i--){
                temperaturaString = temperaturaString + temperaturaList[i];
            }
            long temperatura = Long.parseLong(temperaturaString, 16);*/
            float temperatura = readParam(valore.substring(8,16));
            if (firstTime){
                pressioneTara = pressioneLorda;
                // Log.i("Lettore", "PRESSIONE TARA LETTA " + Float.toString(pressioneLorda));
            }else{
                float pressioneNetta = pressioneLorda-pressioneTara;
                activity.pressioneChanged(pressioneLorda, temperatura,pressioneNetta);
                // Log.i("Lettoactivityre", "PRESSIONE Lorda: " + Float.toString(pressioneLorda) + " Tara: " + Float.toString(pressioneTara) + " Netta: " + Float.toString(pressioneNetta));
            }

        }
        if (characteristic.getUuid().equals(UUID.fromString("00002a19-0000-1000-8000-00805f9b34fb"))){
            Long valore = Long.parseLong(bytesToHex(characteristic.getValue()),16);
            activity.batteriaChanged(valore);
            // Log.i("Lettore", "BATTERIA CAMBIATA " + valore);
        }
    }

    public void registerToNotification(BluetoothGatt gatt, BluetoothGattCharacteristic c){
        gatt.setCharacteristicNotification(c, true);
        UUID uuid = UUID.fromString("00002902-0000-1000-8000-00805f9b34fb");
        BluetoothGattDescriptor descriptor = c.getDescriptor(uuid);
        descriptor.setValue(BluetoothGattDescriptor.ENABLE_NOTIFICATION_VALUE);
        gatt.writeDescriptor(descriptor);
    }
    public void tryRead(BluetoothGatt gatt, BluetoothGattCharacteristic c){
        if (!waitWhileWritingDescriptor){
            //se è la prima notifica a cui mi registro, vado direttamente. Dico che la sto registrando
            waitWhileWritingDescriptor = true;
            gatt.readCharacteristic(c);

        }else{
            //se invece sto registrando altre modifiche, metto questa in coda.
            charQueue.add(c);
        }

    }
    /*public void tryRegister(BluetoothGatt gatt, BluetoothGattCharacteristic c){
        if (!waitWhileWritingDescriptor){
            //se è la prima notifica a cui mi registro, vado direttamente. Dico che la sto registrando
            waitWhileWritingDescriptor = true;
            registerToNotification(gatt,c);

        }else{
            //se invece sto registrando altre modifiche, metto questa in coda.
            if (!charQueue.c)
            charQueue.add(c);
        }

    }*/


    /*metodi per leggere la pressione e la temperatura*/
    private final static char[] hexArray = "0123456789ABCDEF".toCharArray();
    public static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }

}