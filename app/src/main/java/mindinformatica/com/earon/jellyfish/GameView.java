package mindinformatica.com.earon.jellyfish;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Rect;
import android.util.LruCache;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import java.util.ArrayList;

import static mindinformatica.com.earon.jellyfish.JellyfishActivity.gameOver;

/**
 * classe per disegnare la schermata di gioco: sfondo e medusa
 * Created by lorenzo on 17/07/18.
 */

public class GameView extends SurfaceView implements SurfaceHolder.Callback {

    private MainThread thread;
    private Jellyfish jellyfish;
    private Sfondo sfondo;

    private Context mainContext;

    /*public GameView(Context context, AttributeSet attr){
        super(context, attr);

        getHolder().addCallback(this);
        thread = new MainThread(getHolder(), this);
        setFocusable(true);
        sfondo = new Sfondo(context);
        jellyfish = new Jellyfish(context);
    }*/

    public GameView(Context context, LruCache<String, Bitmap> mMemoryCache) {
        super(context);

        this.mainContext = context;
        /*this.setBackgroundColor(Color.TRANSPARENT);
        this.setZOrderOnTop(true);
        getHolder().setFormat(PixelFormat.TRANSPARENT);*/


        getHolder().addCallback(this);
        thread = new MainThread(getHolder(), this);
        setFocusable(true);
        sfondo = new Sfondo(context, mMemoryCache);
        jellyfish = new Jellyfish(context);
    }

    public Jellyfish getJellyfish(){
        return jellyfish;
    }

    public MainThread getMainThread(){
        return thread;
    }

   /* public void setImage(String stringName){
        int id = getResources().getIdentifier(stringName, "drawable", getContext().getPackageName());
        Bitmap bmp = BitmapFactory.decodeResource(getResources(), id, jellyfish.getOptions());
        jellyfish.image = Bitmap.createScaledBitmap(bmp,JellyConstants.JELLY_WIDTH,JellyConstants.JELLY_HEIGHT,false);
    }*/

    /*public void setSfondo(int j){
        sfondo.setImageFrames(j);
    }*/

    public void update(){
        jellyfish.update();
        sfondo.update();
        if(!gameOver) {
            gameOver = checkForCollision();
            if (gameOver){
                thread.setRunning(false);
            }
        }else{
            thread.setRunning(false);
        }
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        thread.setRunning(true);
        thread.start();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        boolean retry = true;
        while (retry) {
            try {
                thread.setRunning(false);
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            retry = false;
        }
    }

    @Override
    public void draw(Canvas canvas) {
        super.draw(canvas);
        setDrawingCacheEnabled(true);
        if (canvas != null) {
            canvas.drawColor(Color.WHITE);
            sfondo.draw(canvas);
            if(!gameOver) { //se siamo in gameOver, si disegna solo lo sfondo per lultima volta
                jellyfish.draw(canvas);
            }else{
                JellyfishActivity.die(this);
            }
        }
    }

    private boolean checkForCollision(){
        Rect jellyRect = jellyfish.getJellyRect();
        if(jellyRect.top < JellyConstants.getTopMargin()){
            return true;
        }
        ArrayList<Rect> fishRects = JellyfishActivity.getRectFromView();
        for (int i = 0; i < fishRects.size(); i++){
            if(jellyRect.intersect(fishRects.get(i))){
                return true;
            }
        }
        return false;
    }

    public void setJellyAcceleration(double jellyAcceleration) {
        jellyfish.setAcceleration(jellyAcceleration);
    }

    public void setJellyInterval(double jellyInterval) {
        jellyfish.setInterval(jellyInterval);
    }

    public Sfondo getSfondo(){
        return sfondo;
    }

}
