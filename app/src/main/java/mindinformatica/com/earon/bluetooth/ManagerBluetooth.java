package mindinformatica.com.earon.bluetooth;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import java.util.ArrayList;
import java.util.List;

import mindinformatica.com.earon.application.EaronApplication;

import static android.os.Build.VERSION_CODES.M;

/**
 * Created by camillacecconi on 12/07/17.
 */



public class ManagerBluetooth extends AbstractManagerBluetooth{

ScanCallback sc;
private Object getScanCallback(){
    if (Build.VERSION.SDK_INT >= 21){
        if (sc != null){
            return sc;
        }else{
            return sc = new ScanCallback() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onScanResult(int callbackType, ScanResult result) {
                    //   Log.i("callbackType", String.valueOf(callbackType));
                    BluetoothDevice btDevice = result.getDevice();
                    if (btDevice.getName() != null)
                        Log.i("device", "device " + btDevice.getName() + " " + app.TAG);
                    app.deviceFound(btDevice);
                }
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onBatchScanResults(List<ScanResult> results) {
                    for (ScanResult sr : results) {
                        Log.i("ScanResult - Results", sr.toString());
                    }
                }

                @Override
                public void onScanFailed(int errorCode) {
                    Log.e("Scan Failed", "Error Code: " + errorCode);
                }

            };
        }

    }else{
        return null;
    }
}

    public ManagerBluetooth(EaronApplication c){
        super(c);
        final BluetoothManager bluetoothManager =
                (BluetoothManager) c.getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
    }

    @Override
    public void startScanner(boolean autoStop){
//controllo se posso usare il bluetooth
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
            if (Build.VERSION.SDK_INT >= 21) {
                mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
                settings = new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                        .build();
                filters = new ArrayList<>();
            }
            if (Build.VERSION.SDK_INT >= M) {  // Only ask for these permissions on runtime when running Android 6.0 or higher
                switch (ContextCompat.checkSelfPermission(activity.getBaseContext(), Manifest.permission.ACCESS_COARSE_LOCATION)) {
                    case PackageManager.PERMISSION_DENIED:
                        Log.e("PERMESSO", "permesso negato");
                        if (ContextCompat.checkSelfPermission(activity.getBaseContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(activity,
                                    new String[]{Manifest.permission.ACCESS_COARSE_LOCATION},
                                    REQUEST_ACCESS_COARSE_LOCATION);
                        }
                        break;
                    case PackageManager.PERMISSION_GRANTED:
                        Log.e("PERMESSO", "permesso concesso");
                        scanLeDevice(true, autoStop);
                        break;
                }
            }else{
                scanLeDevice(true, autoStop);
            }

        }
    }

    @Override
    public void pause(){
        if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
            scanLeDevice(false, true);
        }
    }

    @Override
    public void destroy(){
        super.destroy();
    }

    @Override
    public void scanLeDevice(final boolean enable, final boolean autoStop) {
        if (enable) {
            Log.e("SCAN", app.TAG + " start");
            if (autoStop){
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (Build.VERSION.SDK_INT < 21) {
                            mBluetoothAdapter.stopLeScan(mLeScanCallback);
                        } else {
                            mLEScanner.stopScan((ScanCallback)getScanCallback());

                        }
                    }
                }, SCAN_PERIOD);
            }

            if (Build.VERSION.SDK_INT < 21) {
                mBluetoothAdapter.startLeScan(mLeScanCallback);
            } else {
                mLEScanner.startScan(filters, settings, (ScanCallback)getScanCallback());
            }
        } else {
            Log.e("SCAN", app.TAG + " stop");
            if (Build.VERSION.SDK_INT < 21) {
                if (mBluetoothAdapter != null){
                    mBluetoothAdapter.stopLeScan(mLeScanCallback);
                }
            } else {
                if (mLEScanner != null){
                    mLEScanner.stopScan((ScanCallback)getScanCallback());
                }

            }
        }
    }
    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {
                @Override
                public void onLeScan(final BluetoothDevice device, int rssi,
                                     byte[] scanRecord) {
                    Log.i("onLeScan", device.toString());
                    app.deviceFound(device);
                }

            };

}
