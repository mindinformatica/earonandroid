package mindinformatica.com.touchviewlibrary;


/**
 * Altra patetica implementazione dei vettori 2D
 * @author lcecchi
 */
public class Vettore2D {
	private float x, y;

	/**
	 * Costruzione per componenti
	 * @param x
	 * @param y
	 */
	public Vettore2D(float x, float y) {
		super();
		this.x = x;
		this.y = y;
	}

	/**
	 * Costruttore per copia
	 * @param other
	 */
	public Vettore2D(Vettore2D other) {
		if ( other==null ) {
			this.x = 0;
			this.y = 0;
		} else {
			this.x = other.x;
			this.y = other.y;
		}
	}
	
	/**
	 * La prima componente
	 * @return
	 */
	public float getX() { return x; }
	
	/**
	 * La seconda componente
	 * @return
	 */
	public float getY() { return y; }
	
	/**
	 * Prende le componenti da un'altro
	 * @param other
	 */
	public void set(Vettore2D other) {
		if ( other==null ) {
			this.x = 0;
			this.y = 0;
		} else {
			this.x = other.getX();
			this.y = other.getY();
		}
	}

	/**
	 * Imposto per componenti
	 * @param otherx
	 * @param othery
	 */
	public void set(float otherx, float othery) {
		this.x = otherx;
		this.y = othery;
	}
	
	/**
	 * Calcola la lunghezza
	 * @return
	 */
	public float getLength() {
		return (float) Math.sqrt(x*x + y*y);
	}
	
	/**
	 * Somma di un vettore a quello corrente
	 * @param altro
	 */
	public void aggiungi(Vettore2D altro) {
		if ( altro==null ) return;
		this.x += altro.x;
		this.y += altro.y;
	}
	
	/**
	 * Sottrazione di un vettore da un'altro conservando i due vettori passati		
	 */
	public static Vettore2D sottrai(Vettore2D sx, Vettore2D dx) {
		return new Vettore2D(sx.getX()-dx.getX(), sx.getY()-dx.getY());
	}
}
