package mindinformatica.com.earon.rec;

import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.PorterDuff;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.text.InputType;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewSwitcher;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.AppCompatImageView;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;
import com.github.mikephil.charting.highlight.Highlight;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.listener.ChartTouchListener;
import com.github.mikephil.charting.listener.OnChartGestureListener;
import com.github.mikephil.charting.listener.OnChartValueSelectedListener;
import com.google.gson.Gson;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import mindinformatica.com.earon.R;
import mindinformatica.com.earon.bluetooth.BluetoothActivity;
import mindinformatica.com.earon.db.Modello;
import mindinformatica.com.earon.utils.ModelloBarView;

import static mindinformatica.com.earon.rec.ModelliActivity.getModelloDefault;

/**
 * Created by camillacecconi on 17/07/17.
 */

public class RecActivity extends BluetoothActivity implements OnChartValueSelectedListener, OnChartGestureListener {

    private final static int REQUEST_MODELLI = 2;
    private final static int MAX_PRESSURE_INDEX = 0;
    private final static int MIN_PRESSURE_INDEX = 1;
    private final static int COMP_PRESSURE_INDEX = 2;

    private boolean readOnlyActivity=false;

    private boolean deviceConnected = false;
    private String savedDeviceAddress;

    private Sessione sessione;
    private ProfiliActivity.Profili profilo;
    //private ArrayList<Misura> data = new ArrayList();
    private LineChart grafico;
    private Integer lastLineColor;

    TextView pressioneView;
    TextView tempoView;
    TextView giriView;

    private float initialX;
    private String profiloValue;
    private Float massimoValue;
    private Float minimoValue;
    private Float compensazioneValue;

    private Float maxValueForColor;//massimo valore per valutare colore testo pressione

    private String modelloValue;

    private MediaPlayer countDownMediaPlayer;

    private float maxPressureSound;
    private float minPressureSound;
    private float pressureSound;//valore che viene di volta in volta aggiornato in base alla misurazione
    private SoundThread soundThread;
    private BluetoothDevice device;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rec);
        readOnlyActivity = getIntent().getBooleanExtra("readonly", false);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setIcon(R.drawable.logo_uba);
        if (!readOnlyActivity){
            getSupportActionBar().setTitle(R.string.activity_rec_title);
        }else{
            long idSessione = getIntent().getLongExtra("sessioneId", 0);
            Sessione s = Sessione.getSessione(this, idSessione);
            String datePattern = android.text.format.DateFormat.getBestDateTimePattern(Locale.getDefault(), "ddMMyyyyHHmmss");
            SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);
            String data = dateFormat.format(s.getDInizioSessione());
            getSupportActionBar().setTitle(data);
        }

        pressioneView = findViewById(R.id.pressione_tv);
        tempoView = findViewById(R.id.tempo_tv);
        giriView = findViewById(R.id.giri_tv);
        //ViewSwitcher vs = findViewById(R.id.main_container);
        impostaGrafico();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (readOnlyActivity){
            MenuInflater inflater=getMenuInflater();
            inflater.inflate(R.menu.menu_diario_detail, menu);
            menu.getItem(0).getIcon().setColorFilter(getResources().getColor(R.color.colorElements), PorterDuff.Mode.SRC_ATOP);
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_share_png:
                condividiSessione("png");
                break;
            case R.id.action_share_eqtool:
                condividiSessione("eqtool");
                break;
            case R.id.action_rename:
                renameSession();
                break;
            case R.id.action_share_csv:
                condividiSessione("csv");
                break;
            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onTouchEvent(MotionEvent touchevent) {
        //if (!readOnlyActivity){
            ViewSwitcher vp = findViewById(R.id.main_container);
            switch (touchevent.getAction()) {
                case MotionEvent.ACTION_DOWN:
                    initialX = touchevent.getX();
                    break;
                case MotionEvent.ACTION_UP:
                    float finalX = touchevent.getX();
                    if (initialX > finalX) {
                        if (vp.getDisplayedChild() == 1)
                            break;

                        vp.setInAnimation(this, R.anim.in_right);
                        vp.setOutAnimation(this, R.anim.out_left);

                        vp.showNext();
                        ((ImageView)findViewById(R.id.dots_iv)).setImageDrawable(getResources().getDrawable(R.drawable.dots2));
                    } else {
                        if (vp.getDisplayedChild() == 0)
                            break;

                        vp.setInAnimation(this, R.anim.in_left);
                        vp.setOutAnimation(this, R.anim.out_right);

                        vp.showPrevious();
                        ((ImageView)findViewById(R.id.dots_iv)).setImageDrawable(getResources().getDrawable(R.drawable.dots1));
                    }
                    break;
            }
            return false;
        /*}else{
            return super.onTouchEvent(touchevent);
        }*/

    }

    private void impostaGrafico() {
        /**funzione che serve per disegnare il grafico
         * */
        grafico = findViewById(R.id.grafico);
        //non voglio vedere righe verticali e orizzontali sullo sfondo
        grafico.getXAxis().setDrawGridLines(false);
        grafico.getAxisRight().setDrawGridLines(false);
        grafico.getAxisLeft().setDrawGridLines(false);
        grafico.setHighlightPerTapEnabled(true);
        if(!readOnlyActivity)
            grafico.setOnTouchListener(null);

        grafico.getXAxis().setLabelCount(8);
        grafico.getXAxis().setPosition(XAxis.XAxisPosition.BOTTOM);
        grafico.getXAxis().setValueFormatter(new IAxisValueFormatter() {
            @Override
            public String getFormattedValue(float value, AxisBase axis) {
                if (value%1000==0){
                    return Integer.toString((int)(value/1000));
                }else{
                    return "";
                }

            }
        });
        grafico.getLegend().setEnabled(false);
        grafico.setNoDataText("");
        grafico.setBackgroundColor(getResources().getColor(R.color.background));
        Description d = new Description();
        d.setText("");
        grafico.setDescription(d);
        if (readOnlyActivity){
            //grafico.setOnChartGestureListener(this);
            grafico.setOnChartValueSelectedListener(this);
        }


    }

    public void apriProfili(View view) {
        if (!readOnlyActivity){
            Intent i = new Intent(this, ProfiliActivity.class);
            startActivity(i);
        }else{
            ViewSwitcher vs = findViewById(R.id.main_container);
            View v = vs.getCurrentView();

            if (v.getId() == R.id.grafico){
                vs.setInAnimation(this, R.anim.in_right);
                vs.setOutAnimation(this, R.anim.out_left);
                vs.showNext();
                ((ImageView)findViewById(R.id.dots_iv)).setImageDrawable(getResources().getDrawable(R.drawable.dots2));
            }else{
                vs.setInAnimation(this, R.anim.in_left);
                vs.setOutAnimation(this, R.anim.out_right);
               vs.showPrevious();
                ((ImageView)findViewById(R.id.dots_iv)).setImageDrawable(getResources().getDrawable(R.drawable.dots1));
            }

        }
    }

    public void apriModelli(View view) {
        if (!readOnlyActivity){
            Intent i = new Intent(this, ModelliActivity.class);
            startActivityForResult(i, REQUEST_MODELLI);
        }

    }

    private void resetChart() {
        if (grafico.getData() != null) {
            //grafico.getData().clearValues();
            grafico.setData(null);
            grafico.notifyDataSetChanged();
            //grafico.clear();
            grafico.invalidate();
        }
    }

    public void iniziaRegistrazione(View view) {
        final View barra = (findViewById(R.id.scrolling_time_indicator));
        if (sessione == null && !readOnlyActivity){
            if(compensazioneValue != null) {
                pressureSound = compensazioneValue - 0.3f;
                minPressureSound = compensazioneValue - 0.3f;
                maxPressureSound = compensazioneValue + 0.5f;
                /*soundThread = new SoundThread();
                soundThread.start();*/
            }

            resetChart();
            final FullScreenDialog dialog = new FullScreenDialog(this);
            dialog.show();
            barra.setVisibility(View.VISIBLE);
            new CountDownTimer(6000, 1000){

                @Override
                public void onTick(long l) {
                    dialog.setText(Long.toString(l/1000));
                    //playCountSound((int)l/1000);
                }

                @Override
                public void onFinish() {
                    getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                    //creo la sessione
                    dialog.dismiss();
                    Sessione s = new Sessione(RecActivity.this);
                    //nella sessione imposto gli steps
                    ModelloBarView barraView = findViewById(R.id.barra);
                    s.setSteps(barraView.getIntervalli());
                    //nella sessione imposto i valori di massimo, minimo e compensazione
                    if(massimoValue != null){
                        s.setMaxPressure(massimoValue);
                    }
                    if(minimoValue != null){
                        s.setMinPressure(minimoValue);
                    }
                    if(compensazioneValue != null){
                        s.setCompPressure(compensazioneValue);
                    }

                    /*String p = ((TextView)findViewById(R.id.massimo_tv)).getText().toString();
                    if (!"".equals(p))
                        s.setMaxPressure(Float.parseFloat(p));
                    p = ((TextView)findViewById(R.id.minimo_tv)).getText().toString();
                    if (!"".equals(p))
                        s.setMinPressure(Float.parseFloat(p));
                    p = ((TextView)findViewById(R.id.compensazione_tv)).getText().toString();
                    if (!"".equals(p))
                        s.setCompPressure(Float.parseFloat(p));*/

                    s.setDInizioSessione(new Date());

                    s.setTemplateName(modelloValue);
                    s.setPeripheralName(getSharedPreferences("com.example.app", Context.MODE_PRIVATE).getString(getString(R.string.uba_device_name),""));

                    /*TextView titoloModello = findViewById(R.id.modello_tv);
                    s.setTemplateName(titoloModello.getText().toString());*/

                    s.setMode(getResources().getString(ProfiliActivity.titoli.get(profilo)));

                    Button b = findViewById(R.id.button_start);
                    b.setBackgroundColor(getResources().getColor(R.color.redbutton));
                    b.setText(getResources().getString(R.string.stop));

                    sessione = s;
                }
            }.start();

        }else{
            /*try {
                if(mediaPlayer != null){
                    mediaPlayer.stop();
                    mediaPlayer.release();
                }
            }catch (Exception ex){
                ex.printStackTrace();
            }*/
            if(soundThread != null){
                soundThread.stopThread();
            }
            sessione.setDFineSessione(new Date());
            sessione.salvaSessione();
            sessione = null;
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    barra.clearAnimation();
                    getWindow().clearFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
                }
            });
            Button b = findViewById(R.id.button_start);
            b.setBackgroundColor(getResources().getColor(R.color.greenbutton));
            b.setText(getResources().getString(R.string.start));
            resetChart();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_MODELLI){
            if (resultCode == RESULT_OK){
                ArrayList<Integer> intervalli = data.getExtras().getIntegerArrayList("modello");
                String titolo = data.getStringExtra("titoloModello");
                impostaModello(titolo, intervalli);
            }
        }
    }

    private void impostaModello(String titolo, ArrayList<Integer> intervalli) {
        ModelloBarView barraView = findViewById(R.id.barra);
        barraView.setIntervalli(intervalli);

        modelloValue = titolo;

        /*TextView titoloModello = findViewById(R.id.modello_tv);
        titoloModello.setText(titolo);*/
        tempoView.setText(ModelliActivity.getTotalTime(intervalli));
    }

    @Override
    protected void onResume() {
        super.onResume();
        SharedPreferences prefs = this.getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        if (readOnlyActivity){
            //imposto i parametri che ho memorizzati
            long idSessione = getIntent().getLongExtra("sessioneId", 0);
            sessione = Sessione.getSessione(this, idSessione);

            profiloValue = sessione.getMode();
            /*((TextView)findViewById(R.id.profilo_value_tv)).setText(sessione.getMode());
            ((TextView)findViewById(R.id.profilo_value_tv)).setTextColor(getResources().getColor(R.color.colorElements));
            ((TextView)findViewById(R.id.profilo_description_tv)).setTextColor(getResources().getColor(R.color.colorElements));*/

            impostaModello(sessione.getTemplateName(), sessione.getSteps());

            massimoValue = sessione.getMaxPressure();
            minimoValue = sessione.getMaxPressure();
            compensazioneValue = sessione.getCompPressure();

            /*((TextView)findViewById(R.id.massimo_tv)).setText((sessione.getMaxPressure()==null)?"":Float.toString(sessione.getMaxPressure()));
            ((TextView)findViewById(R.id.minimo_tv)).setText((sessione.getMinPressure()==null)?"":Float.toString(sessione.getMinPressure()));
            ((TextView)findViewById(R.id.compensazione_tv)).setText((sessione.getCompPressure()==null)?"":Float.toString(sessione.getCompPressure()));*/

            disegnaGrafico();
            findViewById(R.id.button_start).setVisibility(View.GONE);
            findViewById(R.id.progressBar).setVisibility(View.GONE);
            findViewById(R.id.dots_iv).setVisibility(View.GONE);
            findViewById(R.id.scrolling_time_indicator).setVisibility(View.GONE);
            if(!"".equals(sessione.getDescr())) {
                addDescriptionTitle(sessione.getDescr());
            }
        }else{
            //disabilito tutti i comandi finché non sono connessa
            findViewById(R.id.main_container).setVisibility(View.GONE);
            findViewById(R.id.button_start).setVisibility(View.GONE);
            findViewById(R.id.dots_iv).setVisibility(View.GONE);
            //mi preparo a connettermi al device
            //imposto i dati della scrollview secondo le preferenze attuali
            savedDeviceAddress = prefs.getString("device", null);
            //Imposto i dati che posso mettere nella scrollview in alto
            //profilo
            impostaProfilo(ProfiliActivity.Profili.values()[prefs.getInt("profilo", 0)]);

            /*((TextView)findViewById(R.id.modello_tv)).setTextColor(getResources().getColor(R.color.colorElements));
            ((TextView)findViewById(R.id.modello_description_tv)).setTextColor(getResources().getColor(R.color.colorElements));*/

            int modelloId = prefs.getInt("modelloId", 0);
            ArrayList<Modello> lista = Modello.getModelli(this);
            Modello m = getModelloDefault(this);
            lista.add(0, m);
            ArrayList<Integer> intervalli = lista.get(modelloId).getIntervalli();
            String nomeModello = lista.get(modelloId).getNomeTemplate();
            impostaModello(nomeModello, intervalli);
            findViewById(R.id.scrolling_time_indicator).setVisibility(View.GONE);
            if(deviceConnected){
                viewWithDevice();
            }else{
                findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
            }

            massimoValue = !"".equals(prefs.getString("limsup", "")) ? Float.parseFloat(prefs.getString("limsup", "")) : null;
            minimoValue = !"".equals(prefs.getString("liminf", "")) ? Float.parseFloat(prefs.getString("liminf", "")) : null;
            compensazioneValue = !"".equals(prefs.getString("soglia", "")) ? Float.parseFloat(prefs.getString("soglia", "")) : null;

            /*((TextView)findViewById(R.id.massimo_tv)).setText(prefs.getString("limsup", ""));
            ((TextView)findViewById(R.id.minimo_tv)).setText(prefs.getString("liminf", ""));
            ((TextView)findViewById(R.id.compensazione_tv)).setText(prefs.getString("soglia", ""));*/

            //faccio partire la ricerca di connessione col device
            //scan(false);
        }
        maxValueForColor = massimoValue != null ? massimoValue : (compensazioneValue != null ? compensazioneValue + 10 : 0);
        if(deviceConnected){
            connectedToDevice(this.device);
        }
    }

    private void impostaProfilo(ProfiliActivity.Profili prof) {
        profilo = prof;
        profiloValue =  getResources().getString(ProfiliActivity.titoli.get(profilo));

        /*((TextView)findViewById(R.id.profilo_value_tv)).setText(profiloValue);
        ((TextView)findViewById(R.id.profilo_value_tv)).setTextColor(getResources().getColor(R.color.colorElements));
        ((TextView)findViewById(R.id.profilo_description_tv)).setTextColor(getResources().getColor(R.color.colorElements));*/

        ViewSwitcher vs = findViewById(R.id.main_container);
        if (vs.getChildAt(1) != null && vs.getChildAt(1).getClass().equals(AppCompatImageView.class)){
            ((ImageView)vs.getChildAt(1)).setImageBitmap(null);
            ((ImageView)vs.getChildAt(1)).setImageResource(ProfiliActivity.grafici.get(profilo));
        }
    }


    @Override
    protected void onPause() {
        super.onPause();
        Log.i("ON PAUSE", "going to disconnect");
        ViewSwitcher vs = (ViewSwitcher) findViewById(R.id.main_container);
        if (vs.getChildAt(1) != null && vs.getChildAt(1).getClass().equals(AppCompatImageView.class)){
            ((ImageView)vs.getChildAt(1)).setImageBitmap(null);
        }
        //disconnect();
        /*if(mediaPlayer != null){
            try{
                mediaPlayer.stop();
                mediaPlayer.release();
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }*/
        if(soundThread != null) {
            soundThread.stopThread();
        }
    }
    @Override
    public void deviceFound(BluetoothDevice device) {
        //in questa activity se trovo un device, devo controllare se è quello che ho memorizzato, se lo è, mi connetto, altrimenti nulla
        if (savedDeviceAddress != null && savedDeviceAddress.equals(device.getAddress()) && device.getName() != null && device.getName().startsWith("UBA")){
            mb.connectToDevice(device);
        }
    }

    @Override
    public void deviceAlreadyConnected(BluetoothDevice mDevice) {
        deviceConnected = true;
        this.device = mDevice;
    }

    @Override
    public void deviceNotAlreadyConnected() {
        deviceConnected = false;
    }

    @Override
    public void connectedToDevice(BluetoothDevice device) {
        deviceConnected = true;
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                // devo nascondere la rotellina di attesa
                viewWithDevice();
            }
        });
    }

    private void viewWithDevice(){
        findViewById(R.id.progressBar).setVisibility(View.GONE);
        // devo far vedere il pulsante "start"
        findViewById(R.id.main_container).setVisibility(View.VISIBLE);
        findViewById(R.id.button_start).setVisibility(View.VISIBLE);
        findViewById(R.id.dots_iv).setVisibility(View.VISIBLE);
    }

    @Override
    public void noBluetooth() {

    }
    private void disegnaGrafico() {
        if (sessione != null){
            if (grafico != null) {
                if (grafico.getData() == null) {
                    ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
                    ILineDataSet maxLine = getMaxLine();
                    maxLine.addEntry(new Entry(0,0));
                    dataSets.add(maxLine);
                    ILineDataSet minLine = getMinLine();
                    minLine.addEntry(new Entry(0,0));
                    dataSets.add(minLine);
                    ILineDataSet compLine = getCompLine();
                    compLine.addEntry(new Entry(0,0));
                    dataSets.add(compLine);
                    dataSets.add(getFakeLine());
                    LineDataSet pressureLine = getNewPressureLine();
                    pressureLine.setColor(getResources().getColor(R.color.soffiocolor));
                    dataSets.add(pressureLine);
                    LineData linedata = new LineData(dataSets);
                    grafico.setData(linedata);

                    for(Misura m : sessione.getData()){
                        grafico.getData().addEntry(new Entry(m.getTimestamp(), 0), COMP_PRESSURE_INDEX+1);
                        if (sessione.getMaxPressure() != null)
                            grafico.getData().addEntry(new Entry(m.getTimestamp(), sessione.getMaxPressure()), MAX_PRESSURE_INDEX);
                        if (sessione.getMinPressure() != null)
                            grafico.getData().addEntry(new Entry(m.getTimestamp(), sessione.getMinPressure()), MIN_PRESSURE_INDEX);
                        if (sessione.getCompPressure() != null)
                            grafico.getData().addEntry(new Entry(m.getTimestamp(), sessione.getCompPressure()), COMP_PRESSURE_INDEX);
                        //TODO COLORI
                        grafico.getData().addEntry(new Entry(m.getTimestamp(), m.getPressione()), getPressureDataSetIndex());
                        int lineColor = sessione.getLineColor(m.getTimestamp());
                        if (lastLineColor == null || lineColor != lastLineColor){
                            pressureLine = getNewPressureLine();
                            pressureLine.setColor(lineColor);
                            grafico.getData().addDataSet(pressureLine);
                            grafico.getData().addEntry(new Entry(m.getTimestamp(), m.getPressione()), getPressureDataSetIndex());
                            lastLineColor = lineColor;
                        }
                    }

                    grafico.notifyDataSetChanged();
                }
            }
        }
    }

    private boolean canRecord = false;

    private Misura setMisureparams(ArrayList<Misura> misure, Misura m, float pressioneLorda, float temperatura, float pressioneNetta){
        m.setPressure(pressioneNetta);
        m.setRawPressure(pressioneLorda);
        m.setTemperature(temperatura);
        m.setTimestamp(misure.size() * 125);
        return m;
    }

    @Override
    public void pressioneChanged(float pressioneLorda, float temperatura, float pressioneNetta) {
       // Log.i("Lettore", "REC ACTIVITY  PRESSIONE CAMBIATA");
        //ho tarato lo strumento nel momento in cui mi sono connessa
        //dopo devo registrare e disegnare il grafico.
        if (sessione != null && !readOnlyActivity){
            ArrayList<Misura> misure = sessione.getData();
            final Misura m = new Misura();
            if(canRecord){
                setMisureparams(misure, m,pressioneLorda, temperatura, pressioneNetta);
            } else {
                //se canRecod è false, si valuta la pressione registrata.
                //Se supera 3 millibar, si imposta canRecord a true e tutte le misurazioni successive saranno registrate, altrimenti verranno ignorate
                if(pressioneNetta > 3 || pressioneNetta < -3){
                    canRecord = true;
                    setMisureparams(misure, m, pressioneLorda, temperatura, pressioneNetta);
                }else{
                    setMisureparams(misure, m,0, 0, 0);
                }
            }
            if (m.getTimestamp() > sessione.getTempoTotale()) { //se ho finito il tempo, chiudo tutto.
                iniziaRegistrazione(null);
                return;
            }
            final float pressioneMisurata = m.getPressione();
            sessione.addMisura(m);
            if (grafico != null){
                if (grafico.getData() == null){ //se non ho ancora creato il grafico, aggiugno le 4 possibili linee che devo far vedere
                    ArrayList<ILineDataSet> dataSets = new ArrayList<ILineDataSet>();
                    dataSets.add(getMaxLine());
                    dataSets.add(getMinLine());
                    dataSets.add(getCompLine());
                    dataSets.add(getNewPressureLine());
                    LineData linedata = new LineData(dataSets);
                    grafico.setData(linedata);
                }
                //adesso sicuramente il grafico ha le "linee" definite, quindi posso aggiungere i punti.
                // il punto che ho misurato a devo aggiungerlo alla linea della pressione
                //lo disegno del colore vecchio. Tanto se poi cambio colore, questo sarà il "punto finale" della linea vecchia.
                grafico.getData().addEntry(new Entry(m.getTimestamp(), pressioneMisurata), getPressureDataSetIndex());
                //controllo il colore. Se cambia, ridisegno il punto appena trovato con il colore nuovo, in modo che sia il "colore iniziale" della nuova linea.
                int lineColor = sessione.getLineColor(m.getTimestamp());
                if (lastLineColor == null || lineColor != lastLineColor){
                    LineDataSet pressureLine = getNewPressureLine();
                    pressureLine.setColor(lineColor);
                    grafico.getData().addDataSet(pressureLine);
                    grafico.getData().addEntry(new Entry(m.getTimestamp(), pressioneMisurata), getPressureDataSetIndex());
                    lastLineColor = lineColor;
                }
                //le altre tre linee, se ci sono, devono anch'essere essere allungate di un punto

                if (sessione.getMaxPressure() != null){
                    grafico.getData().addEntry(new Entry(m.getTimestamp(), sessione.getMaxPressure()), MAX_PRESSURE_INDEX);
                }
                if (sessione.getMinPressure() != null){
                    grafico.getData().addEntry(new Entry(m.getTimestamp(), sessione.getMinPressure()), MIN_PRESSURE_INDEX);
                }
                if (sessione.getCompPressure() != null){
                    grafico.getData().addEntry(new Entry(m.getTimestamp(), sessione.getCompPressure()), COMP_PRESSURE_INDEX);
                }
                grafico.notifyDataSetChanged(); //dico al grafico di aggiornarsi
                //muovo il grafico in modo che la parte che sto inserendo sia visibile.
                if (misure.size() > 0){
                    grafico.setVisibleXRange(16000,16000); //sulla x farò vedere 16 secondi
                    grafico.moveViewToX(m.getTimestamp()); //sposto il grafico in modo che la fine della linea che sto disegnando, sia sempre visibile.

                }
                if (misure.size()%8 == 1 && misure.size() > 0){ //è inutile cambiare le lable di tempo e pressione a ogni misurazione, l'occhio non fa in tempo a leggerle
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            //aggiorno tempo rimasto
                            if (sessione != null){
                                long secOra = m.getTimestamp()/1000;
                                int indexOra = sessione.getIntervalloCorrente(m.getTimestamp());
                                ArrayList<Integer> intervalliFinora = new ArrayList<Integer>();
                                for (int i = 0; i <= indexOra; i++){
                                    intervalliFinora.add(sessione.getSteps().get(i));
                                }
                                long tempoRimasto = ModelliActivity.getTotalTimeSec(intervalliFinora) - secOra;
                                tempoView.setText(DateUtils.formatElapsedTime(tempoRimasto));
                                //aggiorno label pressione
                                managePressureText(pressioneMisurata);
                                //aggiorno label giri
                                giriView.setText(Integer.toString(sessione.getNGiriTrascorsi(m.getTimestamp())) + "/" +sessione.getNGiriTotali());

                                //muovo l'indicatore nella barra del modello
                                LinearLayout v = findViewById(R.id.scrolling_time_indicator);

                                float scrollPixel = (((float)v.getWidth()/(float)sessione.getTempoTotale())*(float)m.getTimestamp());
                                v.setPivotX(v.getWidth());
                                v.setScaleX(1-(scrollPixel/v.getWidth()));

                                /*long tempoIntervalliRimasti = 0;
                                for (int i = indexOra; i <= sessione.getSteps().size() - 1; i++){
                                    tempoIntervalliRimasti += sessione.getSteps().get(i);
                                }*/
                                //countdown secondi
                                if(tempoRimasto <= 5){
                                    //playCountSound((int) (tempoRimasto));
                                }
                            }
                        }

                    });

                    //se supero la soglia minima, avviamo il suono
                    if(compensazioneValue != null) {
                        if (pressioneMisurata > minPressureSound) {
                            if (pressureSound > pressioneMisurata - 2 || pressureSound < pressioneMisurata + 2) {
                                pressureSound = pressioneMisurata;
                                //soundThread.setPause(false);//TODO da rimettere
                            }
                        } else {
                            //altrimenti si ferma il suono
                            //soundThread.setPause(true);//TODO da rimettere
                        }
                    }

                }

            }
        }
    }

    /*private void playSoundPool() {
        int MAX_STREAMS = 20;
        int REPEAT = 0;
        SoundPool soundPool = new SoundPool(MAX_STREAMS, AudioManager.STREAM_MUSIC, REPEAT);
        soundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {

            @Override
            public void onLoadComplete(SoundPool soundPool, int soundId, int status) {
                int priority = 0;
                int repeat = 0;
                float rate = 1.f; // Frequency Rate can be from .5 to 2.0
                // Set volume
                AudioManager mgr = (AudioManager)getSystemService(Context.AUDIO_SERVICE);
                float streamVolumeCurrent = mgr.getStreamVolume(AudioManager.STREAM_MUSIC);
                float streamVolumeMax = mgr.getStreamMaxVolume(AudioManager.STREAM_MUSIC);
                float volume = streamVolumeCurrent / streamVolumeMax;
                // Play it
                soundPool.play(soundId, volume, volume, priority, repeat, rate);
            }
        });
        soundPool.load(this, R.raw.sound, 1);
    }*/

    private int getPressureDataSetIndex() {
        return grafico.getData().getDataSets().size() -1;
    }
    private int getPressureDataSetIndex(long timestamp) {
        return sessione.getIntervalloCorrente(timestamp) + 5;
    }


    @Override
    public void batteriaChanged(long batteria) {
        //non dovrebbe servire
    }

    @Override
    public void disconnectedFromDevice() {
        deviceConnected = false;
    }

    private void condividiSessione(String tipo){
        Intent shareIntent;
        File file;
        switch (tipo) {
            case "png":
                String filepath = null;
                try {
                    filepath = getExternalFilesDir(Environment.DIRECTORY_PICTURES).getCanonicalPath();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                boolean success = grafico.saveToPath("grafico", filepath);
                if (success) {
                    shareIntent = new Intent(Intent.ACTION_SEND);
                    shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(filepath + "/" + "grafico.png"));
                    shareIntent.setType("image/png");
                    startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.scegli_share)));
                } else {
                    Toast.makeText(this, getResources().getString(R.string.impossibile_salvare_grafico), Toast.LENGTH_SHORT).show();
                }
                break;
            case "eqtool":
                Gson gson = new Gson();
                sessione.setStartDate(sessione.getDInizioSessione());
                sessione.setStopDate(sessione.getDFineSessione());
                // Java object to JSON string
                String sessionJson = gson.toJson(sessione);
                file = createFile(sessionJson, "session.eqtool");
                shareIntent = createIntent(file, "text/plain");
                startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.scegli_share)));
                break;
            case "csv":
                String csvString = "TIMESTAMP;TEMPERATURA;PRESSIONE\n";
                if (sessione.getData() != null && sessione.getData().size() > 0) {
                    for (Misura m : sessione.getData()) {
                        csvString = csvString + Long.toString(m.getTimestamp()) + ";" + Float.toString(m.getTemperature()) + ";" + Float.toString(m.getPressione()) + "\n";
                    }
                    file = createFile(csvString, "session.csv");
                    shareIntent = createIntent(file, "text/csv");
                    startActivity(Intent.createChooser(shareIntent, getResources().getString(R.string.scegli_share)));
                }
                break;
            default:
                break;
        }
    }

    /**
     * Rinomina la sessione
     */
    public void renameSession() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Title");

        // Set up the input
        final EditText input = new EditText(this);
        // Specify the type of input expected; this, for example, sets the input as a password, and will mask the text
        input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);
        input.setText(sessione.getDescr());
        builder.setView(input);

        // Set up the buttons
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String descrizione = input.getText().toString();
                if(!"".equals(descrizione)) {
                    updateDescrizioneSessione(descrizione);
                }
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    private void updateDescrizioneSessione(final String descrizione){
        sessione.setDescr(descrizione.trim());
        sessione.salvaDescrizioneSessione();
        addDescriptionTitle(descrizione);
    }

    private void addDescriptionTitle(final String descrizione) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                TextView v = findViewById(R.id.descrizione_sessione);
                v.setText(descrizione);
                v.setVisibility(View.VISIBLE);
            }
        });
    }

    private Intent createIntent(File file, String type) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        if (Build.VERSION.SDK_INT >= 24) {
            //se API >= 24 ==> usare FileProvider
            Uri photoURI = FileProvider.getUriForFile(this, getApplicationContext().getPackageName() + ".fileprovider", file);
            shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            shareIntent.putExtra(Intent.EXTRA_STREAM, photoURI);
        } else {
            shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
        }
        shareIntent.setType(type);
        return shareIntent;
    }

    private File createFile(String stringFile, String fileName) {
        final File path = getExternalFilesDir(Environment.DIRECTORY_DOCUMENTS);
        if (!path.exists()) {
            path.mkdirs();
        }
        final File file = new File(path, fileName);
        try {
            file.createNewFile();
            FileOutputStream fOut = new FileOutputStream(file);
            OutputStreamWriter myOutWriter = new OutputStreamWriter(fOut);
            myOutWriter.append(stringFile);
            myOutWriter.close();
            fOut.flush();
            fOut.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed " + e.toString());
        }
        return file;
    }

    /**FUNZIONI CHE SERVONO PER DISEGNARE IL GRAFICO*/
    private LineDataSet getMaxLine(){
        LineDataSet line = new LineDataSet(null, null);
        line.setDrawCircles(false);
        line.setDrawCircleHole(false);
        line.setLineWidth(1); //TODO lo spessore è 1???
        line.setColor(getResources().getColor(R.color.redbutton));
        line.setHighlightEnabled(false);

        return line;
    }
    private LineDataSet getMinLine(){
        LineDataSet line = new LineDataSet(null, null);
        line.setDrawCircles(false);
        line.setDrawCircleHole(false);
        line.setLineWidth(1); //TODO lo spessore è 1???
        line.setColor(getResources().getColor(R.color.redbutton));
        line.setHighlightEnabled(false);
        return line;
    }
    private LineDataSet getFakeLine(){
        LineDataSet line = new LineDataSet(null, null);
        line.setDrawCircles(false);
        line.setDrawCircleHole(false);
        line.setLineWidth(1); //TODO lo spessore è 1???
        line.setColor(getResources().getColor(R.color.background));
        line.setHighlightEnabled(false);
        return line;
    }
    private LineDataSet getCompLine(){
        LineDataSet line = new LineDataSet(null, null);
        line.setDrawCircles(false);
        line.setDrawCircleHole(false);
        line.setLineWidth(1); //TODO lo spessore è 1???
        line.setColor(getResources().getColor(R.color.greenbutton));
        line.setHighlightEnabled(false);
        return line;
    }
    private LineDataSet getNewPressureLine(){
        LineDataSet line = new LineDataSet(null, null);
        line.setDrawCircles(false);
        line.setDrawCircleHole(false);
        line.setLineWidth(2);
        line.setHighlightEnabled(true);
        return line;
    }

    @Override
    public void onValueSelected(Entry e, Highlight h) {
        managePressureText(e.getY());
        long secOra = (long)e.getX()/1000;
        tempoView.setText(DateUtils.formatElapsedTime(secOra));
        giriView.setText(Integer.toString(sessione.getNGiriTrascorsi((long)e.getX())) + "/" +sessione.getNGiriTotali());
    }

    //si imposta il colore ed il testo della pressione
    private void managePressureText(Float value) {
        String pressioneString = String.format(Locale.getDefault(), "%.1f",value )/*Float.toString(m.getPressione()/100)*/ + "hPa";
        pressioneView.setText(pressioneString);
        int colorInt = getPressureColor(value);
        pressioneView.setTextColor(ContextCompat.getColor(this, colorInt));
    }

    //si ricava il colore per il testo della pressione
    private int getPressureColor(float val) {
        if(compensazioneValue != null){
            Float diff = compensazioneValue - val;
            if(diff > 3) {
                return R.color.colorInactive;
            }
            if(diff > -3){
                return R.color.greenbutton;
            }
            if(val < maxValueForColor){
                return R.color.orangebutton;
            }
            return R.color.redbutton;
        }
        return R.color.colorInactive;

    }

    @Override
    public void onNothingSelected() {
        pressioneView.setText("");
        tempoView.setText("");
        giriView.setText("");
    }

    @Override
    public void onChartGestureStart(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartGestureEnd(MotionEvent me, ChartTouchListener.ChartGesture lastPerformedGesture) {

    }

    @Override
    public void onChartLongPressed(MotionEvent me) {

    }

    @Override
    public void onChartDoubleTapped(MotionEvent me) {

    }

    @Override
    public void onChartSingleTapped(MotionEvent me) {
       float xValue = me.getX();
        float indexInf = (long)(xValue/125);
        if( (xValue-(indexInf*125)) < (((indexInf + 1)*125)-xValue)){
            xValue = 125 * indexInf;
        }else{
            xValue = 125 * (indexInf + 1);
        }
        List<Entry> list = grafico.getData().getDataSetByIndex(getPressureDataSetIndex((long)xValue)).getEntriesForXValue(xValue);
        final Entry entry = list.get(0);

        if (entry != null /*&& _lastTappedIndex != entry.getX()*/) {
            grafico.highlightValue(entry.getX(), entry.getY(), getPressureDataSetIndex((long)xValue));

        }
        managePressureText(entry.getY());
        long secOra = (long)me.getX()/1000;
        tempoView.setText(DateUtils.formatElapsedTime(secOra));
    }

    @Override
    public void onChartFling(MotionEvent me1, MotionEvent me2, float velocityX, float velocityY) {

    }

    @Override
    public void onChartScale(MotionEvent me, float scaleX, float scaleY) {

    }

    @Override
    public void onChartTranslate(MotionEvent me, float dX, float dY) {

    }

    private void playCountSound(int l){
        //voce countdown
        if(countDownMediaPlayer != null){
            countDownMediaPlayer.release();
        }
        switch (l){
            case 1:
                countDownMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.cd1);
                break;
            case 2:
                countDownMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.cd2);
                break;
            case 3:
                countDownMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.cd3);
                break;
            case 4:
                countDownMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.cd4);
                break;
            case 5:
                countDownMediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.cd5);
                break;
            default:
                countDownMediaPlayer = null; //MediaPlayer.create(getApplicationContext(), R.raw.sound);
                break;
        }
        if(countDownMediaPlayer != null) {
            countDownMediaPlayer.start();
        }
    }

    private class SoundThread extends Thread{

        private MediaPlayer mediaPlayer;
        private boolean isPaused; //in pausa all'inizio
        private boolean isRunning;
        private double coeff;

        public SoundThread(){
            super("SoundThread");
            mediaPlayer = MediaPlayer.create(getApplicationContext(), R.raw.sound);
            this.isRunning = true;
            this.isPaused = true;
            this.coeff = (double) 1000 / (minPressureSound - maxPressureSound);
        }

        public void run() {
            while(isRunning) {
                //delay = 1000 * ( -1 / (maxPressureSound - minPressureSound) ) * (pressureSound - maxPressureSound)
                long delay;
                if(pressureSound > minPressureSound){
                    delay = (long) Math.max(0, coeff * (pressureSound - maxPressureSound) );
                }else {
                    delay = 250;
                }
                Log.d("DELAY TIMER SOUND", String.valueOf(delay));
                if(!isPaused) {
                    mediaPlayer.start();
                }
                try {
                    Thread.sleep(delay);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            if(mediaPlayer != null){
                try {
                    mediaPlayer.stop();
                    mediaPlayer.release();
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        }

        public void setPause(boolean isPaused){
            this.isPaused = isPaused;
        }

        public void stopThread(){
            this.isRunning = false;
        }

    }

}
