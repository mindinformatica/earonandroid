package mindinformatica.com.earon.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import androidx.appcompat.app.AppCompatActivity;
import android.util.Log;

import java.util.ArrayList;

import mindinformatica.com.earon.rec.Sessione;


/**
 * Created by camillacecconi on 26/07/17.
 */

public class Modello implements TableInterface {
    public Modello() {
    }

    public Modello(ArrayList<Integer> intervalli, String nomeTemplate) {
        this.intervalli = intervalli;
        this.nomeTemplate = nomeTemplate;
    }

    public static final String TABLE_NAME = "Modelli";
    public static final String COLUMN_ID_MODELLO = "_id";
    public static final String TYPE_ID_MODELLO = TYPE_INTEGER;
    public static final String COLUMN_NOME = "Nome";
    public static final String TYPE_NOME = TYPE_TEXT;
    public static final String COLUMN_INTERVALLI = "Intervalli";
    public static final String TYPE_INTERVALLI = TYPE_TEXT;

    private Long id;
    private String nomeTemplate;
    private ArrayList<Integer> intervalli;


    public String getNomeTemplate() {
        return nomeTemplate;
    }

    public void setNomeTemplate(String nomeTemplate) {
        this.nomeTemplate = nomeTemplate;
    }

    public ArrayList<Integer> getIntervalli() {
        return intervalli;
    }

    public void setIntervalli(ArrayList<Integer> intervalli) {
        this.intervalli = intervalli;
    }

    @Override
    public String getCreateString() {
        return "CREATE TABLE " + TABLE_NAME + " (" +
                COLUMN_ID_MODELLO + " " + TYPE_ID_MODELLO + " PRIMARY KEY, " +
                COLUMN_NOME + " " + TYPE_NOME + "," +
                COLUMN_INTERVALLI + " " + TYPE_INTERVALLI + ")";
    }

    @Override
    public String getDropString() {
        return "DROP TABLE IF EXISTS " + TABLE_NAME;
    }
    public ContentValues createContentValues(){
        ContentValues values = new ContentValues();
        //non metto l'id, si riempie da solo con uno non ancora preso. Potrebbe riutilizzare l'id di righe cancellate.
        values.put(COLUMN_NOME, nomeTemplate);
        values.put(COLUMN_INTERVALLI, Sessione.intervalliToString(intervalli));
        return values;
    }

    public void fillFromCursor(Cursor crs){
        //quando arrivo qui, il cursore deve già indicarmi l'elemento che mi interessa
        id = crs.getLong(crs.getColumnIndexOrThrow(COLUMN_ID_MODELLO));
        nomeTemplate = crs.getString(crs.getColumnIndexOrThrow(COLUMN_NOME));
        intervalli = Sessione.staticStringToIntervalli(crs.getString(crs.getColumnIndexOrThrow(COLUMN_INTERVALLI)));

    }
    public void inserisciModello(Context context){
        DbHelper db = new DbHelper(context);
        try{
            long id = db.getWritableDatabase().insertOrThrow(TABLE_NAME, null, createContentValues());
            Log.i("INSERITO", Long.toString(id));
        }finally {
            db.close();
        }


    }
    public void salvaModello(Context context, Long id){
        DbHelper db = new DbHelper(context);
        try{
            String whereString = COLUMN_ID_MODELLO + " = " +Long.toString(id);
            db.getWritableDatabase().update(TABLE_NAME, createContentValues(), whereString, null);
        }finally {
            db.close();
        }

    }
    public static void deleteModello(Context c, Long id){
        DbHelper db = new DbHelper(c);
        try{
            String whereString = COLUMN_ID_MODELLO + " = " +Long.toString(id);
            db.getWritableDatabase().delete(TABLE_NAME, whereString, null);
        }finally {
            db.close();
        }

    }
    public static ArrayList<Modello> getModelli(Context c){
        DbHelper db = new DbHelper(c);
        try{
            Cursor crs = db.getReadableDatabase().query(TABLE_NAME, null, null, null, null, null, COLUMN_NOME); //colonne: tutte, colonne where: nessuno, valore where: nessuno
            ArrayList<Modello> modelli = new ArrayList<>();
            if (crs != null && crs.getCount() > 0){;
                while (crs.moveToNext()){
                    Modello s = new Modello();
                    try{
                        s.fillFromCursor(crs);
                        modelli.add(s);
                    }catch(NumberFormatException e){
                        e.printStackTrace();
                    }


                }
            }
            return modelli;
        }finally {
            db.close();
        }

    }
    public static Modello getModello(AppCompatActivity c, Long id) {
        DbHelper db = new DbHelper(c);
        try{
            String whereString = COLUMN_ID_MODELLO + " = " +Long.toString(id);
            // String[] whereArgs = {id};
            Cursor crs = db.getReadableDatabase().query(TABLE_NAME, null, whereString, null, null, null, null); //colonne: tutte, colonne where: nessuno, valore where: nessuno

            if (crs != null && crs.getCount() > 0) {
                crs.moveToFirst();
                Modello s = new Modello();
                s.fillFromCursor(crs);

                return s;
            }else{
                return null;
            }
        }finally {
            db.close();
        }

    }

    public long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
