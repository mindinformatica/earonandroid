package mindinformatica.com.earon.jellyfish;

import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;

import mindinformatica.com.earon.utils.Constants;

/**
 * classe che contiene le costanti utili per il gioco Jellyfish
 * Created by lorenzo on 18/07/18.
 */

public class JellyConstants extends Constants {
    private static final double G = 9.81;
    private static int TOP_MARGIN;
    static float SOGLIA_COMPENSAZIONE;
    static double G_PIXEL; //G non in m/s^2, ma pixel/s^2
    static float MASSA;
    static final float MULTIPLICATIVE_CONSTANT = 1;
    private final static int SCREEN_METERS = 6; //la finta altezza del fondale per il gioco
    static final Integer targetFPS = 24; //numero di frame per secondo desiderati
    static int JELLY_WIDTH;
    static int JELLY_HEIGHT;
    static boolean DEVICE_CONNECTED = false;
    final static int STANDARD_HEIGHT = 1200;

    public static void setgPixel(int yNumPixel) {
        G_PIXEL = (G * yNumPixel) / SCREEN_METERS;
        //G_PIXEL = (G * STANDARD_HEIGHT) / SCREEN_METERS;
    }

    public static int getTopMargin() {
        return TOP_MARGIN;
    }

    public static double getInterval(){
        return (double) 1 / targetFPS;
    }

    /**
     *
     * @param options le option per ottimizzare il caricamento dell'immagine (Bitmap)
     * @param reqWidth la larghezza desiderata
     * @param reqHeight l'altezza desiderata
     * @return Il parametro di scala InSampleSize
     */
    public static int calculateInSampleSize(BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    /**
     *
     * @param res Le Resources
     * @param resId Id dell'immagine da prendere
     * @param reqWidth larghezza desiderata immagine
     * @param reqHeight altezza desiderata immagine
     * @return la bitmap da disegnare
     */
    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId, int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    /**
     * cerco il primo pixel delle onde che servirà come margine superiore del gioco, oltre il quale jelly muore
     * @param bitmap la bitmap dello sfondo
     */
    public static void setTopMargin(Bitmap bitmap) {

        int bitmapX = JELLY_WIDTH / 2;
        for (int i = 0; i < JELLY_HEIGHT; i++) {
            int pixel = bitmap.getPixel(bitmapX, i);
            if(pixel != 0 && Color.red(pixel) != 255 && Color.blue(pixel) != 255 && Color.blue(pixel) != 255) {
                TOP_MARGIN = i;
                break;
            }
        }
    }

}
