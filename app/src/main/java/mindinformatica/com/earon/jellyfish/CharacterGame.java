package mindinformatica.com.earon.jellyfish;

import android.graphics.Canvas;

/**
 * Created by lorenzo on 18/07/18.
 */

public interface CharacterGame {

    public void update();
    public void draw(Canvas canvas);
}
