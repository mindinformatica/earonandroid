package mindinformatica.com.earon;

import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import mindinformatica.com.earon.rec.ModelliActivity;
import mindinformatica.com.earon.rec.RecActivity;
import mindinformatica.com.earon.rec.Sessione;
import mindinformatica.com.earon.utils.ModelloBarView;

/**
 * Created by camillacecconi on 11/07/17.
 */

public class FragmentDiario extends Fragment {

    ArrayList<Sessione> lista;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_diario, container, false);
        ListView lv = rootView.findViewById(R.id.lv_diario);
        lista = Sessione.getSessioni(getActivity());
        if (lista == null){
            lista = new ArrayList<>();
        }
        //if (lista != null){
            final DiarioAdapter ad = new DiarioAdapter(getActivity(), lista);
            lv.setAdapter(ad);
            /*lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    Intent intent = new Intent(getActivity(), RecActivity.class);
                    intent.putExtra("sessioneId", ad.getItem(i).getId());
                    intent.putExtra("readonly", true);
                    startActivity(intent);
                }
            });*/
            /*lv.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {

                    return false;
                }
            });*/
       // }
        return rootView;
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getActivity() != null && isVisibleToUser){
            lista = Sessione.getSessioni(getActivity());
            if (lista != null && lista.size() > 0){
                if (getView() != null){
                    ListView lv = getView().findViewById(R.id.lv_diario);
                    if (lv.getAdapter() != null){
                        final DiarioAdapter ad = new DiarioAdapter(getActivity(), lista);
                        lv.setAdapter(ad);
                        ad.notifyDataSetChanged();
                    }
                }
            }
            View fab = getActivity().findViewById(R.id.fab);
            fab.setVisibility(View.VISIBLE);
        }

    }
    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null){
            lista = Sessione.getSessioni(getActivity());
            if (lista == null){
                lista = new ArrayList<>();
            }
            ListView lv = getView().findViewById(R.id.lv_diario);
            if (lv.getAdapter() != null){
                final DiarioAdapter ad = new DiarioAdapter(getActivity(), lista);
                lv.setAdapter(ad);
                ad.notifyDataSetChanged();
            }
        }
        View fab = getActivity().findViewById(R.id.fab);
        fab.setVisibility(View.VISIBLE);
    }

    private class DiarioAdapter extends  ArrayAdapter<Sessione>{

        public DiarioAdapter(Context context, List<Sessione> objects) {
            super(context, R.layout.diario_list_item, objects);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View v = convertView;
            if (v == null) {
                LayoutInflater vi;
                vi = LayoutInflater.from(getContext());
                v = vi.inflate(R.layout.diario_list_item, null);
            }
            manageHeaderTitle(position, v);

            v.setTag(position);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    SharedPreferences prefs = getActivity().getSharedPreferences(
                            "com.example.app", Context.MODE_PRIVATE);
                    prefs.edit().putInt("viewpager_position", 0).apply();
                    Intent intent = new Intent(getActivity(), RecActivity.class);
                    intent.putExtra("sessioneId", getItem((int)view.getTag()).getId());
                    intent.putExtra("readonly", true);
                    startActivity(intent);
                }
            });
            v.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    final int deletePosition = (int)view.getTag();
                    AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());

                    alert.setTitle(getResources().getString(R.string.delete));
                    alert.setMessage(getResources().getString(R.string.delete_explain));
                    alert.setPositiveButton(getResources().getString(R.string.ok), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TOD O Auto-generated method stub

                            // main code on after clicking yes
                            Sessione.deleteSessione(getActivity(), getItem(deletePosition).getId());
                            remove(getItem(deletePosition));
                            notifyDataSetChanged();
                            notifyDataSetInvalidated();

                        }
                    });
                    alert.setNegativeButton(getResources().getString(R.string.annulla), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    });

                    alert.show();
                    return false;
                }
            });
            Sessione s = getItem(position);
            Drawable img = getDrawable(s);
            ((ImageView)v.findViewById(R.id.img_profilo)).setImageDrawable(img);
            String datePattern = android.text.format.DateFormat.getBestDateTimePattern(Locale.getDefault(), "ddMMyyyyHHmm");
            SimpleDateFormat dateFormat = new SimpleDateFormat(datePattern);
            String data = dateFormat.format(s.getDInizioSessione());
            String descr = s.getDescr();
            if(descr != null && !"".equals(descr)){
                if(descr.length() > 9){
                    descr = descr.substring(0, 9) + "...";
                }
                data = descr + "  " + data;
            }
            ((TextView)v.findViewById(R.id.data_tv)).setText(data);
            long durata = ModelliActivity.getTotalTimeSec(s.getSteps());
            ((TextView)v.findViewById(R.id.durata_tv)).setText(DateUtils.formatElapsedTime(durata) + getResources().getString(R.string.secondi_um));
            ((TextView)v.findViewById(R.id.profilo_tv)).setText(getResources().getString(R.string.profilo) + ": " + s.getMode());

            ModelloBarView barra = v.findViewById(R.id.modello_bar_diario);
            barra.setIntervalli(s.getSteps());
            barra.setActive(false, v);
            return v;
        }

        private void manageHeaderTitle(int position, View v){
            TextView headerTitle = v.findViewById(R.id.header_diario_title);
            if(position > 0) {
                if (lista.get(position).getImported() != (lista.get(position - 1).getImported())) {
                    headerTitle.setText(getString(R.string.imported));
                    headerTitle.setVisibility(View.VISIBLE);
                }else{
                    headerTitle.setVisibility(View.GONE);
                }
            }else if(position == 0){
                if(lista.get(position).getImported()) {
                    headerTitle.setText(getString(R.string.imported));
                    headerTitle.setVisibility(View.VISIBLE);
                }else{
                    //headerTitle.setText(getString(R.string.mie));
                    headerTitle.setVisibility(View.GONE);
                }
                //headerTitle.setVisibility(View.VISIBLE);
            }else{
                headerTitle.setVisibility(View.GONE);
            }
        }

        private Drawable getDrawable(Sessione s) {
            String profilo = s.getMode();
            int id = R.drawable.free_new;
            if (getResources().getString(R.string.frenzel).equals(profilo)){
                id = R.drawable.frenzel_new;
            }else if (getResources().getString(R.string.ad_frenzel).equals(profilo)){
                id = R.drawable.advanced_frenzel_new;
            }else if (getResources().getString(R.string.ad_frenzel_2).equals(profilo)){
                id = R.drawable.advanced_frenzel_2_new;
            }else if (getResources().getString(R.string.rev_pack).equals(profilo)){
                id = R.drawable.reverse_packing_new;
            }else if (getResources().getString(R.string.mouthfill).equals(profilo)){
                id = R.drawable.mouthfill_new;
            }else if (getResources().getString(R.string.mouthfill_refill).equals(profilo)){
                id = R.drawable.mouthfill_refill_new;
            }
            return getResources().getDrawable(id);
        }
    }
}
