package mindinformatica.com.earon;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.FragmentManager;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.legacy.app.FragmentPagerAdapter;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;

import java.util.Date;

import mindinformatica.com.earon.bluetooth.BluetoothActivity;
import mindinformatica.com.earon.impostazioni.FragmentImpostazioni;
import mindinformatica.com.earon.impostazioni.InfoActivity;
import mindinformatica.com.earon.impostazioni.ListaDevicesActivity;
import mindinformatica.com.earon.impostazioni.NoteLegaliActivity;
import mindinformatica.com.earon.impostazioni.SoglieActivity;
import mindinformatica.com.earon.jellyfish.FragmentJellyfish;
import mindinformatica.com.earon.jellyfish.JellyfishActivity;
import mindinformatica.com.earon.rec.ModelliActivity;
import mindinformatica.com.earon.rec.ProfiliActivity;
import mindinformatica.com.earon.rec.RecActivity;
import mindinformatica.com.earon.rec.Sessione;
import mindinformatica.com.earon.tutorial.FragmentTutorial;

import static android.view.View.INVISIBLE;

public class MainActivity extends BluetoothActivity {
    private static final int DIARIO_TAB_INDEX = 0;
    private static final int IMPOSTAZIONI_TAB_INDEX = 1;
    private static final int TUTORIAL_TAB_INDEX = 2;
    private static final int JELLYFISH_TAB_INDEX = 3;
    private static final int REQUEST_MODEL = 2;
    private boolean deviceConnected = false;
    private TabLayout tabLayout;
    private BluetoothDevice device;
    private String soglia;

    private static final int IMPORT_REQUEST_CODE = 42;


    /**
     * The {@link PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link FragmentStatePagerAdapter}.
     */
    private SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private String savedDeviceAddress;
    private static int defaultPageSelected = IMPOSTAZIONI_TAB_INDEX;
    private boolean isOnPause = false;

    /*@Override
    protected void onPause() {
        super.onPause();
        Log.i("ON PAUSE", "going to disconnect MAIN ACTIVITY");
        disconnect();
    }*/
    @Override
    public void onPause() {
        super.onPause();
        this.isOnPause = true;
        /*if (BluetoothActivity.class.isAssignableFrom((this.getClass()))){
            this.disconnect();
        }*/
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

          // Create the adapter that will return a fragment for each of the four
        // primary sections of the activity.
        mSectionsPagerAdapter = new SectionsPagerAdapter(getFragmentManager());

        // Set up the ViewPager with the sections adapter.
        mViewPager = findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                int titoloId = R.string.app_name;
                switch (position){
                    case TUTORIAL_TAB_INDEX:
                        titoloId = R.string.tab_tutorial;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                View fab = findViewById(R.id.fab);
                                fab.setVisibility(View.INVISIBLE);
                                String urlToConnect;
                                if(deviceConnected) {
                                    urlToConnect = getResources().getString(R.string.tutorial_url);
                                }else{
                                    urlToConnect = getResources().getString(R.string.no_tutorial_url);
                                }
                                loadTutorialUrl(urlToConnect);
                            }
                        });
                        break;
                    case DIARIO_TAB_INDEX:
                        titoloId = R.string.tab_diario;
                        break;
                    case IMPOSTAZIONI_TAB_INDEX:
                        titoloId = R.string.tab_impostazioni;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                TextView tv = findViewById(R.id.periferica_connessa_tv);
                                if (tv != null && device != null){
                                    tv.setText(device.getName());
                                }else {
                                    setDefaulTextImpostazioni();
                                }
                            }
                        });
                        break;
                    case JELLYFISH_TAB_INDEX:
                        titoloId = R.string.tab_jellyfish;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                View fab = findViewById(R.id.fab);
                                fab.setVisibility(INVISIBLE);
                                //cambio il testo centrale
                                setTextViewJellyFragment();
                            }
                        });
                        break;
                }
                getSupportActionBar().setTitle(titoloId);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        tabLayout = findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);

        int length = tabLayout.getTabCount();
        for (int i = 0; i < length; i++) {
            tabLayout.getTabAt(i).setCustomView(getTabView(i));
        }
       // setupTabIcons(tabLayout);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();*/
                apriRec(view, MainActivity.this, mViewPager.getCurrentItem());
            }
        });

        // Figure out what to do based on the intent type
        if ("application/octet-stream".equals(getIntent().getType())) {
            Uri dataUri = getIntent().getData();
            getFileFromIntent(dataUri);
        }
    }

    @SuppressLint("ApplySharedPref")
    public void getFileFromIntent(Uri dataUri){
        try {
            String importedData = readTextFromUri(dataUri);
            Gson gson = new Gson();
            Sessione sessione = gson.fromJson(importedData, Sessione.class);
            if(sessione != null){
                sessione.setContext(this);
                sessione.setImported(true);

                Date dateStart = new Date((long) (sessione.getStartDate() * 1000));
                sessione.setDInizioSessione(dateStart);

                Date dateStop = new Date((long) (sessione.getStopDate() * 1000));
                sessione.setDFineSessione(dateStop);

                sessione.salvaSessione();
                SharedPreferences prefs = getSharedPreferences("com.example.app", Context.MODE_PRIVATE);
                prefs.edit().putInt("viewpager_position", DIARIO_TAB_INDEX).commit();
            }
        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(this, getResources().getString(R.string.impossibile_importare_sessione), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_import_session:
                performFileSearch();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    private View getTabView(int i) {
        LinearLayout l = new LinearLayout(this);
        l.setOrientation(LinearLayout.HORIZONTAL);
        l.setGravity(Gravity.CENTER);
        ImageView v = new ImageView(this);

        l.addView(v);
       // TextView tv = new TextView(this);
        //l.addView(tv);
        switch (i){
            case DIARIO_TAB_INDEX:
                v.setImageDrawable(getResources().getDrawable(R.drawable.ic_diario_selector));
         //       tv.setText(getResources().getString(R.string.tab_diario));
                break;
            case IMPOSTAZIONI_TAB_INDEX:
                v.setImageDrawable(getResources().getDrawable(R.drawable.ic_settings_selector));
          //      tv.setText(getResources().getString(R.string.tab_impostazioni));
                break;
            case TUTORIAL_TAB_INDEX:
                v.setImageDrawable(getResources().getDrawable(R.drawable.ic_tutorial_selector));
           //     tv.setText(getResources().getString(R.string.tab_tutorial));
                break;
            case JELLYFISH_TAB_INDEX:
                v.setImageDrawable(getResources().getDrawable(R.drawable.ic_jellyfish_selector));
                //     tv.setText(getResources().getString(R.string.tab_tutorial));
                break;
        }
        return l;
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.isOnPause = false;
        SharedPreferences prefs = this.getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        savedDeviceAddress = prefs.getString("device", null);
        mViewPager.setCurrentItem(prefs.getInt("viewpager_position", defaultPageSelected));
        prefs.edit().remove("viewpager_position").apply();
        boolean primaVolta = prefs.getBoolean("firstTime", true);
        if (primaVolta){
            apriNoteLegali(null);
        }
        soglia = prefs.getString("soglia", null);
        if (deviceConnected) {
            //imposto il livello della batteria dalle prefs, altrimenti compare la label di default
            connectedToDevice(device);
            //setBatteryFromPrefs(this, prefs);
        }
    }

    public static void setBatteryFromPrefs(MainActivity activity, SharedPreferences prefs){
        final TextView sub = activity.findViewById(R.id.periferica_connessa_sub);
        final String batteryLevel = prefs.getString("batteryLevel", "");
        if (sub != null) {
            activity.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    sub.setText(batteryLevel);
                }
            });
        }
    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("selectedPage", mViewPager.getCurrentItem());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null){
            defaultPageSelected = savedInstanceState.getInt("selectedPage", IMPOSTAZIONI_TAB_INDEX);
        }
    }

    /** COSE LEGATE AL BLUETOOTH **/

    @Override
    public void deviceFound(BluetoothDevice device) {
        //in questa activity se trovo un device, devo controllare se è quello che ho memorizzato, se lo è, mi connetto, altrimenti nulla
        if (savedDeviceAddress != null && savedDeviceAddress.equals(device.getAddress()) && device.getName() != null && device.getName().startsWith("UBA")){
            mb.connectToDevice(device);
        }
    }

    @Override
    public void deviceAlreadyConnected(BluetoothDevice mDevice) {
        deviceConnected = true;
        this.device = mDevice;
    }

    @Override
    public void deviceNotAlreadyConnected() {
        deviceConnected = false;
    }

    @Override
    public void connectedToDevice(final BluetoothDevice device) {
        deviceConnected = true;
        this.device = device;
        //qui dipende che fragment sto visualizzando. Se c'è il fragment delle impostazioni, devo scrivere il nome del device nella casella giusta, e indicare il livello della batteria
        if (mViewPager.getCurrentItem() == IMPOSTAZIONI_TAB_INDEX){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    TextView tv = findViewById(R.id.periferica_connessa_tv);
                    String name = device.getName();
                    if (tv != null) {
                        tv.setText(name);
                    }
                    setPrefDeviceName(name);
                    final TextView sub = findViewById(R.id.periferica_connessa_sub);
                    SharedPreferences prefs = getSharedPreferences(
                            "com.example.app", Context.MODE_PRIVATE);
                    final String batteryLevel = prefs.getString("batteryLevel", "");
                    if (sub != null) {
                        sub.setText(batteryLevel);
                    }
                }
            });

        }else if (mViewPager.getCurrentItem() == TUTORIAL_TAB_INDEX){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    loadTutorialUrl(getResources().getString(R.string.tutorial_url));
                }
            });
        }else if (mViewPager.getCurrentItem() == JELLYFISH_TAB_INDEX){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    setTextViewJellyFragment();
                }
            });
        }else if (mViewPager.getCurrentItem() == DIARIO_TAB_INDEX){
            String a = "";
        }
    }

    private void setPrefDeviceName(String name) {
        SharedPreferences.Editor editor = getSharedPreferences("com.example.app", Context.MODE_PRIVATE).edit();
        if(name != null && !"".equals(name)){
            editor.putString(getString(R.string.uba_device_name), name).apply();
        }else{
            editor.remove(getString(R.string.uba_device_name)).apply();
        }
    }

    @Override
    public void disconnectedFromDevice() {
        deviceConnected = false;
        this.device = null;

        if (mViewPager.getCurrentItem() == IMPOSTAZIONI_TAB_INDEX){
            if(!isOnPause) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setDefaulTextImpostazioni();
                    }
                });
                //scan(false);
            }
        }else if (mViewPager.getCurrentItem() == JELLYFISH_TAB_INDEX) {
            if(!isOnPause) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setTextViewJellyFragment();
                    }
                });
            }
        }
        /*else if (mViewPager.getCurrentItem() == TUTORIAL_TAB_INDEX){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    WebView wv = (WebView) findViewById(R.id.webview_tutorial);
                    String url = getResources().getString(R.string.no_tutorial_url);
                    Log.i("WEBVIEW", url);
                    if (wv != null)
                    wv.loadUrl(url);
                }
            });
            scan(false);
        }*/
    }

    private void loadTutorialUrl(String url){
        WebView wv = findViewById(R.id.webview_tutorial);
        //String url = getResources().getString(R.string.tutorial_url);
        Log.i("WEBVIEW", url);
        //if (wv != null && (wv.getUrl() == null || wv.getUrl().equals(getResources().getString(R.string.no_tutorial_url))))
        if (wv != null){
            if((wv.getUrl() == null || !wv.getUrl().equals(url))) {//se webView esiste e (la url non è null o se è diversa da quella che voglio caricare)
                wv.onResume();
                wv.loadUrl(url);
            }else{
                wv.onResume();
            }
        }
    }

    private void setDefaulTextImpostazioni(){
        TextView tv = findViewById(R.id.periferica_connessa_tv);
        if (tv != null)
            tv.setText(getResources().getString(R.string.no_dispositivo_connesso));
        TextView sub = findViewById(R.id.periferica_connessa_sub);
        if (sub != null)
            sub.setText(getResources().getString(R.string.sel_dispositivo));
    }

    @Override
    public void noBluetooth() {
        /*Toast toast = Toast.makeText(this, R.string.bluetooth_error, Toast.LENGTH_LONG);
        toast.show();*/
    }

    @Override
    public void pressioneChanged(float pressioneLorda, float temperatura, float pressioneNetta) {
       // Log.i("Lettore", "MAIN ACTIVITY PAGE " + ((mViewPager == null)?"nessuna": mViewPager.getCurrentItem()) +"PRESSIONE CAMBIATA");
    }

    @Override
    public void batteriaChanged(final long batteria) {
        if (mViewPager.getCurrentItem() == IMPOSTAZIONI_TAB_INDEX){
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    TextView tv = findViewById(R.id.periferica_connessa_sub);
                    if (tv != null)
                    tv.setText(getResources().getString(R.string.batteria) + " " + Long.toString(batteria) + " %");
                }
            });
        }
    }


    /** FUNZIONI PER APRIRE LE VARIE PAGINE NEL FRAGMENT IMPOSTAZIONI**/

    public void apriProfiliMainActivity(View view) {
        Intent i = new Intent(this, ProfiliActivity.class);
        startActivity(i);
    }

    public void apriModelliMainActivity(View view) {
        Intent i = new Intent(this, ModelliActivity.class);
        //startActivityForResult(i, REQUEST_MODEL);
        startActivity(i);
    }

    public void apriListaDevices(View view) {
        Intent i = new Intent(this, ListaDevicesActivity.class);
        startActivity(i);
    }

    public void apriNoteLegali(View view) {
        Intent i = new Intent(this, NoteLegaliActivity.class);
        startActivity(i);
    }
    public void apriSoglieActivity(View view) {
        Intent i = new Intent(this, SoglieActivity.class);
        startActivity(i);
    }
    public void apriInfoActivity(View view) {
        Intent i = new Intent(this, InfoActivity.class);
        startActivity(i);
    }
    public static void apriRec(View view, Activity ac, int currentItem) {
        SharedPreferences prefs = ac.getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        prefs.edit().putInt("viewpager_position", currentItem).apply();
        Intent i = new Intent(ac, RecActivity.class);
        ac.startActivity(i);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == IMPORT_REQUEST_CODE){
            if(resultCode == RESULT_OK){
                Uri dataUri = data.getData();
                getFileFromIntent(dataUri);
            }
        }
    }

    public void performFileSearch() {
        // ACTION_OPEN_DOCUMENT is the intent to choose a file via the system's file
        // browser.
        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        // Filter to only show results that can be "opened", such as a
        // file (as opposed to a list of contacts or timezones)
        intent.addCategory(Intent.CATEGORY_OPENABLE);
        // Filter to show only images, using the image MIME data type.
        // If one wanted to search for ogg vorbis files, the type would be "audio/ogg".
        // To search for all documents available via installed storage providers,
        // it would be "*/*".
        intent.setType("application/octet-stream");
        startActivityForResult(intent, IMPORT_REQUEST_CODE);
    }

    /**
     * gestione del click sul linearLayout di FragmentJellifish
     * @param view
     */
    public void beginJellyfish(View view) {
        // valutare se ci sono un dispositivo connesso e una soglia impostata
        if(deviceConnected && !"".equals(soglia) && soglia != null){
            SharedPreferences prefs = this.getSharedPreferences(
                    "com.example.app", Context.MODE_PRIVATE);
            prefs.edit().putInt("viewpager_position", JELLYFISH_TAB_INDEX).apply();
            //se c'è un dispositivo connesso, si apre l'activity del gioco Jellyfish
            Intent i = new Intent(this, JellyfishActivity.class);
            startActivity(i);
        }else{ //se non c'è alcun dispositivo connesso, si va nelle impostazioni e ci cerca di connetterci
            TabLayout.Tab tab = tabLayout.getTabAt(IMPOSTAZIONI_TAB_INDEX);
            tab.select();
        }
    }

    private void setTextViewJellyFragment(){
        if(!this.isOnPause) {
            TextView tv = findViewById(R.id.begin_text_view);
            if (tv != null) {
                if (!"".equals(soglia) && soglia != null) {
                    if (deviceConnected) {
                        tv.setText(R.string.begin);
                    } else {//se il dispositivo non è connesso
                        tv.setText(R.string.touch_to_connect);
                    }
                } else {//se la soglia non è impostata
                    tv.setText(R.string.touch_to_set_threshold);
                }
            }
        }
    }

    /** ROBA CHE RIGUARDA IL VIEWPAGER */

//metto le icone alle tabs
    private void setupTabIcons(TabLayout tabLayout) {
        tabLayout.getTabAt(DIARIO_TAB_INDEX).setIcon(R.drawable.ic_diario_selector);
        tabLayout.getTabAt(IMPOSTAZIONI_TAB_INDEX).setIcon(R.drawable.ic_settings_selector);
        tabLayout.getTabAt(TUTORIAL_TAB_INDEX).setIcon(R.drawable.ic_tutorial_selector);
        tabLayout.getTabAt(JELLYFISH_TAB_INDEX).setIcon(R.drawable.ic_jellyfish_selector);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment correspnding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentPagerAdapter{
        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            // Show 3 total pages.
            return 4;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            /*switch (position) {
                case 0:
                    return getResources().getString(R.string.tab_diario);
                case 1:
                    return getResources().getString(R.string.tab_impostazioni);
                case 2:
                    return getResources().getString(R.string.tab_tutorial);
            }*/
            return "";
        }

        @Override
        public android.app.Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return new FragmentDiario();
                case 1:
                    return new FragmentImpostazioni();
                case 2:
                    return new FragmentTutorial();
                case 3:
                    return new FragmentJellyfish();
            }
            return null;
        }
    }

    public static int getDefaultTabIndex(){
        return defaultPageSelected;
    }

    /*
    //chiamato quando ruota lo schermo
    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        Log.d("tag", "config changed");
        super.onConfigurationChanged(newConfig);

        int orientation = newConfig.orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT)
            Log.d("tag", "Portrait");
        else if (orientation == Configuration.ORIENTATION_LANDSCAPE)
            Log.d("tag", "Landscape");
        else
            Log.w("tag", "other: " + orientation);
    }*/
}
