package mindinformatica.com.earon.rec;

/**
 * Created by camillacecconi on 19/07/17.
 */

public class Misura {
    private float pressure;
    private float temperature;
    private long timestamp;
    private float rawPressure;

    public float getPressione() {
        return pressure;
    }

    public void setPressure(float pressione) {
        this.pressure = pressione;
    }

    public float getRawPressure() {
        return rawPressure;
    }

    public void setRawPressure(float pressoneLorda) {
        this.rawPressure = pressoneLorda;
    }

    public float getTemperature() {
        return temperature;
    }

    public void setTemperature(float temperatura) {
        this.temperature = temperatura;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timeStamp) {
        this.timestamp = timeStamp;
    }

    public String creaStringa(){
        return Float.toString(pressure) + "^" + Float.toString(rawPressure) + "^" + Float.toString(temperature) + "^" + Long.toString(timestamp);
    }
    public void parsaStringa(String s){
        String[] lista = s.split("\\^");
        if (lista.length >= 4){
            if (!"".equals(lista[0]))
                pressure = Float.parseFloat(lista[0]);
            if (!"".equals(lista[1]))
                rawPressure = Float.parseFloat(lista[1]);
            if (!"".equals(lista[2]))
                temperature = Float.parseFloat(lista[2]);
            if (!"".equals(lista[3]))
                timestamp = (long)Float.parseFloat(lista[3]);
        }

    }
}
