package mindinformatica.com.earon.impostazioni;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import mindinformatica.com.earon.MainActivity;
import mindinformatica.com.earon.application.EaronApplication;
import mindinformatica.com.earon.bluetooth.BluetoothActivity;
import mindinformatica.com.earon.R;
import mindinformatica.com.earon.rec.ProfiliActivity;

/**
 * Created by camillacecconi on 11/07/17.
 */

public class FragmentImpostazioni extends Fragment {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_settings, container, false);
        return rootView;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (getActivity() != null && BluetoothActivity.class.isAssignableFrom((getActivity().getClass()))&& getUserVisibleHint()){
            ((BluetoothActivity)getActivity()).scan(false); //inizio con uno scan, se non lo trovo, dopo un pochino mi fermo
        }
        View fab = getActivity().findViewById(R.id.fab);
        fab.setVisibility(View.VISIBLE);

        //imposto la descrizione del profilo selezionato
        SharedPreferences prefs = getActivity().getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        ProfiliActivity.Profili profilo = ProfiliActivity.Profili.values()[prefs.getInt("profilo", 0)];
        String profiloTitle =  getResources().getString(ProfiliActivity.titoli.get(profilo));
        ((TextView)getView().findViewById(R.id.profilo_impost)).setText(profiloTitle);
        ImageView iv = getView().findViewById(R.id.img_logo_profilo);
        if (Build.VERSION.SDK_INT < 21){
            iv.setImageDrawable(getResources().getDrawable(ProfiliActivity.loghi.get(profilo)));
        }else{
            iv.setImageDrawable(getResources().getDrawable(ProfiliActivity.loghi.get(profilo), getActivity().getTheme()));
        }
        String titoloModello = prefs.getString("titoloModello", getResources().getString(R.string.default_template_title));
        ((TextView)getView().findViewById(R.id.modello_impost)).setText(titoloModello);

    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if (getActivity() != null && BluetoothActivity.class.isAssignableFrom((getActivity().getClass()))){
            if (isVisibleToUser){
                View fab = getActivity().findViewById(R.id.fab);
                fab.setVisibility(View.VISIBLE);
                //ac.scan(false);//inizio con uno scan, se non lo trovo, dopo un pochino mi fermo
                SharedPreferences prefs = getActivity().getSharedPreferences(
                        "com.example.app", Context.MODE_PRIVATE);
                //imposto il livello della batteria dalle prefs, altrimenti compare la label di default
                MainActivity.setBatteryFromPrefs((MainActivity) getActivity(), prefs);
            }else{
                //ac.disconnect();
                putBatteryLevelIntoPrefs();
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if (getActivity() != null && BluetoothActivity.class.isAssignableFrom((getActivity().getClass()))){
            Log.i("ON PAUSE", "going to disconnect FRAGMENT");
            //((BluetoothActivity)getActivity()).disconnect();
        }
        putBatteryLevelIntoPrefs();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        TextView tv = (TextView) view.findViewById(R.id.periferica_connessa_tv);
        tv.setText(R.string.no_dispositivo_connesso);
        TextView tvsub = (TextView) view.findViewById(R.id.periferica_connessa_sub);
        MainActivity ac = ((MainActivity)getActivity());

        if( ((EaronApplication) ac.getApplication()).isConnected()){
            SharedPreferences prefs = ac.getSharedPreferences(
                    "com.example.app", Context.MODE_PRIVATE);
            final String batteryLevel = prefs.getString("batteryLevel", "");
            final TextView sub = ac.findViewById(R.id.periferica_connessa_sub);
            if (sub != null) {
                sub.setText(batteryLevel);
            }
        }else{
            tvsub.setText(getActivity().getResources().getString(R.string.sel_dispositivo));
        }

        SharedPreferences prefs = getActivity().getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        String soglia = prefs.getString("soglia", "");
        String limsup = prefs.getString("limsup", "");
        String liminf = prefs.getString("liminf", "");
        if(!"".equals(soglia) || !"".equals(liminf) | !"".equals(limsup)) {
            TextView tvSoglie = view.findViewById(R.id.soglia_impost);
            String testo = "";
            if (!"".equals(soglia)) {
                testo = testo + getString(R.string.soglia_des) + " " + soglia + " ";
            }
            if (!"".equals(limsup)) {
                testo = testo + getString(R.string.max) + " " + limsup + " ";
            }
            if (!"".equals(liminf)) {
                testo = testo + getString(R.string.min) + " " + liminf;
            }
            tvSoglie.setText(testo);
        }
    }

    private void putBatteryLevelIntoPrefs(){
        SharedPreferences prefs = getActivity().getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        TextView subTextView = getActivity().findViewById(R.id.periferica_connessa_sub);
        if (subTextView != null) {
            String batteryLevel = subTextView.getText().toString();
            prefs.edit().putString("batteryLevel", batteryLevel).commit();
        }
    }
}