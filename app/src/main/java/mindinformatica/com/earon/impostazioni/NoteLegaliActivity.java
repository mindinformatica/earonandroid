package mindinformatica.com.earon.impostazioni;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.os.Bundle;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import java.util.Locale;

import mindinformatica.com.earon.R;
import mindinformatica.com.earon.bluetooth.BluetoothActivity;
import mindinformatica.com.touchviewlibrary.TouchImageView;

/**
 * Created by camillacecconi on 17/07/17.
 */

public class NoteLegaliActivity extends AppCompatActivity {

    private boolean primaVolta = true;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SharedPreferences prefs = this.getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        primaVolta = prefs.getBoolean("firstTime", true);
        setContentView(R.layout.activity_note_legali);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(R.string.activity_note_title);
        Button acceptBtn = findViewById(R.id.button_accept);
        if(primaVolta){
            //si mostra il pulsante per accettare le note legali
            acceptBtn.setVisibility(View.VISIBLE);
        }else{
            //si abilita il pulsante indietro sulla toolbar
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
        TouchImageView pdfView = findViewById(R.id.pdf_view);
        //apro il file delle note legali in base alla lingua
        String language = Locale.getDefault().getLanguage();
        int idNote;
        switch (language){
            case "it":
                idNote = R.drawable.note_legali;
                break;
            case "zh":
                idNote = R.drawable.note_legali_zh;
                break;
            default:
                idNote = R.drawable.note_legali_en;
                break;
        }
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), idNote);
        float scale = ((float)getResources().getDisplayMetrics().widthPixels)/((float)bitmap.getWidth());
        pdfView.setBitmap(bitmap, scale);

    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                BluetoothActivity.changeColorStatusBar(this, R.color.colorPrimary);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        //si abilita il back button solo se non è la prima volta che leggiamo le note legali
        if (!primaVolta) {
            super.onBackPressed();
        }
    }

    public void acceptBtnClicked(View view) {
        SharedPreferences prefs = getSharedPreferences(
                "com.example.app", Context.MODE_PRIVATE);
        prefs.edit().putBoolean("firstTime", false).commit();
        finish();
    }
}
