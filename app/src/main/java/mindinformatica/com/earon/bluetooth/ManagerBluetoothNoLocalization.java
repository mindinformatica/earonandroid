package mindinformatica.com.earon.bluetooth;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.ScanSettings;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.os.Build;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import android.util.Log;
import java.util.ArrayList;

import mindinformatica.com.earon.application.EaronApplication;

/**
 * manager bluetooth per device con api superiore alla 23
 * Si scansionano i dispositivi bluetooth disponibili senza dover avere la localizzazione attiva
 * Created by lorenzo on 03/08/18.
 */



public class ManagerBluetoothNoLocalization extends AbstractManagerBluetooth {


    public ManagerBluetoothNoLocalization(EaronApplication c){
        super(c);
        //mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    //@Override
    public void startScannerOLD(boolean autoStop){
        //controllo se posso usare il bluetooth
        if (mBluetoothAdapter == null || !mBluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
        } else {
            mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
            settings = new ScanSettings.Builder()
                    .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                    .build();
            filters = new ArrayList<>();

            switch (ContextCompat.checkSelfPermission(app, Manifest.permission.ACCESS_FINE_LOCATION)) {
                case PackageManager.PERMISSION_DENIED:
                    Log.e("PERMESSO", "permesso negato");
                    if (ContextCompat.checkSelfPermission(app, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(activity,
                                new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                REQUEST_ACCESS_FINE_LOCATION);
                    }
                    break;
                case PackageManager.PERMISSION_GRANTED:
                    Log.e("PERMESSO", "permesso concesso");
                    scanLeDevice(true, autoStop);
                    break;
            }

        }
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void startScanner(boolean autoStop){
        switch (ContextCompat.checkSelfPermission(app, Manifest.permission.ACCESS_FINE_LOCATION)) {
            case PackageManager.PERMISSION_DENIED:
                Log.e("PERMESSO", "permesso negato");
                if (ContextCompat.checkSelfPermission(app, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(activity,
                            new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                            REQUEST_ACCESS_FINE_LOCATION);
                }
                break;
            case PackageManager.PERMISSION_GRANTED:
                Log.e("PERMESSO", "permesso concesso");
                final BluetoothManager bluetoothManager = (BluetoothManager) app.getSystemService(Context.BLUETOOTH_SERVICE);
                if (bluetoothManager != null) mBluetoothAdapter = bluetoothManager.getAdapter();
                if (!mBluetoothAdapter.isEnabled()){
                    //mBluetoothAdapter.enable();
                    Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                    activity.startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
                }
                mLEScanner = mBluetoothAdapter.getBluetoothLeScanner();
                settings = new ScanSettings.Builder()
                        .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                        .build();
                filters = new ArrayList<>();

                scanLeDevice(true, autoStop);
                break;
        }

    }


    @Override
    public void pause(){
        if (mBluetoothAdapter != null && mBluetoothAdapter.isEnabled()) {
            mBluetoothAdapter.cancelDiscovery();
        }
    }

    @Override
    public void destroy() {
        super.destroy();
        //app.unregisterReceiver(mReceiver);
    }

    private boolean mScanning;

    @Override
    public void scanLeDevice(final boolean enable, final boolean autoStop) {
        if (enable) {
            if (!mScanning) {
                Log.e("SCAN", app.TAG + " scan start");
                if (mReceiver == null) {
                    mReceiver = new SingBroadcastReceiver();
                    app.registerReceiver(mReceiver, new IntentFilter(BluetoothDevice.ACTION_FOUND));
                }
                if (autoStop) {
                    mHandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            mScanning = false;
                            mBluetoothAdapter.cancelDiscovery();
                            unregisterAppReceiver();
                        }
                    }, SCAN_PERIOD);
                }
                mScanning = true;
                mBluetoothAdapter.startDiscovery();
            }
        } else {
            mScanning = false;
            Log.e("SCAN", app.TAG + " scan stop");
            if (mBluetoothAdapter != null){
                mBluetoothAdapter.cancelDiscovery();
            }
            unregisterAppReceiver();
        }
    }

    public void unregisterAppReceiver(){
        if(mReceiver != null) {
            app.unregisterReceiver(mReceiver);
            mReceiver = null;
        }
    }

    private SingBroadcastReceiver mReceiver;

    public class SingBroadcastReceiver extends BroadcastReceiver {

        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (BluetoothDevice.ACTION_FOUND.equals(action)){
                // Get the BluetoothDevice object from the Intent
                BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                if (device.getName() != null)
                    Log.i("device", "device " + device.getName() + " " + app.TAG);
                app.deviceFound(device);
            }
        }
    }

}
