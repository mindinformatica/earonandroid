package mindinformatica.com.earon.rec;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.TextView;

import mindinformatica.com.earon.R;

/**
 * Created by camillacecconi on 25/07/17.
 */

public class FullScreenDialog extends Dialog{

    public Activity c;

    public FullScreenDialog(Activity a) {
        super(a);
        this.c = a;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.fullscreen_dialog);
        ((LinearLayout)findViewById(R.id.bg_dialog)).setAlpha(0.5f);

    }
    @Override
    public void onStart() {
        super.onStart();

        int height = (int) (c.getResources().getDisplayMetrics().heightPixels);
        int width = (int) (c.getResources().getDisplayMetrics().widthPixels);

        getWindow().setLayout(width, height);
        getWindow().setBackgroundDrawableResource(android.R.color.transparent);

    }

    public void setText(String t){
        TextView text = findViewById(R.id.fullScreen_dialog);
        if(text != null) {
            text.setText(t);
        }
    }
}