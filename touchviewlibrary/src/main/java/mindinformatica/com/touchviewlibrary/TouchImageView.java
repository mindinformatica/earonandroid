package mindinformatica.com.touchviewlibrary;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

/**
 * Questa classe gestisce una immagine sulla quale puo' essere fatto pan e zoom.
 * Se viene cliccato sul marcatore allora lancia un evento.
 * @author lcecchi
 */
@SuppressLint("ClickableViewAccessibility")
public class TouchImageView extends View implements View.OnTouchListener {
	private Matrix mTrasformazione;
	private Bitmap mBitmap;
	private Paint mPaint;
	private Vettore2D mPosizione;
	private float mScale;
	private TouchHelper mTouchHelper;
	private Rect mTempRect;
	private final float mDensity;
	private Rect mTempRect2;
	private Paint mPaint2;

	// Questo parametro rappresenta la larghezza del dito :)
	private static float DELTA_MARCATORE = 20;
	
	@SuppressLint("ClickableViewAccessibility")
	public TouchImageView(Context context, AttributeSet setAttributi) {
		super(context, setAttributi);
		
		mTrasformazione = new Matrix();
		mPaint = new Paint();
		mPaint2 = new Paint();
		mDensity = getContext().getResources().getDisplayMetrics().density;
		mPaint.setTextSize(mDensity * 20);
		mBitmap = null;
		mTouchHelper = new TouchHelper(2, mDensity);
		mTempRect = new Rect();
		mTempRect2 = new Rect();
		reset();
		setOnTouchListener(this);
	}
	
	public void reset() {
		mScale = 1.0f;
		mPosizione = new Vettore2D(0,0);
		invalidate();
	}
	
	/**
	 * Imposta l'immagine da visualizzare
	 * @param value
	 */
	public void setBitmap(Bitmap value) {
		mBitmap = value;
		mScale = 1.0f;
		invalidate();
	}
	public void setBitmap(Bitmap value, float scale) {
		mBitmap = value;
		mScale = scale;
		invalidate();
	}
	
	/**
	 * Prende l'immagine da visualizzare
	 * @return
	 */
	public Bitmap getBitmap() {
		return mBitmap;
	}

	/**
	 * Disegno di tutto il casino
	 */
	@Override
	public void onDraw(Canvas canvas) {
		if ( mBitmap!=null ) {
			if ( mPosizione.getX()==0 && mPosizione.getY()==0 ) {
				// Siamo al primo disegno!
				mPosizione.set(mBitmap.getWidth()/2, mBitmap.getHeight()/2);
			}
			
			// Disegno la bitmap
			mTrasformazione.reset();
			mTrasformazione.postTranslate(-mBitmap.getWidth()/2, -mBitmap.getHeight()/2);
			mTrasformazione.postScale(mScale, mScale);
			mTrasformazione.postTranslate(mPosizione.getX(), mPosizione.getY());
			mTrasformazione.postTranslate((this.getWidth()/2)-mBitmap.getWidth()/2, (this.getHeight()/2)-mBitmap.getHeight()/2);
			canvas.drawBitmap(mBitmap, mTrasformazione, mPaint);


		}
	}

	/**
	 * Gestione degli eventi touch
	 */
	@Override
	public boolean onTouch(View v, MotionEvent event) {
		mTouchHelper.processaEvento(event);
		if ( mTouchHelper.getDitaPremute()==1 ) {
			// Si tratta di una traslazione
			mPosizione.aggiungi(mTouchHelper.deltaMovimento(0));
			
			// Gestisco anche il click sul marker

			invalidate();
		} else if ( mTouchHelper.getDitaPremute()==2 ) {
			// Si tratta di uno zoom
			float corrente = Vettore2D.sottrai(mTouchHelper.getPosizione(1), mTouchHelper.getPosizione(0)).getLength();
			float precedente = Vettore2D.sottrai(mTouchHelper.getPosizionePrecedente(1), mTouchHelper.getPosizionePrecedente(0)).getLength();
			
			if ( precedente!=0 && Math.abs(corrente-precedente)<64) {
				mScale *= corrente/precedente;
			}
			invalidate();
		} else {
			// Troppe dita!!!
		}
		return true;
	}
	
	/**
	 * Chi vuole ascoltare i click sui marker deve implementare questa intefaccia 
	 * @author lcecchi
	 */
	public static interface OnMarkerClick {
		/**
		 * Chiamato sul click del marker
		 */
		
		public void onMarkerClick(float x, float y);
		public void onBalloonClick(float x, float y);
	}

}
