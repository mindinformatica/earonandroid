package mindinformatica.com.earon.db;

/**
 * Created by camillacecconi on 16/01/17.
 */

public interface TableInterface {
    public static final String TYPE_TEXT = " TEXT";
    public static final String TYPE_INTEGER = " INTEGER";
    public static final String TYPE_REAL = " REAL";

    public String getCreateString();
    public String getDropString();
  //  public static Uri getUriPath(); // perché in un'interfaccia non posso mettere metordi statici???
}
