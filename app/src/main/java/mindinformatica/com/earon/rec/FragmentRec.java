package mindinformatica.com.earon.rec;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.formatter.IAxisValueFormatter;

import mindinformatica.com.earon.R;

/**
 * Created by camillacecconi on 19/07/17.
 */

public class FragmentRec extends Fragment{
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.grafico_layout, container, false);
            LineChart grafico = (LineChart) rootView.findViewById(R.id.grafico);
            //non voglio vedere righe verticali e orizzontali sullo sfondo
            grafico.getXAxis().setDrawGridLines(false);
            grafico.getAxisRight().setDrawGridLines(false);
            grafico.getAxisLeft().setDrawGridLines(false);

            grafico.getXAxis().setLabelCount(8);
            //TODO labelposition
            grafico.getXAxis().setValueFormatter(new IAxisValueFormatter() {
                @Override
                public String getFormattedValue(float value, AxisBase axis) {
                    return null; //TODO
                }
            });
            grafico.getLegend().setEnabled(false);
            grafico.setNoDataText("");
            //TODO self.lineChartView.chartDescription?.text = ""

            //TODO eventualmente devo mettere le label alle righe delle soglie

        return rootView;
    }
}
