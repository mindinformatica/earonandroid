package mindinformatica.com.earon.rec;

import android.os.Build;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.NumberPicker;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.ArrayList;

import mindinformatica.com.earon.R;
import mindinformatica.com.earon.bluetooth.BluetoothActivity;
import mindinformatica.com.earon.db.Modello;
import mindinformatica.com.earon.utils.ModelloBarView;

/**
 * Created by camillacecconi on 19/07/17.
 */

public class ModelloDetailActivity extends AppCompatActivity {
    private static final String[] valoriPicker = new String[180];
    static{
        for (int i = 1; i <= 180; i++){
            valoriPicker[i-1] = DateUtils.formatElapsedTime(i);
        }
    }
    private int indiceIntervalloSelezionato;
    private Long idModello;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_modello_detail);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_cancel);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final ModelloBarView barra = findViewById(R.id.modello_detail_barra);
        final NumberPicker np = findViewById(R.id.np_modello);
        np.setWrapSelectorWheel(false);
        np.setMinValue(1);
        np.setMaxValue(180);
        np.setDisplayedValues(valoriPicker);
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                barra.getIntervalli().set(indiceIntervalloSelezionato, i1);
                ArrayList<Integer> intervalliNuovi = barra.getIntervalli();
                barra.setIntervalli(intervalliNuovi);
               // float scala = (float)barra.getWidth()/(float)barra.getDurata();
               // barra.invalidate();
            }
        });
        np.setVisibility(View.INVISIBLE);

        Bundle b = getIntent().getExtras();

        ArrayList<Integer> intervalli = null;
        String titolo = "";
        if (b != null){
            intervalli = b.getIntegerArrayList("modello");
            titolo = b.getString("titoloModello", null);
            idModello = b.getLong("idModello");
        }
        ((EditText)findViewById(R.id.titolo_template)).setText(titolo);
        if (idModello != null && (Long.valueOf(-1)).equals(idModello)){
            findViewById(R.id.titolo_template).setActivated(false);
        }
        barra.setIntervalli(intervalli);
        barra.setActive(true, null);
        ModelloBarView.OnBarSegmentClickListener listener = new ModelloBarView.OnBarSegmentClickListener() {
            @Override
            public void cliccato(Integer indice) {
                indiceIntervalloSelezionato = indice;
                np.setVisibility(View.VISIBLE);
                np.setValue(barra.getIntervalli().get(indice));
            }
        };
        barra.setSegmentClickListener(listener);

        if (intervalli.size() <= 1){
            findViewById(R.id.minus_button).setClickable(false);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                BluetoothActivity.changeColorStatusBar(this, R.color.colorPrimary);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater=getMenuInflater();
        inflater.inflate(R.menu.menu_modelli_detail, menu);
        // se ho il modello di default, devo nascondere il tasto per cancellare
        Long idModello = getIntent().getLongExtra("idModello", -1);
        if (idModello != null && (Long.valueOf(-1)).equals(idModello)){
            menu.findItem(R.id.action_save_modello).setVisible(false);
            menu.findItem(R.id.action_delete_modello).setVisible(false);
            findViewById(R.id.minus_button).setVisibility(View.INVISIBLE);
            findViewById(R.id.plus_button).setVisibility(View.INVISIBLE);
        }

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_save_modello:
                String nome = ((EditText)findViewById(R.id.titolo_template)).getText().toString();
                ModelloBarView barra = findViewById(R.id.modello_detail_barra);
                Modello m = new Modello(barra.getIntervalli(), nome);
                if (idModello == -2){
                    m.inserisciModello(this);
                    finish();
                }else if (idModello != -1){
                    m.setId(idModello);
                    m.salvaModello(this, idModello);
                    finish();
                }

                break;
            case R.id.action_delete_modello:
                if ( idModello != null && !idModello.equals(-1)){
                    Modello.deleteModello(this, idModello);
                    finish();
                }
                break;
        }
        return super.onOptionsItemSelected(item);
    }
    public void plusClicked(View view) {
        final ModelloBarView barra = findViewById(R.id.modello_detail_barra);
        barra.addIntervallo(30);
        findViewById(R.id.minus_button).setClickable(true);
        barra.getChildAt(barra.getChildCount()-1).performClick();
    }
    public void minusClicked(View view) {
        final ModelloBarView barra = findViewById(R.id.modello_detail_barra);
        barra.removeIntervallo();
        if (barra.getIntervalli().size() <= 1){
            view.setClickable(false);
        }
    }
}
