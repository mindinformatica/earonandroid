package mindinformatica.com.earon.rec;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import java.util.HashMap;

import mindinformatica.com.earon.R;
import mindinformatica.com.earon.bluetooth.BluetoothActivity;

/**
 * Created by camillacecconi on 18/07/17.
 */

public class ProfiliActivity extends AppCompatActivity {
    public static enum Profili {FREE, FRENZEL, ADVANCED_FRENZEL, ADVANCED_FRENZEL_2, CARPA_INVERSA, MOUTHFILL, MOUTHFILL_REFILL};
    public static final HashMap<Profili, Integer> immagini = new HashMap<>();
    public static final HashMap<Profili, Integer> grafici = new HashMap<>();
    public static final HashMap<Profili, Integer> titoli = new HashMap<>();
    public static final HashMap<Profili, Integer> loghi = new HashMap<>();
    static{
        immagini.put(Profili.FREE, R.drawable.free);
        grafici.put(Profili.FREE, R.drawable.free);
        titoli.put(Profili.FREE, R.string.free);
        loghi.put(Profili.FREE, R.drawable.free_new);
        immagini.put(Profili.FRENZEL, R.drawable.frenzel);
        grafici.put(Profili.FRENZEL, R.drawable.frenzel_grafico);
        titoli.put(Profili.FRENZEL, R.string.frenzel);
        loghi.put(Profili.FRENZEL, R.drawable.frenzel_new);
        immagini.put(Profili.ADVANCED_FRENZEL, R.drawable.advanced_frenzel);
        grafici.put(Profili.ADVANCED_FRENZEL, R.drawable.advanced_frenzel_grafico);
        titoli.put(Profili.ADVANCED_FRENZEL, R.string.ad_frenzel);
        loghi.put(Profili.ADVANCED_FRENZEL, R.drawable.advanced_frenzel_new);
        immagini.put(Profili.ADVANCED_FRENZEL_2, R.drawable.advanced_frenzel_2);
        grafici.put(Profili.ADVANCED_FRENZEL_2, R.drawable.advanced_frenzel_2_grafico);
        titoli.put(Profili.ADVANCED_FRENZEL_2, R.string.ad_frenzel_2);
        loghi.put(Profili.ADVANCED_FRENZEL_2, R.drawable.advanced_frenzel_2_new);
        immagini.put(Profili.CARPA_INVERSA, R.drawable.reverse_packing);
        grafici.put(Profili.CARPA_INVERSA, R.drawable.reverse_packing_grafico);
        titoli.put(Profili.CARPA_INVERSA, R.string.rev_pack);
        loghi.put(Profili.CARPA_INVERSA, R.drawable.reverse_packing_new);
        immagini.put(Profili.MOUTHFILL, R.drawable.mouthfill);
        grafici.put(Profili.MOUTHFILL, R.drawable.mouthfill_grafico);
        titoli.put(Profili.MOUTHFILL,  R.string.mouthfill);
        loghi.put(Profili.MOUTHFILL, R.drawable.mouthfill_new);
        immagini.put(Profili.MOUTHFILL_REFILL, R.drawable.mouthfill_refill);
        grafici.put(Profili.MOUTHFILL_REFILL, R.drawable.mouthfill_refill_grafico);
        titoli.put(Profili.MOUTHFILL_REFILL, R.string.mouthfill_refill);
        loghi.put(Profili.MOUTHFILL_REFILL, R.drawable.mouthfill_refill_new);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profili);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(R.string.activity_profili_title);
        ListView lv = findViewById(R.id.lv_profili);
        ProfiliAdapter ad = new ProfiliAdapter();
        lv.setAdapter(ad);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                SharedPreferences.Editor editor = ProfiliActivity.this.getSharedPreferences(
                        "com.example.app", Context.MODE_PRIVATE).edit();
                editor.putInt("profilo", i);
                editor.commit();
                onBackPressed();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                BluetoothActivity.changeColorStatusBar(this, R.color.colorPrimary);
            }
        }catch (Exception ex){
            ex.printStackTrace();
        }
    }

    private class ProfiliAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return Profili.values().length;
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public View getView(int i, View view, ViewGroup viewGroup) {
            View v = LayoutInflater.from(ProfiliActivity.this).inflate(R.layout.profilo_list_item, null);
            ImageView iv = v.findViewById(R.id.img_profilo);
            ImageView iv_logo = v.findViewById(R.id.img_logo_profilo);
            TextView tv = v.findViewById(R.id.txt_profilo);
            Profili p = Profili.values()[i];
            if (Build.VERSION.SDK_INT < 21){
                //solo per il profilo free, non disegno l'immagine ed il logo
                if( !"FREE".equals(p.name()) ) {
                    iv.setImageDrawable(getResources().getDrawable(immagini.get(p)));
                    iv_logo.setImageDrawable(getResources().getDrawable(loghi.get(p)));
                }
            }else{
                if ( !"FREE".equals(p.name()) ) {
                    iv.setImageDrawable(getResources().getDrawable(immagini.get(p), getTheme()));
                    iv_logo.setImageDrawable(getResources().getDrawable(loghi.get(p), getTheme()));
                }
            }
            tv.setText(getResources().getString(titoli.get(p)));
            return v;
        }
    }

}
